// The file for the current environment will overwrite this one during build.
// Different environments can be found in ./environment.{dev|prod}.ts, and
// you can create your own and use it with the --env flag.
// The build system defaults to the dev environment.

export const environment = {
	production: false,
	apiURL: "http://88.198.134.26:9292",
	apiSufix: '/ws/rest'
};
