import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { UserAdministrationComponent } from './user-administration/user-administration.component';
import { RolePermissionsComponent } from './role-permissions/role-permissions.component';
import { ConfigValuesComponent } from './config-values/config-values.component';
import { PeripheralSystemsComponent } from './peripheral-systems/peripheral-systems.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CustomersListComponent } from './user-administration/customers-list/customers-list.component';

import { SharedModule } from './../shared/shared.module';
import { PipesModule } from './../shared/pipes/pipes.module';

import {
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatChipsModule,
    MatSelectModule
} from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NewUserComponent } from './user-administration/new-user/new-user.component';
import { NewRoleComponent } from './role-permissions/new-role/new-role.component';
import { NgSelectModule } from '@ng-select/ng-select';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditRoleComponent } from './role-permissions/edit-role/edit-role.component';

@NgModule({
    declarations: [
        UserAdministrationComponent,
        RolePermissionsComponent,
        ConfigValuesComponent,
        PeripheralSystemsComponent,
        NewUserComponent,
        NewRoleComponent,
        CustomersListComponent,
        EditRoleComponent],
    imports: [
        CommonModule,
        SettingsRoutingModule,
        NgxDatatableModule,
        MatInputModule,
        MatFormFieldModule,
        MatProgressBarModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
        FlexLayoutModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        NgSelectModule,
        SharedModule,
        MatChipsModule,
        MatSelectModule,
        PipesModule
    ],
    entryComponents: [CustomersListComponent]
})
export class SettingsModule { }
