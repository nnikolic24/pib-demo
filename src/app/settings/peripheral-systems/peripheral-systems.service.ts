import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PeripheralSystemsService {

  constructor(private http:HttpClient) { }

  /**
   * fetchPeripherals will receive url as parameter and return promise
   */
  fetchPeripherals(url){
    return this.http.get(url);
  }

  /**
   * parseData will reveive JSON from endpoint,
   * parse it and put into parsedArray and return
   * to controller
   */
  parseData(data) {
    // define empty array which will store result
    let parsedArray = [];

    // if data and rows exist do parsing
    if(data && data.rows && data.rows.count!=0) {
        // go through each row and use only valuable properties
        data.rows.forEach(row => {
            // define newProvider empty object
            let newProvider = {};
            newProvider["Service"] = row.serviceTypeName;
            newProvider["Status"] = "REAL";
            // add object to result array
            parsedArray.push(newProvider);
        });
    }
    // return parsed data
    return parsedArray;
  }
  
  /**
   * updatePeripheralSystem will receive url and service, return observable
   * @param url 
   * @param service 
   */
  updatePeripheralSystem(url:string,service:Object) {
    return this.http.put(url,service);
  }



}
