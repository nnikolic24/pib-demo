import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../core/route-animation/route.animation";
import { TranslateService } from '@ngx-translate/core';
import { PeripheralSystemsService } from './peripheral-systems.service';
import { CoreService } from '../../service/core/core.service';

import {environment} from '../../../environments/environment';


@Component({
   selector: 'ms-peripheral-systems',
   templateUrl: './peripheral-systems.component.html',
   styleUrls: ['./peripheral-systems.component.scss'],
   encapsulation: ViewEncapsulation.None,
   host: {
      "[@fadeInAnimation]": 'true'
   },
   animations: [fadeInAnimation]
})
export class PeripheralSystemsComponent implements OnInit {

   rows: any = [];

   columns = [
      { name: 'Service', prop: 'serviceTypeName' },
      { name: 'Status', prop: 'useDummy' }
   ]

   temp: any = [];

   constructor(private pageTitleService: PageTitleService,
      private translate: TranslateService,
      private peripheralsSvc: PeripheralSystemsService,
      private coreSvc: CoreService) {
      // Call getPeripheral function to retrieve data from endpoint
      this.getPeripheral();
   }

   ngOnInit() {
      this.pageTitleService.setTitle("Peripheral System Services");
   }

   /**
    * getPeripheral() will retrieve data from peripheral-service
    */
   getPeripheral() {
      let url = environment.apiURL + environment.apiSufix + "/settings/peripheral/types";
      this.peripheralsSvc.fetchPeripherals(url)
         .subscribe((result) => {
            console.log("Result:", result);
            // cache our list
            this.temp = result["rows"];
            // push our inital complete list
            this.rows = result["rows"];
         },
            (error) => {
               console.log('Error occured in peripheral-systems component in calling peripheral-systems service:', error.message);
            });
   }

   /**
    * updateService will receive service object and send it to endpoint for update
    * @param service service object
    */
   updateService(service) {
      // Change service useDummy property 
      service.useDummy = service.useDummy ? false :true;
      
      /**
       * Subscribe to service update method, in success callback retrieves all peripherals
       */    
      this.peripheralsSvc.updatePeripheralSystem(environment.apiURL + environment.apiSufix + "/settings/peripheral/types/SmsProvider", service)
         .subscribe((result) => {
            // Call getPeripheral function to retrieve data from endpoint
            this.getPeripheral();
         },
         (error) => {
            console.log('Error occured while calling updatePeripheralSystem in peripheral service:',error.message);
         });
   }
}
