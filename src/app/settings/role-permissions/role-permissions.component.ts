import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../core/route-animation/route.animation";
import { Router } from '@angular/router';
import { RolePermissionsService } from './role-permissions.service';

@Component({
   selector: 'ms-role-permissions',
   templateUrl: './role-permissions.component.html',
   styleUrls: ['./role-permissions.component.scss'],
   encapsulation: ViewEncapsulation.None,
   host: {
      "[@fadeInAnimation]": 'true'
   },
   animations: [fadeInAnimation]
})
export class RolePermissionsComponent implements OnInit {

   roles = [];
   //Temporary array for local filtering
   temp = [];

   constructor(
      private pageTitleService: PageTitleService,
      private router: Router,
      private rolePermsService: RolePermissionsService) { }

   ngOnInit() {
      this.pageTitleService.setTitle("Roles");
      this.rolePermsService.getAllRoles()
         .subscribe((data: any) => {
            this.roles = data.rows.map(row => {
               return {
                  "ID": row.id,
                  "Name": row.name
               }
            });
            this.temp = this.roles;
         }, error => {
            console.error(error);
         });
   }

   /**
     * updateFilter method is used to filter the data.
     */
   updateFilter(event) {
      const val = event.target.value;

      // filter our data
      const temp = this.temp.filter(function (d) {
         return d.ID.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.roles = temp;

   }


   /**
   * Function will execute on any row event and will redirect you to the edit form
   * @param event Received event
   */
   onActivate(event) {
      // If user clicks on row it should redirect him to form with user data 
      if (event.type == 'click') {
         this.router.navigate(['/settings/ac/roles/', event.row.ID]);
      }
   }

   /**
    * Goes back one step
    */
   goBack() {
      window.history.back();
   }
}
