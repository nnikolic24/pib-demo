import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { PageTitleService } from '../../../core/page-title/page-title.service';
import {fadeInAnimation} from "../../../core/route-animation/route.animation";
import { TranslateService } from '@ngx-translate/core';
import { RolePermissionsService } from '../role-permissions.service';
import {  Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ms-new-role',
  templateUrl: './new-role.component.html',
  styleUrls: ['./new-role.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
      "[@fadeInAnimation]": 'true'
  },
  animations: [ fadeInAnimation ]
})
export class NewRoleComponent implements OnInit {

  determinateValue  = 30;

  public form: FormGroup;
  	
  constructor(
    private fb: FormBuilder, 
    private pageTitleService: PageTitleService,
    private translate : TranslateService,
    private rolePermService: RolePermissionsService,
    private router: Router,
    private toastr: ToastrService) {}
  
  ngOnInit() {
    this.pageTitleService.setTitle("Add new role");

    this.form = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)])]   
    });
  }  

  /**
   * Creates a new role
   * @param value Value of the new role that is being created
   */
  createRole() {
    //Creates a new role
    console.log(this.form.value);
    this.rolePermService.createNewRole(this.form.value)
      .subscribe((result:any) => {
        //After creating new role, redirects to the edit form of the newly created role
        this.router.navigate(['/settings/ac/roles/' + result.id]);
        this.toastr.success('Successfully created!');
      },
      error => {
        console.log("There was an error while creating a new role!", error.message);
        this.toastr.error('There was an error!');
      });
  }

  /**
  * Goes back one step
  */
  goBack() {
    window.history.back();
  }

}
