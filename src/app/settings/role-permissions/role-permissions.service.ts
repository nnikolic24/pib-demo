import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreService } from '../../service/core/core.service';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolePermissionsService {

  constructor(private http:HttpClient, private coreSvc: CoreService) { }


    /**
     * Gets all roles from the endpoint
     */
    getAllRoles() {
        return this.http.get(environment.apiURL + environment.apiSufix + `/roles`);
    }

    /**
     * Gets the role by the ID
     * @param id ID of the role you want to get
     */
    getRoleById(id) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/roles/${id}`);
    }

    /**
     * Updates the role
     * @param id ID of the role that needs to be updated
     * @param value New value that you send from Edit form
     */
    updateRole(id, value) {
        let data = value;
        return this.http.put(environment.apiURL + environment.apiSufix + `${id}`, data);
    }

    /**
     * Creates new role
     * @param value Name of the role that is being created
     */
    createNewRole(value) {
        let data = value;
        return this.http.post(environment.apiURL + environment.apiSufix + `/roles/`, data);
    }

    /**
     * Pulls all operations from the endpoint
     */
    getOperations() {
        return this.http.get(environment.apiURL + environment.apiSufix + `/accessrules/matrix/operations`);
    }

    /**
     * Pulls all ressources from the endpoint
     */
    getRessources() {
        return this.http.get(environment.apiURL + environment.apiSufix + `/accessrules/matrix/resources`);
    }
    
    /**
     * Gets the matrix for the specific role
     * @param roleID ID of the role you want to get matrix for
     */
    getACRMatrix(roleID) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/accessrules/matrix/roles/${roleID}`);
    }

    /**
     * Updates the matrix of the specific role
     * @param matrix Updated matrix you send for update
     * @param roleID ID of the role you want to update
     */
    updateACRMatrix(matrix, roleID) {
        return this.http.put(environment.apiURL + environment.apiSufix + `/accessrules/matrix/roles/${roleID}`, matrix);
    }
}
