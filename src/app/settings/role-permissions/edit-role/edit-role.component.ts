import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PageTitleService } from '../../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../../core/route-animation/route.animation";
import { ActivatedRoute } from '@angular/router';
import { RolePermissionsService } from '../role-permissions.service';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax
import { CoreService } from './../../../service/core/core.service';
import { ToastrService } from 'ngx-toastr';
import { BlockUIService } from 'ng-block-ui';

@Component({
  selector: 'ms-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    "[@fadeInAnimation]": 'true'
  },
  animations: [fadeInAnimation]
})
export class EditRoleComponent implements OnInit {

  determinateValue = 30;

  resources = {};
  operations = {};
  matrix: any = {};
  adaptedMatrix = [];
  adaptedOperations = [];
  pro = [];
  id: string;
  role;
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private pageTitleService: PageTitleService,
    private route: ActivatedRoute,
    private coreService: CoreService,
    private rolePermService: RolePermissionsService,
    private toastr: ToastrService,
    private blockUI: BlockUIService) {
  }

  ngOnInit() {
    this.pageTitleService.setTitle("Edit role");
    this.form = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)])]
    });
    //Gets the ID of the role from the URL
    this.id = this.route.snapshot.params.id;
    
    this.connect();
  }

  prepare() {
    //Pulls all data from the endpoint that you need
    return forkJoin([
      this.rolePermService.getRoleById(this.id),
      this.rolePermService.getOperations(),
      this.rolePermService.getRessources(),
      this.rolePermService.getACRMatrix(this.id)
    ]);
  }

  connect() {
    this.blockUI.start('main-block');
    this.prepare()
      .subscribe(data => {
        this.fillForm(data[0]);
        this.prepareOperations(data[1]);
        this.prepareResources(data[2]);
        this.updateScopeMatrix(data[3]);
        this.adaptedMatrix = this.coreService.objectToArray(this.resources);
        this.adaptedOperations = this.coreService.objectToArray(this.operations);
        this.blockUI.stop('main-block');
      })
  }

  //Prepare data (Ressources) you get from the endpoint for easier use
  prepareResources(rawResources) {
    rawResources.rows.forEach(res => {
      this.resources[res] = {
        name: res,
        isNew: true,
        ops: JSON.parse(JSON.stringify(this.operations))
      }
    });
  }

  //Prepare data (Operations) you get from the endpoint for easier use
  prepareOperations(rawOperations) {
    rawOperations.rows.forEach(op => {
      this.operations[op] = {
        name: op,
        checked: false,
        acrId: null
      }
    });
  }

  /**
   * If some checkboxes are checked, they are checked on init.
   * @param matrix Matrix of the specific role
   */
  updateScopeMatrix(matrix) {
    console.log("MATRIX IN SCOPE: ", matrix);
    this.matrix = matrix;
    for (let key in matrix.resourceTypes) {
      let res = this.resources[key];
      if (res) {
        matrix.resourceTypes[key].allowedOperations.forEach((op) => {
          res.ops[op].checked = true;
        });
      }
    }
  }

  /**
   * Fills up the form on load 
   * @param role The role which we selected from the grid
   */
  fillForm(role: any) {
    // Fill all form fields
    this.role = role;
    this.form.controls.name.setValue(role.name);
  }

  /**
   * Saves/Updates the role with all the permissions
   */
  save() {
    //Update the roles name with the name from the form
    this.role.name = this.form.value.name;
    this.rolePermService.updateRole(this.id, this.role)
      .subscribe(result => {
        //Updated role is stored into a variable
        this.role = result;
        //Saves permissions for the role
        this.savePermissions();
        this.toastr.success('Successfully updated!');
        location.reload();
      },
        error => {
          console.log("There was an error while updating role!", error.mesage);
          this.toastr.error('There was an error!');
        });
  }

  /**
   * Saves permissions for the role
   */
  savePermissions() {
    this.adaptedMatrix.forEach((resource) => {
      let res = this.matrix.resourceTypes[resource.name];
      //If is's update, clear all arrays and fill them with new (correct) data
      if (res) {
        res.allowedOperations = [];
        res.deniedOperations = [];
      } else {
        //If it's save, create arrays so you can fill them with correct data
        res = {
          allowedOperations: [],
          deniedOperations: []
        };
        //When both arrays are filled with correct data, save them to the matrix
        this.matrix.resourceTypes[resource.name] = res;
      }
      //Go through all ressources and check witch ones are checked
      for (let key in resource.ops) {
        let operation = resource.ops[key];
        if (operation.checked === true) {
          //If they are checked, pushe them in allowedOperations array
          res.allowedOperations.push(operation.name);
        } else {
          //If they are not checked, push them in deniedOperations array
          res.deniedOperations.push(operation.name);
        }
      }
    });
    //Update the matrix for the corresponding role witht the new matrix
    this.rolePermService
      .updateACRMatrix(this.matrix, this.role.id)
      .subscribe(result => {
        this.updateScopeMatrix(result);
      });
  }

  /**
   * Goes back one step
   */
  goBack() {
    window.history.back();
  }

}
