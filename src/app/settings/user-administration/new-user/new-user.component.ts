// Angular imports
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatDialog } from '@angular/material/dialog';

// 3rd party imports
import { CustomValidators } from 'ng2-validation';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

// Application imports
import { PageTitleService } from '../../../core/page-title/page-title.service';
import { SettingsService } from './../../../service/settings/settings.service';
import { CustomersListComponent } from './../customers-list/customers-list.component';

@Component({
    selector: 'ms-new-user',
    templateUrl: './new-user.component.html',
    styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {

    private passwordRegexPattern: RegExp = /(?=.*[a-z].*)(?=.*[A-Z].*)(?=.*\d.*)(?=.*[^a-zA-Z\d\s:].*)(?!(?=.*[\s].*))/;
    private phoneRegexPattern: RegExp = /\d{3} ?\d{3} ?\d{2} ?\d{2}$/;
    private pageTitle: string = "";
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    form: FormGroup;
    roles: any[];
    retrievedRoles: any[];
    userID: string;
    contactID: string;
    user: any;
    isEditMode: boolean;
    formReady: boolean;
    isClient: boolean;
    actions: any[];
    customers: any[];
    customersDialogConfig: any;
    actionControl = new FormControl();

    constructor(private fb: FormBuilder,
        public dialog: MatDialog,
        private router: Router,
        private pageTitleService: PageTitleService,
        private translate: TranslateService,
        private route: ActivatedRoute,
        private settingsService: SettingsService,
        private toastr: ToastrService) {
        this.isEditMode = false;
        this.formReady = false;
        this.isClient = false;
        this.actions = [
            { id: 1, name: 'Setup client' },
            { id: 2, name: 'Disable Google Authenticator' },
            { id: 3, name: 'Disable MobileID' },
            { id: 4, name: 'Reset wrong password counter' }
        ];
        this.customers = [];
        this.customersDialogConfig = {
            width: '800px',
        };
    }


    /**
     * Angular Lifecycle hook
     */
    ngOnInit(): void {
        this.resolveURLSergments(this.route.snapshot.url);
        this.pageTitleService.setTitle(this.pageTitle);
        let id = this.route.snapshot.paramMap.get("id");
        this.resolveIDs(id);
        this.createForm(this.userID);
        this.modifyFormState();
        this.getAllRoles()
            .then(() => {
                this.getUserData();
            });
    }

    /**
     * Remove customer chip from input
     * @param contact 
     */
    removeCustomer(contact: any): void {
        this.customers = this.customers.filter(item => item.id != contact.id);
    }

    /**
     * Based on flag isClient assign value of ID parameter to one of the class properties
     * @param id 
     */
    resolveIDs(id: string): void {
        if (this.isClient) {
            this.contactID = id;
        } else {
            this.userID = id;
        }
    }

    /**
     * Actions dropdown event (on change) handler
     * @param action 
     */
    executeAction(action): void {
        if (action) {
            switch (action.id) {
                case 1:
                    break;
                case 2:
                    this.disableGoogleAuth();
                    break;
                case 3:
                    this.disableMobileID();
                    break;
                case 4:
                    this.resetWrongPasswordCounter();
                    break;
            }
        }
    }

    /**
     * Reset action dropdown after operation is executed
     */
    resetActionDropdown() {
        this.actionControl.reset();
        let activeElement: any = document.activeElement;
        activeElement.blur();
    }

    /**
     * Disable google authenticator feature
     */
    private disableGoogleAuth(): void {
        this.settingsService
            .setGoogleAuth(this.userID, false)
            .subscribe(() => {
                this.toastr.success('Google Authenticator was successfully disabled for the user');
            }).add(this.resetActionDropdown.bind(this))
    }

    /**
     * Disable mobile id feature
     */
    private disableMobileID(): void {
        this.settingsService
            .setMobileIDAuth(this.userID, false)
            .subscribe(() => {
                this.toastr.success('Mobile ID was successfully disabled for the user');
            })
            .add(this.resetActionDropdown.bind(this))
    }

    /**
     * Reset wrong password counter (field inside user object)
     */
    private resetWrongPasswordCounter(): void {
        let payload = Object.assign({}, this.user);
        payload.wrongPasswordCounter = 0;
        this.settingsService
            .updateUsers(payload, this.userID)
            .subscribe(this.onUserUpdated.bind(this))
            .add(this.resetActionDropdown.bind(this))
    }

    /**
     * Based on url segments (elements taken from ActivatedRoute snapshot)
     * determine context of form (edit, new, new user with customers etc.)
     * @param urlSegments 
     */
    private resolveURLSergments(urlSegments): void {
        if (urlSegments[1].path == "new") {
            this.pageTitle = "new_user";
        } else {
            this.pageTitle = "edit_user";
            this.isEditMode = true;
        }
        if (urlSegments.length === 3) {
            this.isClient = true;
        }
    }

    /**
     * Disable roles if form is opened from
     * one of the contact pages
     */
    private modifyFormState(): void {
        if (this.isClient) {
            this.form.controls["roles"].disable();
        }
    }

    /**
     * Create form
     * @param userId 
     */
    private createForm(userId): void {
        let passwordValidators = [
            Validators.minLength(8),
            Validators.maxLength(999),
            Validators.pattern(this.passwordRegexPattern)
        ];
        // In case of new user, password is required
        if (!userId) {
            passwordValidators.push(Validators.required);
        }
        const password = new FormControl(null, passwordValidators);
        const confirmPassword = new FormControl(null, CustomValidators.equalTo(password));
        this.form = this.fb.group({
            username: [null, [Validators.required]],
            active: [null],
            firstName: [null, [Validators.required]],
            lastName: [null, [Validators.required]],
            email: [null, [Validators.required, CustomValidators.email]],
            mobileNumber: [null, [Validators.required, Validators.pattern(this.phoneRegexPattern)]],
            roles: [''],
            password: password,
            confirmPassword: confirmPassword,
            crmUserId: [null]
        });
    }

    /**
     * Empty password fields
     */
    private emptyPasswordFields(): void {
        this.form.controls['password'].setValue('');
        this.form.controls['confirmPassword'].setValue('');
    }

    /**
     * On form change sort selected roles
     * @param event 
     */
    onRoleChange(event) {
        let selectedRoles = this.form.controls.roles.value;
        this.form.controls.roles.setValue(this.sortRoles(selectedRoles));
    }

    /**
     * Fetch roles from server (ng-select items)
     */
    private getAllRoles(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.settingsService
                .getRoles()
                .subscribe((roles: any) => {
                    if (roles.rows) {
                        this.retrievedRoles = this.sortRoles(roles.rows);
                        this.roles = roles.rows.filter(role => role.name);
                        resolve(true);
                    }
                }, reject);
        });
    }

    /**
     * Process ID from URL (if there is any)
     */
    private getUserData(): void {
        if (this.contactID) {
            this.settingsService
                .getContactById(this.contactID)
                .subscribe(contact => {
                    this.populateFormWithContact(contact);
                });
        } else if (this.userID) {
            this.settingsService
                .getUserById(this.userID)
                .subscribe(userObject => {
                    this.populateForm(userObject, false);
                })
        } else {
            this.formReady = true;
        }
    }

    /**
     * Sort roles by name
     * @param roles 
     */
    sortRoles(roles) {
        let array = Object.assign([], roles).filter(role => role.name);
        array.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : ((b.name.toLowerCase() > a.name.toLowerCase()) ? -1 : 0));
        return array;
    }

    /**
     * Process contact object
     * Copy essential properties from contact to new user
     * @param contact 
     */
    private populateFormWithContact(contact): void {
        if (contact && contact.id) {
            this.customers.push(contact);
        }
        let rolesToSelect = [];
        this.retrievedRoles.forEach(role => {
            if (role.name === 'CLIENT' || role.name === 'USER') {
                rolesToSelect.push(Object.assign({}, role));
            }
        });
        this.form.patchValue({
            username: contact.email,
            active: true,
            firstName: contact.surname,
            lastName: contact.name,
            email: contact.email,
            mobileNumber: contact.mobilePhoneNo,
            roles: rolesToSelect
        });
        this.formReady = true;
    }

    /**
     * Apply users data to the form
     * @param user 
     * @param skipContacts 
     */
    private populateForm(user: any, skipContacts): void {
        this.user = user;
        let roles = [];
        if (user.roles) {
            roles = this.sortRoles(user.roles)
        }
        if (!skipContacts) {
            this.isClient = user.roles.filter(role => role.name === 'CLIENT').length != 0;
            if (user.accesspermission) {
                user.accesspermission.customer.forEach(customer => {
                    this.customers.push(customer);
                });
            }
        }
        this.form.patchValue({
            username: user.username,
            active: user.active,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            mobileNumber: user.mobileNumber,
            roles: roles,
            crmUserId: user.crmUserId
        });
        this.formReady = true;
    }

    /**
     * Save button event handler
     */
    onSubmit(): void {
        let roles = this.prepareRoles();
        let user = this.createUserObject(roles);
        this.usernameExists(user.username, (result) => {
            if (result === 1) {
                this.toastr.warning(this.translate.instant('username_already_in_use'));
            } else if (result === 0) {
                this.proceedWithSave(user);
            } else {
                console.log("Error occured during username check");
            }
        })
    }

    /**
     * Finds source object for each selected role
     */
    private prepareRoles(): Array<any> {
        let roles = this.form.getRawValue().roles;
        if (roles) {
            return roles;
        }
        return [];
    }

    /**
     * Show some frendly message to user after we update record
     * @param user 
     */
    private onUserUpdated(user): void {
        this.emptyPasswordFields();
        this.populateForm(user, true);
        let message = this.translate.instant('saved');
        this.toastr.success(message + ' ' + user.username);
    }

    private proceedWithSave(user): void {
        if (this.userID) {
            this.settingsService
                .updateUsers(user, this.userID)
                .subscribe((userObject: any) => {
                    let customerIDsObject = {
                        customerIds: this.customers.map(contact => contact.id)
                    };
                    this.settingsService
                        .updateCustomerList(userObject.accesspermission.id, customerIDsObject)
                        .subscribe(() => {
                            this.onUserUpdated(userObject);
                        })
                }, (error) => {
                    this.toastr.error('Error occured during update.');
                });
        } else {
            this.createNewUser(user);
        }
    }

    /**
     * Check if username is already in use
     * @param username 
     * @param callback 
     */
    private usernameExists(username, callback): void {
        if (!this.user || (this.user && this.user.username !== username)) {
            this.settingsService
                .checkIfUserExist(username)
                .subscribe((result) => {
                    if (!result) {
                        callback(1);
                        return;
                    }
                }, (error) => {
                    if (error.status === 404 && error.statusText == "OK") {
                        callback(0);
                        return;
                    } else {
                        callback(-1);
                        return;
                    }
                });
        } else {
            callback(0);
        }
    }

    /**
     * Copy, prepare data for save / update
     * @param roles 
     */
    private createUserObject(roles): any {
        console.log("Creating user: with roles", roles);
        let user: any = Object.assign({}, this.user);
        for (let key in this.form.value) {
            user[key] = this.form.value[key];
        }
        user.roles = roles;
        if (this.user && !user.password) {
            user.password = this.user.password;
        }
        return user;
    }

    /**
     * Map selected customers into acceptable format and then create new user
     * @param user 
     */
    private createNewUser(user): void {
        user.accesspermission = {
            customer: this.customers.map((contact) => {
                return {
                    id: contact.id
                }
            })
        }
        this.settingsService
            .createUser(user)
            .subscribe((result: any) => {
                let message = this.translate.instant('inserted_new_user');
                this.toastr.success(message);
                this.router.navigate(['/settings/ac/users/', result.id]);
            }, (error) => {
                console.error(error);
            })
    }

    /**
     * Go back
     */
    goBack(): void {
        window.history.back();
    }

    /**
     * Open dialog with list of customers,
     * allowing current user to pick one of them
     */
    openCustomerDialog(): void {
        const dialogRef = this.dialog.open(CustomersListComponent, this.customersDialogConfig);
        dialogRef.afterClosed()
            .subscribe(result => {
                if (result && !this.customers.filter(item => item.id === result.id).length) {
                    this.customers.push(result);
                }
            });
    }

}
