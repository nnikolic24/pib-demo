// Angular imports
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

// Application imports
import { PageTitleService } from '../../core/page-title/page-title.service';
import { SettingsService } from '../../service/settings/settings.service';

@Component({
    selector: 'ms-user-administration',
    templateUrl: './user-administration.component.html',
    styleUrls: ['./user-administration.component.scss']
})
export class UserAdministrationComponent implements OnInit {

    rows: any[];
    columns: any[];
    originalArray = [];

    constructor(private pageTitleService: PageTitleService,
        private router: Router,
        public setttingsService: SettingsService) {
    }

    ngOnInit() {
        this.pageTitleService.setTitle("users");
        this.getData();
        this.setttingsService.getAccessUrles();
    }

    getData() {
        this.setttingsService
            .getAllUsers()
            .subscribe(data => {
                this.originalArray = this.setttingsService.parseUsers(data);
                this.rows = this.setttingsService.parseUsers(data);
            }, error => {
                console.error(error);
            });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.originalArray.filter((item) => {
            return item.id.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    onActivate(event) {
        if (event.type == 'click') {
            this.router.navigate(['/settings/ac/users/', event.row.id]);
        }
    }

}
