import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
   providedIn: 'root'
})
export class UserAdministrationService {

   constructor(private http: HttpClient) { }

   /**
    * Get users from users endpoint
    */
   fetchUsers(url) {
      return this.http.get(url);
   }

   /**
    * parseData will receive data from server
    * and use only columns that needed
    */
   parseData(result) {
      // Empty array in which will be stored mapped data
      let data: any = [];

      // Check if result exists and map to column names
      if (result && result.rows) {
         result.rows.forEach(row => {
            // Set roles to empty string for each new row
            let roleNames: String = "";
            // Go through all roles and create string
            if (row.roles && row.roles.length != 0) {
               row.roles.forEach((role, index) => {
                  if (index == 0) {
                     roleNames += String(role.name).toUpperCase();
                  }
                  else {
                     roleNames += " " + String(role.name).toUpperCase();
                  }
               });
            }
            data.push({
               "UserID": row.id,
               "Username": row.username,
               "Roles": roleNames
            });
         });
      }

      // Return mapped data
      return data;
   }

   /**
    * getUserById function will receive user id and
    * retrieve from endpoint
    */
   getUserById(url) {
      return this.http.get(url);
   }


   /*
    * parseDataOnUserAdminInit will receive data from user-administration.component
    * and use only columns from json that needed
    */
   parseDataOnUserAdminInit(result) {
      // Empty array in which will be stored mapped data
      let data: any = [];

      // Check if result exists and map to column names
      if (result && result.rows) {
         result.rows.forEach(row => {
            // Set roles to empty string for each new row
            let roleNames: String = "";
            // Go through all roles and create string
            if (row.roles && row.roles.length != 0) {
               row.roles.forEach((role, index) => {
                  if (index == 0) {
                     roleNames += String(role.name).toUpperCase();
                  }
                  else {
                     roleNames += " " + String(role.name).toUpperCase();
                  }
               });
            }
            data.push({
               "UserID": row.id,
               "Username": row.username,
               "Roles": roleNames
            });
         });

      }

      // Return mapped data
      return data;
   }

   /**
    * Update existing user
    */
   updateUsers(url,user) {
      return this.http.put(url,user);
   }

   /**
    * Add new user
    */
   addUsers(url,user) {
      return this.http.post(url,user);
   }


   /**
    * fetchConfigValues will retrieve all user preferences from endpoint
    * @param url endpoint address
    */
   fetchConfigValues(url){
      return this.http.get(url);
   }
   
   /**
    * checkIfUserExist method will receive url and user id 
    * and return observable
    * @param url endpoint url
    * @param userId user id
    */
   checkIfUserExist(url){
      return this.http.head(url);
   }
   
   /**
    * createUser method will receive url and user and return 
    * observable
    * @param url endpoint url 
    * @param user new user object
    */
   createUser(url,user){
      return this.http.post(url,user);
   }

}
