// Angular imports
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// 3rd party imports
import { TranslateService } from '@ngx-translate/core';

// Application imports
import { EntitySearchService } from './../../../service/core/entity-search.service';
import { ContactsService } from './../../../contacts/contacts.service';

@Component({
    selector: 'ms-customers-list',
    templateUrl: './customers-list.component.html'
})
export class CustomersListComponent implements OnInit {

    @Output() customerSelected = new EventEmitter();

    preparedSearchParams: any;
    totalNumberOfElemetns: number = 0;
    searchQuery: string = "";
    contactsArray = [];
    columns = [];

    constructor(
        public dialogRef: MatDialogRef<CustomersListComponent>,
        private translate: TranslateService,
        private entitySearchService: EntitySearchService,
        private contactsService: ContactsService) {
    }

    /**
     * Angular Lifecycle hook
     */
    ngOnInit(): void {
        // Translate columns
        this.callTransalteService();
        // Prepare and execute search
        this.prepareEntitySearch();
    }

    /**
     * Translate datatable columns
     */
    private callTransalteService(): void {
        this.columns = [
            { name: this.translate.instant("kontakt_name"), prop: 'fullName' },
            { name: this.translate.instant("kontakt_adresse"), prop: 'adresseFull' },
            { name: this.translate.instant("kontakt_email"), prop: 'email' },
            { name: this.translate.instant("standort"), prop: 'location' },
        ];
    }

    /**
     * Prepare and execute search
     * Executed also when page change
     * @param getPage Paginator event
     */
    prepareEntitySearch(getPage?) {
        let createEntitySearch: any = {};
        createEntitySearch.entity = "contacts";
        createEntitySearch.paging = { sortColumns: "name" };
        createEntitySearch.predicateSource = {
            hasName: "%" + this.searchQuery + "%",
        };
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "!isDeleted", null);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "isKunde", null);
        this.preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);
        if (getPage) {
            this.preparedSearchParams.paged.page = getPage.page;
        }
        this.getData(this.preparedSearchParams);
    }

    /**
     * Fetch data based on parepared parameters
     * @param preparedSearchParams 
     */
    getData(preparedSearchParams) {
        this.contactsService
            .fetchContactsPersons(preparedSearchParams)
            .subscribe((data: any) => {
                this.contactsArray = this.contactsService.parseData(data);
                this.totalNumberOfElemetns = data.total;
            });
    }

    /**
     * Handle event from datatable
     * @param event 
     */
    onActivate(event) {
        if (event.type == 'click') {
            this.dialogRef.close(event.row);
        }
    }

    /**
     * Search button event handler
     */
    filterBy() {
        let firstPage = { page: 1 }
        this.prepareEntitySearch(firstPage);
    }

}
