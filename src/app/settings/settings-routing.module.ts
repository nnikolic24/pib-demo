import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserAdministrationComponent } from './user-administration/user-administration.component';
import { RolePermissionsComponent } from './role-permissions/role-permissions.component';
import { PeripheralSystemsComponent } from './peripheral-systems/peripheral-systems.component';
import { ConfigValuesComponent } from './config-values/config-values.component';
import { NewUserComponent } from './user-administration/new-user/new-user.component';
import { NewRoleComponent } from './role-permissions/new-role/new-role.component';
import { EditRoleComponent } from './role-permissions/edit-role/edit-role.component';

const routes: Routes = [{
  path: 'ac',
  children: [
    {
      path: 'users',
      component: UserAdministrationComponent
    },
    {
      path: 'roles',
      component: RolePermissionsComponent
    },
    {
      path: 'configvalues',
      component: ConfigValuesComponent
    },
    {
      path: 'peripherals',
      component: PeripheralSystemsComponent
    },
    {
      path: 'users/new',
      component: NewUserComponent
    },
    {
      path: 'users/:id',
      component: NewUserComponent
    },
    {
      path: 'users/new/:id',
      component: NewUserComponent
    },
    {
      path: 'roles/new',
      component: NewRoleComponent
    },
    {
      path: 'roles/:id',
      component: EditRoleComponent
    },

  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
