import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../core/route-animation/route.animation";
import { TranslateService } from '@ngx-translate/core';
import { RoleService } from '../../service/core/role.service';
import { CoreService } from '../../service/core/core.service';
import { UserAdministrationService } from '../user-administration/user-administration.service';

import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { ConfigValueService } from './config-value.service';

import { ToastrService } from 'ngx-toastr';


@Component({
   selector: 'ms-config-values',
   templateUrl: './config-values.component.html',
   styleUrls: ['./config-values.component.scss'],
   encapsulation: ViewEncapsulation.None,
   host: {
      "[@fadeInAnimation]": 'true'
   },
   animations: [fadeInAnimation]
})
export class ConfigValuesComponent implements OnInit {
   // rows will store data for ngx-datatable
   rows = [];

   // roles array will hold all roles
   roles: any = [];

   // configValues array will hold all user preferences
   configValues: any = [];

   // columns for ngx-datatable configuration
   columns = [];

   // temp array for filter purpose
   temp = [];

   // main form
   form: FormGroup;
   // dynamic form controls array
   items: FormArray;

   constructor(private pageTitleService: PageTitleService,
      private translate: TranslateService,
      private roleSvc: RoleService,
      private coreSvc: CoreService,
      private userAdminSvc: UserAdministrationService,
      private formBuilder: FormBuilder,
      private configSvc:ConfigValueService,
      private toastr:ToastrService
   ) {

   }

   ngOnInit() {
      // call translate function
      this.callTranslateService();
      // set empty form controls array
      this.form = this.formBuilder.group({
         items: this.formBuilder.array([])
      });

      // retrieve data from endpoint 
      this.fetchData();
   }

   /**
    * Used for translating text that's provided through code
    * Set initial translation for page title, and further translate on lang change
    */
   callTranslateService() {
      // Translate page title
      this.pageTitleService.setTitle(this.translate.instant("SETTINGS_CONFIG_VALUES"));

      //On lang change transalte page title
      this.translate.onLangChange.subscribe((event: any) => {
         // Translate page title
         this.pageTitleService.setTitle(this.translate.instant("SETTINGS_CONFIG_VALUES"));

         // columns needed for ngx-datatable columns to connect with object properties
         this.columns = [
            { name: this.translate.instant("config_key"), prop: 'configKey' },
            { name: this.translate.instant("configuration_value"), prop: 'configValue' },
            { name: this.translate.instant("allow_override_for"), prop: 'allowOverrideFor' }
         ];
      });
   }

   /**
    * addItem method will add new dynamic form control to controls array
    * @param value 
    */
   addItem(value: any): void {
      this.items = this.form.get('items') as FormArray;
      this.items.push(this.formBuilder.control(value));
   }

   /**
    * update method will receive id and index as a parameter, 
    * call endpoint api and send object for update       
    * @param id config values id
    * @param index row index that will be used finding form control array
    */
   update(id,index) {      
      // get updated row from config values array
      let row:any = this.rows.filter(function(event){return event.id == id});
      // get all form controls
      this.items = this.form.get('items') as FormArray;
      // set new roles to the current row
      row[0].allowedOverridingRoles = this.roles.filter((event) => { return this.items.controls[index].value.indexOf(event.id)!==-1});

      // define empty config object 
      // and add data that will be send to endpoint
      // for update
      let configObj:any = {}
      configObj.id = row[0].id;
      configObj.configValue = row[0].configValue;
      configObj.configKey = row[0].configKey;
      configObj.allowedOverridingRoles = row[0].allowedOverridingRoles;

      console.log('Object to send:',configObj);

      // call endpoint and send updated config value
      this.configSvc.updateConfigValue(this.coreSvc.base_url + "/ws/rest/userpreferences/configvalues/" + configObj.id, configObj)
          .subscribe((result:any)=>{
            console.log('Received result after update:',result);
            this.toastr.success(this.translate.instant("row_updated_successfully"));
          },
          (error)=>{
            console.log('Error occured:',error.message);
          });
   }

   /**
    * retrieve data from config values endpoint
    */
   fetchData(){
        // get all roles from role endpoint
        this.roleSvc.fetchRoles(this.coreSvc.base_url + "/ws/rest/roles")
        .subscribe((data) => {
           // set response to rows array 
           this.roles = data["rows"];
           console.log('Received roles:', this.roles);
           // get all config values       
           this.userAdminSvc.fetchConfigValues(this.coreSvc.base_url + "/ws/rest/userpreferences/configvalues")
              .subscribe((data:any) => {
                 console.log('Config values:',data.rows);
                 // set response to rows array 
                 this.rows = data.rows;
                 // set response to array for filtering
                 this.temp = data.rows;
                 // go throught all chosen roles and add to empty array
                 // that will hold id of chosen roles
                 if (this.rows && this.rows.length != 0) {
                    this.rows.forEach(row => {
                       let roleArray: any = [];
                       if (row.allowedOverridingRoles && row.allowedOverridingRoles.length != 0) {
                          row.allowedOverridingRoles.forEach(allowedRole => {
                             roleArray.push(allowedRole.id);
                          });
                       }
                       this.addItem(roleArray);
                    });
                 }

              },
                 (error) => {
                    console.log('Error occured in config-value.component while retrieving config values:', error.message);
                 });
        },
           (error) => {
              console.log('Error occured in config-value.component while retrieving all roles:', error.message);
           });
   }

}


