import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigValueService {

  constructor(private http:HttpClient) { }

  /**
   * updateConfigValue method will receive url and data
   * to update and return observable
   * @param url 
   * @param data 
   */
  updateConfigValue(url,data){
    return this.http.put(url,data);
  }
}
