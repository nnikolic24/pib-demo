import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
    selector: 'ms-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

    title: string;
    data: string;

    constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>) {
    }

    ngOnInit() {
    }

    // yes method is used to close the delete dialog and send the response "yes".
    yes() {
        this.dialogRef.close("yes");
    }
}
