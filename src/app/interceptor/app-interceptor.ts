import { Injectable } from "@angular/core";
import { Router } from '@angular/router'
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from '../service/auth-service/auth.service';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
  constructor(private authSvc: AuthService,
    private router: Router) { }
  //function which will be called for all http calls
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    //how to update the request Parameters
    const updatedRequest = request.clone({
      headers: request.headers.set("X-Requested-With", "XMLHttpRequest"),
      withCredentials: true
    });
    //logging the updated Parameters to browser's console
    //  console.log("Before making api call : ", updatedRequest);
    return next.handle(updatedRequest).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.status == 403) {
            // clear user from local storage,
            // set service flag isLoggedIn to false (security check)
            // and redirect to login 
            localStorage.removeItem("userProfile");
            this.authSvc.isLoggedIn = false;
            this.router.navigate(['/login']);
          }
          else {
            // console.log('Success');
          }
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 404 && error.statusText == "OK") {
          return throwError(error);
        }
        // clear user from local storage,
        // set service flag isLoggedIn to false (security check)
        // and redirect to login 
        localStorage.removeItem("userProfile");
        this.authSvc.isLoggedIn = false;
        this.router.navigate(['/login']);
        return throwError(error);
      }));

  }
}