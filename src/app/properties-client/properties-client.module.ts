import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatButtonModule, MatDividerModule, MatSelectModule, MatTabsModule, MatIconModule, MatDatepickerModule, MatRadioModule, MatCheckboxModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PropertiesClientComponent } from './properties-client/properties-client.component';
import { propertiesClientRoutes } from './properties-client.routing';

@NgModule({
  declarations: [PropertiesClientComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    NgxDatatableModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatSelectModule,
    TranslateModule,
    FlexLayoutModule,
    MatTabsModule,
    MatIconModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCheckboxModule,
    RouterModule.forChild(propertiesClientRoutes),
  ]
})
export class PropertiesClientModule { }
