import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PropertiesClientComponent } from './properties-client/properties-client.component';

export const propertiesClientRoutes: Routes = [
   {
      path: '',
      redirectTo: 'properties-client',
      pathMatch: 'full'
   },
   {
      path: '',
      children: [
         {
            path: 'properties-client',
            component: PropertiesClientComponent
         }
      ]
   }
];

@NgModule({
   imports: [RouterModule.forChild(propertiesClientRoutes)],
   exports: [RouterModule]
 })
 export class PropertiesClientRoutingModule { }