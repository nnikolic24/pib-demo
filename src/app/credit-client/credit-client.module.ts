import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatButtonModule, MatDividerModule, MatSelectModule, MatTabsModule, MatIconModule, MatDatepickerModule, MatRadioModule, MatCheckboxModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { TotalLoansClientComponent } from './total-loans-client/total-loans-client.component';
import { InterestRateSwapsClientComponent } from './interest-rate-swaps-client/interest-rate-swaps-client.component';
import { creditClientRoutes, CreditClientRoutingModule } from './credit-client-routing.module';

@NgModule({
  declarations: [TotalLoansClientComponent, InterestRateSwapsClientComponent],
  imports: [
    CommonModule,
    CreditClientRoutingModule,
    MatFormFieldModule,
    NgxDatatableModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatSelectModule,
    TranslateModule,
    FlexLayoutModule,
    MatTabsModule,
    MatIconModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCheckboxModule,
    RouterModule.forChild(creditClientRoutes),
  ]
})
export class CreditClientModule { }
