import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { Router } from '@angular/router';

@Component({
  selector: 'total-loans-client',
  templateUrl: './total-loans-client.component.html',
  styleUrls: ['./total-loans-client.component.scss']
})
export class TotalLoansClientComponent implements OnInit {

  rows = [];

  columns = [
    { name: 'Name', prop: 'Name' },
    { name: 'Customer', prop: 'Customer' }
  ];

  temp = [];


  constructor(private pageTitleService: PageTitleService, private router: Router) {
    this.fetch((data) => {
      // cache our list
      this.temp = data;

      // push our inital complete list
      this.rows = data;
      console.log("Data in rows", this.rows);
    });
  }

  ngOnInit() {
    this.pageTitleService.setTitle("Total loans");
  }

  /**
        * To fetch the data from JSON file.
        */
  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/properties.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  /**
    * updateFilter method is used to filter the data.
    */
  updateFilter(event) {
    const val = event.target.value;

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
  }

  /**
   * Function will execute on any row event
   * @param event Received event
   */
  onActivate(event) {
    // If user clicks on row it should redirect him to form with user data 
    if (event.type == 'click') {
      this.router.navigate(['/properties/', 1]);
    }
  }

}
