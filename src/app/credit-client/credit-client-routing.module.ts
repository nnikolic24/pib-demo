import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InterestRateSwapsClientComponent } from './interest-rate-swaps-client/interest-rate-swaps-client.component';
import { TotalLoansClientComponent } from './total-loans-client/total-loans-client.component';

export const creditClientRoutes: Routes = [
   {
      path: '',
      children: [
         {
            path: 'interest-rate-swaps',
            component: InterestRateSwapsClientComponent
         },
         {
            path: 'total-loans',
            component: TotalLoansClientComponent
         },
      ]
   }
];

@NgModule({
   imports: [RouterModule.forChild(creditClientRoutes)],
   exports: [RouterModule]
})
export class CreditClientRoutingModule { }