import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatTabsModule, MatFormFieldModule, MatButtonModule, MatDatepickerModule, MatCardModule, MatInputModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from 'app/shared/shared.module';

import { HomeClientComponent } from './home-client/home-client.component';
import { HomeClientRoutingModule } from './home-client-routing.module';

@NgModule({
  declarations: [HomeClientComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatIconModule,
    MatTabsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatCardModule,
    MatInputModule,
    FlexLayoutModule,
    HomeClientRoutingModule
  ]
})

export class HomeClientModule { }
