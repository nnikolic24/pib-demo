import { Component, OnInit, ViewChild } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';

import { InterestsComponent } from 'app/shared/interests/interests.component';
import { InterestTermComponent } from 'app/shared/interest-term/interest-term.component';
import { DueCreditsComponent } from 'app/shared/due-credits/due-credits.component';
import { LenderStructureComponent } from 'app/shared/lender-structure/lender-structure.component';
import { CreditGrowthComponent } from 'app/shared/credit-growth/credit-growth.component';

@Component({
  selector: 'home-client',
  templateUrl: './home-client.component.html',
  styleUrls: ['./home-client.component.scss']
})
export class HomeClientComponent implements OnInit {

  @ViewChild("tabInterests") tabInterests: InterestsComponent;
  @ViewChild("tabInterestTerm") tabInterestTerm: InterestTermComponent;
  @ViewChild("tabDueCredits") tabDueCredits: DueCreditsComponent;
  @ViewChild("tabLenderStructure") tabLenderStructure: LenderStructureComponent;
  @ViewChild("tabCreditGrowth") tabCreditGrowth: CreditGrowthComponent;

  constructor(private pageTitleService: PageTitleService) { }

  ngOnInit() {
    this.pageTitleService.setTitle("Home");
  }

  tabChanged(event) {
    switch (event.index) {
      case 1:
      this.tabInterests.initChart();
      break;
      case 2:
      this.tabInterestTerm.initChart();
      break;
      case 3:
      this.tabDueCredits.initChart();
      break;
      case 4:
      this.tabLenderStructure.initChart();
      break;
      case 5:
      this.tabCreditGrowth.initChart();
      break;
    }
  }

}
