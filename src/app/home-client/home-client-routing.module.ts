import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeClientComponent } from './home-client/home-client.component';

export const homeClientRoutes: Routes = [
   {
      path: '',
      redirectTo: 'home-client',
      pathMatch: 'full'
   },
   {
      path: '',
      children: [
         {
            path: 'home-client',
            component: HomeClientComponent
         }
      ]
   }
];

@NgModule({
   imports: [RouterModule.forChild(homeClientRoutes)],
   exports: [RouterModule]
 })
 export class HomeClientRoutingModule { }