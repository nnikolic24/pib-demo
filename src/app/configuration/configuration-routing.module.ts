import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InterestRateSeriesComponent } from './interest-rate-series/interest-rate-series.component';
import { DeactivatedComponent } from './deactivated/deactivated.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'interestrate',
      component: InterestRateSeriesComponent
    },
    {
      path: 'deactivated',
      component: DeactivatedComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
