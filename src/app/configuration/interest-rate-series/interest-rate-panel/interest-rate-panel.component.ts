// Angular imports
import {
    Component,
    EventEmitter,
    Output
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

// 3rd party imports
import { TranslateService } from '@ngx-translate/core';
import { BlockUIService } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';

// Application imports
import { ConfigurationService } from './../../../service/configuration/configuration.service';
import { CoreService } from './../../../service/core/core.service';

@Component({
    selector: 'ms-interest-rate-panel',
    templateUrl: './interest-rate-panel.component.html'
})
export class InterestRatePanelComponent {

    @Output() onEdit = new EventEmitter();
    @Output() onReload = new EventEmitter();

    form: FormGroup;
    interestRates = [];
    interestRateNames = {};
    disableOtherButtons: boolean = false;

    /**
     * Component constructor
     * @param formBuilder Service used to build reactive form
     * @param toastr Show notifications
     * @param blockUI Gives ability to show overlay while data is being fetched from server
     * @param translateService Translate labels and messages
     * @param coreService Utilities
     * @param configurationService Main service, handles API calls
     */
    constructor(private formBuilder: FormBuilder,
        private toastr: ToastrService,
        private blockUI: BlockUIService,
        private translateService: TranslateService,
        private coreService: CoreService,
        private configurationService: ConfigurationService) {

    }

    /**
     * Angular Lifecycle hook
     */
    ngOnInit() {
        this.form = this.formBuilder.group({
            name: [null],
        });
        this.getAllInterestRates();
    }

    /**
     * Get all interest rates for interest rates panel
     * Reference: ZinsReiheCtrl.js Line: 17
     */
    getAllInterestRates() {
        this.blockUI.start('main-block');
        this.configurationService
            .getInterestRates()
            .subscribe((data: any) => {
                this.blockUI.stop('main-block');
                if (data.rows) {
                    this.interestRates = data.rows.sort((a, b) => (a.name > b.name) ? 1 : -1);
                }
                this.interestRates.forEach(rate => {
                    this.interestRateNames[rate.name] = true;
                });
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translateService.instant('zins_reihe_loading_failed');
                this.toastr.error(message);
            })
    }

    reSortInterestRates() {
        this.interestRates = this.interestRates.sort((a, b) => (a.name > b.name) ? 1 : -1);
    }

    /**
     * Change mode from DISPLAY to EDIT
     * Reference: ZinsReiheCtrl.js Line: 104
     * @param rate 
     */
    edit(rate) {
        this.disableOtherButtons = true;
        rate.backupName = rate.name;
        rate.edit = true;
        this.onEdit.emit(rate);
    }

    /**
     * Undo changes and go back to DISPLAY mode
     * Reference: ZinsReiheCtrl.js Line: 126
     */
    close(rate) {
        this.disableOtherButtons = false;
        rate.edit = false;
        rate.name = rate.backupName;
        delete rate.backupName;
        this.onEdit.emit(null);
    }

    /**
     * Save changes
     * Reference: ZinsReiheCtrl.js Line: 109
     * @param rate 
     */
    save(rate) {
        if (!(rate.backupName === rate.name)) {
            if (this.isNotDuplicateName(rate.name)) {
                this.blockUI.start('main-block');
                this.configurationService
                    .updatRate(rate)
                    .subscribe((response: any) => {
                        delete this.interestRateNames[rate.backupName];
                        this.interestRateNames[rate.name] = true;
                        rate.edit = false;
                        delete rate.backupName;
                        this.disableOtherButtons = false;
                        this.blockUI.stop('main-block');
                        this.onReload.emit(true);
                        this.onEdit.emit(null);
                    });
            }
        } else {
            this.disableOtherButtons = false;
            rate.edit = false;
            delete rate.backupName;
            this.onEdit.emit(null);
        }
    }

    /**
     * Delete single rate
     * Reference: ZinsReiheCtrl.js Line: 132
     * @param rate 
     */
    delete(rate) {
        let title = `'${rate.name}' ` + this.translateService.instant('zins_reihe_loeschen_title');
        let message = this.translateService.instant('zins_reihe_loeschen_msg');
        this.coreService
            .confirmDialog(title, message)
            .subscribe((res) => {
                if (res === 'yes') {
                    this.blockUI.start('main-block');
                    this.configurationService
                        .deleteRate(rate)
                        .subscribe((response: any) => {
                            this.interestRates = this.removeRateById(rate.id);
                            delete this.interestRateNames[rate.name];
                            this.blockUI.stop('main-block');
                            this.onReload.emit(true);
                        }, error => {
                            this.blockUI.stop('main-block');
                            let message = this.translateService.instant('zins_reihe_loeschen_failed');
                            this.toastr.error(message);
                        });
                }
            });
    }

    /**
     * Remove rate by id from interest rate array
     * @param id number
     */
    removeRateById(id) {
        return this.interestRates.filter(rate => rate.id != id);
    }

    /**
     * Show warning if name of is already in use
     * Reference: ZinsReiheCtrl.js Line: 222
     * @param name string
     */
    isNotDuplicateName(name) {
        if (this.interestRateNames[name]) {
            let message = this.translateService.instant('zins_reihe_name_dublicate') + `'${name}'`;
            this.toastr.warning(message);
            return false;
        }
        return true;
    }

    /**
     * Add new record
     * Reference: ZinsReiheCtrl.js Line: 31
     */
    addNewRate() {
        let newName = this.form.value.name;
        if (!newName || !this.isNotDuplicateName(newName)) {
            return;
        }
        this.blockUI.start('main-block');
        this.configurationService
            .createNewRate(this.form.value)
            .subscribe((response: any) => {
                this.interestRates.push(response);
                this.reSortInterestRates();
                this.interestRateNames[response.name] = true;
                this.form.reset();
                this.blockUI.stop('main-block');
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translateService.instant('zins_reihe_save_failed');
                this.toastr.error(message);
            });
    }
}