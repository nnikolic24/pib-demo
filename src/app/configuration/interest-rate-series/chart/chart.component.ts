// Angular imports
import {
    Component,
    OnInit,
    EventEmitter,
    Input
} from '@angular/core';

// 3rd party imports
import { forkJoin } from 'rxjs';

// Application imports
import { ConfigurationService } from './../../../service/configuration/configuration.service';
import {
    LineChartMethods,
    LineChartInput,
    LineChartOutput,
    LineChartOutputEvent
} from './../../../shared/charts/line-chart/line-chart.misc';

@Component({
    selector: 'chart-rate-series',
    templateUrl: './chart.component.html'
})
export class ChartComponent implements OnInit {

    @Input() reload: EventEmitter<any>;
    chartInput = new EventEmitter<LineChartInput>();
    showChart = false;
    steppedLine: boolean = true;
    dataset = null;

    constructor(
        private configurationService: ConfigurationService
    ) { }

    ngOnInit() {
        this.reload
            .subscribe(signal => {
                this.loadData();
            })
        this.loadData();
    }

    getAllInterestRates() {
        let dataForChart: any = [];
        let waitList = [];
        this.configurationService
            .getInterestRates()
            .subscribe((data: any) => {
                data.rows.forEach(row => {
                    if (true) {
                        waitList.push(this.configurationService.getAllInterestSeries(row.id));
                        dataForChart.push({
                            data: [],
                            label: row.name
                        });
                    }
                });
                forkJoin(waitList)
                    .subscribe(interestSeries => {
                        for (let i = 0; i < dataForChart.length; i++) {
                            for (let j = 0; j < interestSeries[i].rows.length; j++) {
                                let el = interestSeries[i].rows[j];
                                dataForChart[i].data.push({
                                    t: el.date,
                                    y: el.interest
                                });
                            }
                        }
                        this.dataset = dataForChart;
                        if (!this.showChart) {
                            this.showChart = true;
                        } else {
                            this.updateDataset();
                        }

                    })
            })
    }

    loadData() {
        this.getAllInterestRates();
    }

    outputHandler(data: LineChartOutput) {
        let event = data.event;
        switch (event) {
            case LineChartOutputEvent.COMPONENT_READY:
                this.chartInput.emit({
                    method: LineChartMethods.INIT,
                    payload: {
                        dataset: this.dataset
                    }
                });
                break;
            // case LineChartOutputEvent.DATE_FILTER_CHANGED:
            //     console.log("FILTERS: ", data.payload);
            //     break;
        }

    }

    updateDataset() {
        this.chartInput.emit({
            method: LineChartMethods.UPDATE_DATA,
            payload: {
                dataset: this.dataset
            }
        });
    }
}
