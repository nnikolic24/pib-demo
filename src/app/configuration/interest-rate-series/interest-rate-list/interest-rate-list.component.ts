// Angular imports
import {
    Component,
    Input,
    EventEmitter,
    Output
} from '@angular/core';
import {
    FormControl,
    FormGroup,
    Validators,
    FormBuilder
} from '@angular/forms';

// 3rd party imports
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import { BlockUIService } from 'ng-block-ui';
import { TranslateService } from '@ngx-translate/core';

// Application imports
import { ConfigurationService } from './../../../service/configuration/configuration.service';
import { EntitySearchService } from './../../../service/core/entity-search.service';
import { CoreService } from './../../../service/core/core.service';

@Component({
    selector: 'ms-interest-rate-list',
    templateUrl: './interest-rate-list.component.html',
    styleUrls: ['./interest-rate-list.component.scss']
})
export class InterestRateListComponent {

    @Input() rate;
    @Output() onReload = new EventEmitter();

    rateSeries = [];
    fromDate = new FormControl();
    toDate = new FormControl();
    dbDateFormat = "YYYY-MM-DD";

    newRateSeries: FormGroup

    /**
     * Component constructor method
     * @param configurationService Main service, handles API calls
     * @param entitySearchHelper Helps with search
     * @param blockUI Gives ability to show overlay while date is being fetched from server
     * @param toastr Show messages
     * @param translateService Translate messages and labels
     * @param coreService Utility service
     */
    constructor(private configurationService: ConfigurationService,
        private entitySearchHelper: EntitySearchService,
        private blockUI: BlockUIService,
        private toastr: ToastrService,
        private formBuilder: FormBuilder,
        private translateService: TranslateService,
        private coreService: CoreService) {
        this.fromDate.setValue(moment().subtract(30, 'days').toDate());
        this.toDate.setValue(moment().add(30, 'days').toDate());
    }

    /**
     * Angular Lifecycle hook
     * 
     */
    ngOnInit() {
        this.blockUI.start('main-block');

        this.newRateSeries = this.formBuilder.group({
            date: ['', [Validators.required]],
            addForDays: ['', [Validators.required, Validators.min(1)]],
            interest: ['', [Validators.required, Validators.min(-100), Validators.max(100)]]
        });

        this.search(true);
    }

    /**
     * Create multiple recods based on selected date, number of days and percentage
     * Reference: ZinsReiheCtrl.js Line: 146
     */
    create() {
        let waitList = [];
        let entryToAdd;
        let lastCreatedDate;

        let formData = this.newRateSeries.value;

        let template = {
            date: formData.date,
            interest: formData.interest,
            parent: {
                id: this.rate.id,
                date: moment().format(this.dbDateFormat)
            }
        }

        this.blockUI.start('main-block');
        for (let i = 0; i < formData.addForDays; i++) {
            entryToAdd = Object.assign({}, template);
            lastCreatedDate = moment(template.date)
                .add(i, 'days')
                .format(this.dbDateFormat);
            entryToAdd.date = lastCreatedDate;
            waitList.push(this.configurationService.createRateEntry(this.rate.id, entryToAdd));
        }
        forkJoin(waitList)
            .subscribe(result => {
                let nextDate = moment(formData.date).add(formData.addForDays, 'days').toDate();

                if (this.fromDate.value && moment(this.fromDate.value).isSameOrAfter(lastCreatedDate)) {
                    this.fromDate.setValue(moment(lastCreatedDate).toDate());
                }
                if (this.toDate && moment(this.toDate.value).isSameOrBefore(lastCreatedDate)) {
                    this.toDate.setValue(moment(lastCreatedDate).toDate())
                }

                this.newRateSeries.patchValue({
                    addForDays: 1,
                    date: nextDate
                });

                this.search(false);
                this.blockUI.stop('main-block');
                this.onReload.emit(true);
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translateService.instant('zins_reihe_eintraege_loading_failed');
                this.toastr.error(message);
            })
    }

    /**
     * Delete record by ID
     * Reference: ZinsReiheCtrl.js Line: 215
     * @param recordID Record unique ID
     */
    deleteByID(recordID) {
        this.blockUI.start('main-block');
        this.configurationService
            .deleteRateEntry(this.rate.id, recordID)
            .subscribe(() => {
                this.onReload.emit(true);
                this.search(false);
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translateService.instant('zins_reihe_eintraege_loeschen_failed');
                this.toastr.error(message);
            })
    }

    /**
     * Save changes, update record by ID
     * Reference: ZinsReiheCtrl.js Line: 199
     * @param row Interest series record
     */
    update(row) {
        this.blockUI.start('main-block');
        row.parent = {
            id: this.rate.id
        }
        this.configurationService
            .updateRateEntry(this.rate.id, row)
            .subscribe(() => {
                delete row.backupInterest;
                delete row.edit;
                this.search(false);
                this.blockUI.stop('main-block');
                this.onReload.emit(true);
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translateService.instant('zins_reihe_eintraege_save_failed');
                this.toastr.error(message);
            })
    }

    /**
     * Allow used to edit interest series
     * @param row 
     */
    onEditInterestSeries(row) {
        row.backupInterest = row.interest;
        row.edit = true;
    }

    /**
     * Cancel changes and lock percentage field
     * @param row 
     */
    onCancelInterestSeries(row) {
        row.edit = false;
        row.interest = row.backupInterest;
        delete row.backupInterest;
    }

    /**
     * Search for interest series records
     */
    search(correctDate) {
        let searchRange = null;
        if (!this.rate || this.rate === null) {
            return;
        }
        let searchSetup: any = {};
        searchSetup.pathParams = { parentId: this.rate.id };
        let entitySearchRates = this.entitySearchHelper.createEntitySearch(searchSetup);
        searchRange = this.coreService.closedRange(this.fromDate.value, this.toDate.value);
        entitySearchRates = this.entitySearchHelper.pushPredicate(entitySearchRates, 'datumIsInRange', searchRange);
        let params = this.entitySearchHelper.prepareParams(entitySearchRates);
        this.configurationService
            .search(params)
            .subscribe((data: any) => {
                this.rateSeries = this.sortRecordsByDate(data.rows);
                if (correctDate) {
                    if (this.rateSeries.length > 0) {
                        let nextDate = moment(this.rateSeries[0].date).add(1, 'days').toDate();
                        this.newRateSeries.controls['date'].setValue(nextDate);
                    } else {
                        this.newRateSeries.controls['date'].setValue(new Date());
                    }
                }
                this.blockUI.stop('main-block');
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translateService.instant('zins_reihe_eintraege_loading_failed');
                this.toastr.error(message);
            });
    }

    /**
     * Sort array of object by date property, desc
     * @param array 
     */
    sortRecordsByDate(array) {
        return array.sort((a, b) => {
            let dateA: any = new Date(a.date);
            let dateB: any = new Date(b.date);
            return dateB.valueOf() - dateA.valueOf();
        });
    }

    /**
     * Search button event handler
     */
    onSearch() {
        this.blockUI.start('main-block');
        this.search(false);
    }
}
