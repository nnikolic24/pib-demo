// Angular imports
import {
    Component,
    OnInit,
    EventEmitter
} from '@angular/core';

// Application imports
import { PageTitleService } from '../../core/page-title/page-title.service';

@Component({
    selector: 'ms-interest-rate-series',
    templateUrl: './interest-rate-series.component.html',
    styleUrls: ['./interest-rate-series.component.scss']
})
export class InterestRateSeriesComponent implements OnInit {

    selectedRate = null;
    emitter = new EventEmitter();

    editRate(rate) {
        this.selectedRate = rate;
    }

    constructor(
        private pageTitleService: PageTitleService
    ) { }

    ngOnInit() {
        this.pageTitleService.setTitle("zins_reihe");
    }

    loadData() {
        this.emitter.emit(true);
    }

}
