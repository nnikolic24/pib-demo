import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationRoutingModule } from './configuration-routing.module';
import { InterestRateSeriesComponent } from './interest-rate-series/interest-rate-series.component';
import { InterestRatePanelComponent } from './interest-rate-series/interest-rate-panel/interest-rate-panel.component';
import { InterestRateListComponent } from './interest-rate-series/interest-rate-list/interest-rate-list.component';
import { DeactivatedComponent } from './deactivated/deactivated.component';
import { ChartComponent } from './interest-rate-series/chart/chart.component';

import { MatFormFieldModule, MatInputModule, MatCardModule, MatButtonModule, MatIconModule, MatListModule, MatDatepickerModule, MatCheckboxModule, MatTableModule, MatTabsModule, MatNativeDateModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from "@angular/material";
import { AlertDialogComponent } from './interest-rate-series/alert-dialog/alert-dialog.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { DirectivesModule } from './../shared/directives/directives.module';
import { ChartsModule } from './../shared/charts/charts.module';
import { SharedModule } from './../shared/shared.module';

@NgModule({
    declarations: [
        InterestRateSeriesComponent,
        DeactivatedComponent,
        AlertDialogComponent,
        InterestRatePanelComponent,
        InterestRateListComponent,
        ChartComponent],
    imports: [
        DirectivesModule,
        SharedModule,
        CommonModule,
        ConfigurationRoutingModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatTableModule,
        MatTabsModule,
        NgxDatatableModule,
        ChartsModule,
        FormsModule,
        HttpClientModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        MatDialogModule,
        FormsModule,
        TranslateModule
    ]
})
export class ConfigurationModule { }
