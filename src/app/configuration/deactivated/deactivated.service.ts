import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreService } from '../../service/core/core.service';
import { environment } from 'environments/environment'

@Injectable({
    providedIn: 'root'
})
export class DeactivatedService {

    constructor(private http: HttpClient, private coreSvc: CoreService) { }


    getTotalLoans(predicate) {
        let predicated = encodeURIComponent(JSON.stringify(predicate.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/totalloans/search?predicate=${predicated}`);
    }

    getFinancingObject(predicate) {
        let predicated = encodeURIComponent(JSON.stringify(predicate.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/financingobjects/search?predicate=${predicated}`);
    }

    getInterestRateSwaps(predicate) {
        let predicated = encodeURIComponent(JSON.stringify(predicate.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/interestRateSwaps/search?predicate=${predicated}`);
    }

    getMortgages(predicate) {
        let predicated = encodeURIComponent(JSON.stringify(predicate.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search?predicate=${predicated}`);
    }

    getCurrentUser() {
        return this.http.get(environment.apiURL + environment.apiSufix + `/users/current`);
    }

    getUserMatrix(userid) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/accessrules/matrix/users/${userid}`);
    }

    getUserPref(userid) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/userpreferences/${userid}`);
    }
}