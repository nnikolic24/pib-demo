import { Component, OnInit, ViewChild } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { DeactivatedService } from './deactivated.service';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax
import { CoreService } from './../../service/core/core.service';
import { Router } from '@angular/router';
import { EntitySearchService } from './../../service/core/entity-search.service';

@Component({
  selector: 'ms-deactivated',
  templateUrl: './deactivated.component.html',
  styleUrls: ['./deactivated.component.scss']
})
export class DeactivatedComponent implements OnInit {

  @ViewChild('totalLoansTable') totalLoansTable: any;
  @ViewChild('propertiesTable') propertiesTable: any;
  @ViewChild('rateSwapsTable') rateSwapsTable: any;
  @ViewChild('mortgagesTable') mortgagesTable: any;

  user;
  loans = {};
  adaptedLoans = [];
  properties = {};
  adaptedProperties = [];
  mortgages = {};
  adaptedMortgages = [];
  interestRate = {};
  adaptedInterestRate = [];

  totalLoansParams;
  financingObjectParams;
  interestRateParams;
  mortgageParams;


  constructor(
    private pageTitleService: PageTitleService,
    private deactivateService: DeactivatedService,
    private coreService: CoreService,
    private router: Router,
    private searchService: EntitySearchService) { }

  ngOnInit() {
    this.pageTitleService.setTitle("Deactivated items");
    // this.deactivateService.getCurrentUser()
    //   .subscribe(res => {
    //     this.user = res;
    //   }, error => {
    //     console.log("Error while getting user data", error);
    //   }); 
    // Is it needed ??!
    this.prepare();
  }

  onActivate(event) {
    if (event.type == 'click') {
      if (event.row.Tab === "Mortgage") {
        this.router.navigate(["/credit/mortgage/" + event.row.ID]);
      } else if (event.row.Tab === "Interest Rate") {
        this.router.navigate(["/interest-rate-swaps/interest-rate-swaps-display/" + event.row.ID]);
      } else if (event.row.Tab === "Properties") {
        this.router.navigate(["/properties/" + event.row.ID]);
      } else {
        this.router.navigate(["/credit/totalloans/total-loan-display/" + event.row.ID]);
      }
    }
  }

  toggleExpandTotalLoansTable(row) {
    this.totalLoansTable.rowDetail.toggleExpandRow(row);
  }

  toggleExpandPropertiesTable(row) {
    this.propertiesTable.rowDetail.toggleExpandRow(row);
  }

  toggleExpandRateSwapsTable(row) {
    this.rateSwapsTable.rowDetail.toggleExpandRow(row);
  }

  toggleExpandMortgagesTable(row) {
    this.mortgagesTable.rowDetail.toggleExpandRow(row);
  }

  //TODO
  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  prepare() {
    this.totalLoansSearch();
    this.financingObjectSearch();
    this.mortgageSearch();
    this.interestRateSearch();
    this.fetch()
      .subscribe(data => {
        this.prepareLoans(data[0]);
        this.prepareProperties(data[1]);
        this.prepareInterestRate(data[2]);
        this.prepareMortgage(data[3])
      });
  }

  fetch() {
    //Pulls all data from the endpoint that you need
    return forkJoin([
      // this.deactivateService.getUserMatrix(this.user.id),
      // this.deactivateService.getUserPref(this.user.id),
      this.deactivateService.getTotalLoans(this.totalLoansParams),
      this.deactivateService.getFinancingObject(this.financingObjectParams),
      this.deactivateService.getInterestRateSwaps(this.interestRateParams),
      this.deactivateService.getMortgages(this.mortgageParams)
    ]);
  }

  prepareLoans(data) {
    this.loans = data.rows;

    this.adaptedLoans = this.coreService.objectToArray(this.loans);
    this.adaptedLoans = this.adaptedLoans.map(item => {

      let fullnameC;
      if (!item.customer.surname) {
        fullnameC = item.customer.name;
      } else {
        fullnameC = item.customer.name + "" + item.customer.surname;
      }

      let fullnameL;
      if (!item.creditor.surname) {
        fullnameL = item.creditor.name;
      } else {
        fullnameL = item.creditor.name + "" + item.creditor.surname;
      }

      return {
        "ID": item.id,
        "Customer": fullnameC,
        "Lender": fullnameL,
        "Loan": item.creditLimits,
        "Deactivated": item.deactivatedSince,
        "Tab": "Loans"
      }
    });
  }

  prepareProperties(data) {
    this.properties = data.rows;

    this.adaptedProperties = this.coreService.objectToArray(this.properties);
    this.adaptedProperties = this.adaptedProperties.map(item => {

      return {
        "ID": item.id,
        "Property": item.name,
        "Deactivated": item.deactivatedSince,
        "Tab": "Properties"
      }
    });
  }

  prepareMortgage(data) {
    this.mortgages = data.rows;

    this.adaptedMortgages = this.coreService.objectToArray(this.mortgages);
    this.adaptedMortgages = this.adaptedMortgages.map(item => {

      let fullnameC;

      if (!item.customer.surname) {
        fullnameC = item.customer.name;
      } else {
        fullnameC = item.customer.name + "" + item.customer.surname;
      }
      return {
        "ID": item.mortgageId,
        "Mortgages": item.name,
        "Customer": fullnameC,
        "Deactivated": item.deactivatedSince,
        "Tab": "Mortgage"
      }
    });
  }

  prepareInterestRate(data) {
    this.interestRate = data.rows;

    this.adaptedInterestRate = this.coreService.objectToArray(this.interestRate);
    this.adaptedInterestRate = this.adaptedInterestRate.map(item => {

      return {
        "ID": item.id,
        "InterestRate": item.name,
        "Deactivated": item.deactivatedSince,
        "Tab": "Interest Rate"
      }
    });
  }

  totalLoansSearch() {

    let totalLoansSerach: any = {};
    totalLoansSerach.entity = "totalloans"; //TODO check will it be needed on new app like this   

    //Create entity search with provided params
    let entitySearch = this.searchService.createEntitySearch(totalLoansSerach);
    this.searchService.pushPredicate(entitySearch, '!isAktiv', null);
    //prepare params to be provided for endpoint
    this.totalLoansParams = this.searchService.prepareParams(entitySearch);
  }

  financingObjectSearch() {

    let financingObjectSerach: any = {};
    financingObjectSerach.entity = "financingobjects"; //TODO check will it be needed on new app like this   

    //Create entity search with provided params
    let entitySearch = this.searchService.createEntitySearch(financingObjectSerach);
    this.searchService.pushPredicate(entitySearch, '!isAktiv', null);
    //prepare params to be provided for endpoint
    this.financingObjectParams = this.searchService.prepareParams(entitySearch);
  }

  interestRateSearch() {

    let interestRateSearch: any = {};
    interestRateSearch.entity = "interestRateSwaps"; //TODO check will it be needed on new app like this   

    //Create entity search with provided params
    let entitySearch = this.searchService.createEntitySearch(interestRateSearch);
    this.searchService.pushPredicate(entitySearch, '!isAktiv', null);
    this.searchService.pushPredicate(entitySearch, 'isZinsswap', null);
    //prepare params to be provided for endpoint
    this.interestRateParams = this.searchService.prepareParams(entitySearch);
  }

  mortgageSearch() {

    let mortgageSearch: any = {};
    mortgageSearch.entity = "mortgage"; //TODO check will it be needed on new app like this   

    //Create entity search with provided params
    let entitySearch = this.searchService.createEntitySearch(mortgageSearch);
    this.searchService.pushPredicate(entitySearch, '!isAktiv', null);
    this.searchService.pushPredicate(entitySearch, '!isZinsswap', null);
    //prepare params to be provided for endpoint
    this.mortgageParams = this.searchService.prepareParams(entitySearch);
  }

}
