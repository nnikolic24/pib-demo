import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreService } from './../service/core/core.service';
import { environment } from './../../environments/environment';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class PropertiesService {

    constructor(private http: HttpClient, private coreSvc: CoreService) { }

    getEligibleProperties(params) {
        let paged = encodeURIComponent(JSON.stringify(params.paged));
        let predicate = encodeURIComponent(JSON.stringify(params.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/financingobjects/search?paged=${paged}&predicate=${predicate}`);
    }

    getGroupedOverdueMortgages(params) {
        let groupedBy = params.groupedBy;
        let predicate = encodeURIComponent(JSON.stringify(params.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search/grouped?groupedBy=${groupedBy}&predicate=${predicate}`);

    }

    getGroupedReservedMortgages(params) {
        let groupedBy = params.groupedBy;
        let predicate = encodeURIComponent(JSON.stringify(params.predicate));
        let referanceDay = JSON.stringify(params.referenceday);
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search/grouped?groupedBy=${groupedBy}&predicate=${predicate}&referenceday=${referanceDay}`);

    }

    getFOById(id) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/financingobjects/${id}`);
    }

    getAllMortgages() {
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage`);
    }

    getMortgages(params) {
        let predicate = encodeURIComponent(JSON.stringify(params.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search?predicate=${predicate}`);
    }

    getContacts() {
        return this.http.get(environment.apiURL + environment.apiSufix + `/contacts`);
    }

    updateFinancingObject(fObject, id) {
        return this.http.put(environment.apiURL + environment.apiSufix + `/financingobjects/${id}`, fObject);
    }

    getContactById(id) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/contacts/${id}`);
    }

    createFinancingObject(fObject) {
        return this.http.post(environment.apiURL + environment.apiSufix + `/financingobjects/`, fObject);
    }

}