import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PropertiesRoutingModule } from './properties-routing.module';
import { PropertiesComponent } from './properties/properties.component';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgSelectModule } from '@ng-select/ng-select';


import { MatInputModule,
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatIconModule,
  MatDividerModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatTabsModule,
  MatDatepickerModule,
  MatSelectModule,
  MatRadioModule,
  MatChipsModule,
  MatExpansionModule
  
} from '@angular/material';

import { ChartsModule } from 'ng2-charts';



import { EditPropertyComponent } from './edit-property/edit-property.component';
import { ProcessPropertyComponent } from './edit-property/process-property/process-property.component';
import { PropertiesOverviewComponent } from './edit-property/properties-overview/properties-overview.component';
import { PropertiesDetailsComponent } from './edit-property/properties-details/properties-details.component';
import { SharedModule } from './../shared/shared.module';

@NgModule({
  declarations: [PropertiesComponent, EditPropertyComponent, ProcessPropertyComponent, PropertiesOverviewComponent, PropertiesDetailsComponent],
  imports: [
    CommonModule,
    PropertiesRoutingModule,
    MatFormFieldModule,
    NgxDatatableModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatSelectModule,
    TranslateModule,
    FlexLayoutModule,
    MatTabsModule,
    MatIconModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCheckboxModule,
    MatChipsModule,
    SharedModule,
    MatExpansionModule
  ]
})
export class PropertiesModule { }
