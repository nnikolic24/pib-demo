import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertiesComponent } from './properties/properties.component';
import { EditPropertyComponent } from './edit-property/edit-property.component';
import { ProcessPropertyComponent } from './edit-property/process-property/process-property.component';

const routes: Routes = [
  {
  path: '',
  children: [
    {
      path: '',
      component: PropertiesComponent
    },
    {
      path: ':id',
      component: EditPropertyComponent
    },
    {
      path: 'edit/:id',
      component: ProcessPropertyComponent
    },
    {
      path: 'new/:id',
      component: ProcessPropertyComponent
    }
  ]
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertiesRoutingModule { }
