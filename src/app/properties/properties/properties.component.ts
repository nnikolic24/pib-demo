import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../core/route-animation/route.animation";
import { Router } from "@angular/router";
import { PropertiesService } from '../properties.service';
import { EntitySearchService } from './../../service/core/entity-search.service';
import { BlockUIService } from 'ng-block-ui';

@Component({
   selector: 'ms-properties',
   templateUrl: './properties.component.html',
   styleUrls: ['./properties.component.scss'],
   encapsulation: ViewEncapsulation.None,
   host: {
      "[@fadeInAnimation]": 'true'
   },
   animations: [fadeInAnimation]
})
export class PropertiesComponent implements OnInit {

   rows = [];
   temp = [];

   adaptedEP = [];
   financingObjectParams;   
   public searchQuery: string = "";
   public totalNumberOfElemetns: number = 0;

   constructor(
      private pageTitleService: PageTitleService,
      private router: Router,
      private propService: PropertiesService,
      private searchService: EntitySearchService,
      private blockUI: BlockUIService) {
   }

   ngOnInit() {
      this.pageTitleService.setTitle("Properties eligible for financing");
      this.blockUI.start('main-block');
      this.financingObjectSearch();
   }

   /**
    * Filter data by provided "searchQuery" value
    * For search result set grid result to first page in grid
    */
   filterBy() {
      this.blockUI.start('main-block');
      let firstPage = {page: 1}
      this.financingObjectSearch(firstPage);
   }

   /**
    * Function will execute on any row event
    * @param event Received event
    */
   onActivate(event) {
      // If user clicks on row it should redirect him to form with user data 
      if (event.type == 'click') {
         this.router.navigate(['/properties/', event.row.ID]);
      }
   }

   financingObjectSearch(getPage?) {

      let financingObjectSerach: any = {};
      financingObjectSerach.entity = "financingobjects"; //TODO check will it be needed on new app like this   
      financingObjectSerach.paging = { sortColumns: "name" };
      financingObjectSerach.predicateSource = {
         //hasKunde: [this.userId], //TODO Check if user has specific role
         hasName: '%' + this.searchQuery + '%',
         isAktiv: null
      };

      //Create entity search with provided params
      let entitySearch = this.searchService.createEntitySearch(financingObjectSerach);
      //prepare params to be provided for endpoint
      this.financingObjectParams = this.searchService.prepareParams(entitySearch);

      //if we get footer page number set params
      if (getPage) {
         this.financingObjectParams.paged.page = getPage.page;
      }

      this.getData(this.financingObjectParams);
   }

   getData(data) {
      this.propService.getEligibleProperties(data)
      .subscribe((result: any) => {
         this.adaptedEP = result.rows.map(row => {

            this.totalNumberOfElemetns = result.total;

            let fullnameC;

            if (!row.customer.surname) {
               fullnameC = row.customer.name;
            } else {
               fullnameC = row.customer.name + "" + row.customer.surname;
            }

            return {
               "ID": row.id,
               "Name": row.name,
               "Customer": fullnameC
            }
         })
         this.blockUI.stop('main-block');
      }, error => {
         console.error(error);
      });
   }
}
