import { Component, OnInit, Input } from '@angular/core';
import { PropertiesService } from '../../properties.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ms-properties-details',
  templateUrl: './properties-details.component.html',
  styleUrls: ['./properties-details.component.scss']
})
export class PropertiesDetailsComponent implements OnInit {

  constructor(private propService: PropertiesService, private translate: TranslateService) { }
  
  @Input() financingObject: any;

  ngOnInit() {
  }

}
