import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../core/route-animation/route.animation";
import { TranslateService } from '@ngx-translate/core';
import { Router } from "@angular/router";
import { PropertiesOverviewComponent } from './properties-overview/properties-overview.component';
import { PropertiesDetailsComponent } from './properties-details/properties-details.component';
import { PropertiesService } from './../properties.service';
import { ActivatedRoute } from '@angular/router';
import { CoreService } from './../../service/core/core.service';
import { BlockUIService } from 'ng-block-ui';

@Component({
   selector: 'ms-edit-property',
   templateUrl: './edit-property.component.html',
   styleUrls: ['./edit-property.component.scss'],
   encapsulation: ViewEncapsulation.None,
   host: {
      "[@fadeInAnimation]": 'true'
   },
   animations: [fadeInAnimation]
})
export class EditPropertyComponent implements OnInit {

   id;
   financingObject: any = {};
   objectWorth = {};
   colleteralValue = {};
   investmentWorth = {};
   rentalIncome = {};
   property = {};

   @ViewChild('propertiesoverview') propertiesoverview: any;

   constructor(
      private pageTitleService: PageTitleService,
      private translate: TranslateService,
      private router: Router,
      private propService: PropertiesService,
      private route: ActivatedRoute,
      private coreService: CoreService,
      private blockUI: BlockUIService) {
   }

   ngOnInit() {
      this.pageTitleService.setTitle("Property eligible for financing");


      this.blockUI.stop('main-block');
      this.id = this.route.snapshot.params.id;

      this.propService.getFOById(this.id)
         .subscribe((result: any) => {
            this.property = result;
            let fullnameC;
            if (!result.customer.surname) {
               fullnameC = result.customer.name;
            } else {
               fullnameC = result.customer.name + " " + result.customer.surname;
            }

            let fullnameO;
            if (result.owner.length == 0) {
               fullnameO = "-"
            } else {
               for (let i = 0; i < result.owner.length; i++) {
                  let tempName;
                  if (i == 0) {
                     if (!result.owner[i].surname) {
                        tempName = result.owner[i].name;
                        fullnameO = tempName;
                     } else {
                        tempName = result.owner[i].name + " " + result.owner[i].surname;
                        fullnameO = tempName;
                     }
                  } else {
                     if (!result.owner[i].surname) {
                        tempName = result.owner[i].name;
                        fullnameO = fullnameO + ', ' + tempName;
                     } else {
                        tempName = result.owner[i].name + " " + result.owner[i].surname;
                        fullnameO = fullnameO + ', ' + tempName;
                     }
                  }
               }
            }

            this.financingObject = {
               CustomerID: result.customer.id,
               Customer: fullnameC,
               Owner: fullnameO
            }
            this.financingObject.Property = result.property;

            this.getSum(this.financingObject.Property);

         }, error => {
            console.log("There was an error: ", error);
         });
   }

   /**
   * Function will execute on any row event
   * @param event Received event
   */
   onActivate(event) {
      // If user clicks on row it should redirect him to form with user data 
      if (event.type == 'click') {
         this.router.navigate(['/properties/', 1]);
      }
   }

   /**
    * Process current property
    */
   processProperty() {
      this.router.navigate(['/properties/edit/' + this.id]);
   }

   //Tabs Event Change

   @ViewChild("propertiesoverview") propertiesOverview: PropertiesOverviewComponent;


   goBack() {
      window.history.back();
   }

   goToCustomer() {
      if (event.type == 'click') {
         this.router.navigate(['/contacts/', this.financingObject.CustomerID]);
      }
   }

   getSum(data) {
      let items = data;

      let objectWorthSum = 0;
      let colleteralValueSum = 0;
      let investmentWorthSum = 0;
      let rentalIncomeSum = 0;

      for (let i = 0; i < items.length; i++) {
         for (let j = 0; j < items[i].object.length; j++) {
            if (items[i].object[j].colleteralValue) {
               colleteralValueSum += items[i].object[j].colleteralValue.amount;
            }

            if (items[i].object[j].investmentWorth) {
               investmentWorthSum += items[i].object[j].investmentWorth.amount;
            }

            if (items[i].object[j].rentalincome) {
               rentalIncomeSum += items[i].object[j].rentalincome.amount;
            }

            if (items[i].object[j].objectWorth) {
               objectWorthSum += items[i].object[j].objectWorth.amount;
            }
         }

      }

      let objectWorth = {
         value: objectWorthSum,
         show: false
      };
      if (objectWorthSum > 0) {
         objectWorth.show = true;
      }

      let colleteralValue = {
         value: objectWorthSum,
         show: false
      };
      if (colleteralValueSum > 0) {
         colleteralValue.show = true;
      }

      let investmentWorth = {
         value: investmentWorthSum,
         show: false
      };
      if (investmentWorthSum > 0) {
         investmentWorth.show = true;
      }

      let rentalIncome = {
         value: rentalIncomeSum,
         show: false
      };
      if (rentalIncomeSum > 0) {
         rentalIncome.show = true;
      }

      this.objectWorth = objectWorth;
      this.colleteralValue = colleteralValue;
      this.investmentWorth = investmentWorth;
      this.rentalIncome = rentalIncome;

   }
}
