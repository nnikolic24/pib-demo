import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../../core/route-animation/route.animation";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { PropertiesService } from 'app/properties/properties.service';
import { ToastrService } from 'ngx-toastr';
import { BlockUIService } from 'ng-block-ui';
import { Router } from '@angular/router';
import * as moment from 'moment';



@Component({
  selector: 'ms-process-property',
  templateUrl: './process-property.component.html',
  styleUrls: ['./process-property.component.scss']
})
export class ProcessPropertyComponent implements OnInit {

  public form: FormGroup;

  owners: any = [];


  showTypeOfProperty: string;
  showSold: boolean;
  id;
  contactID;
  fObject: any = {};
  fullName: string;
  postCodeVal = false;

  allUsageItems = [
    { value: 'SELBSTGENUTZTESWOHNEIGENTUM', viewValue: 'SELBSTGENUTZTESWOHNEIGENTUM' },
    { value: 'IMMOBILIENANLAGE', viewValue: 'IMMOBILIENANLAGE' },
    { value: 'NONPROFITIMMOBILIE', viewValue: 'NONPROFITIMMOBILIE' },
    { value: 'UEBRIGEIMMOBILIE', viewValue: 'UEBRIGEIMMOBILIE' }
  ];

  propertyType = [
    { value: 'BAULAND', viewValue: 'BAULAND' },
    { value: 'EINFAMILIEN_HAUS', viewValue: 'EINFAMILIEN_HAUS' },
    { value: 'MEHRFAMILIEN_HAUS', viewValue: 'MEHRFAMILIEN_HAUS' },
    { value: 'EIGENTUMSWOHNUNG', viewValue: 'EIGENTUMSWOHNUNG' },
    { value: 'GESCHAEFTSHAUS', viewValue: 'GESCHAEFTSHAUS' },
    { value: 'WOHN_UND_GESCHAEFTSHAUS', viewValue: 'WOHN_UND_GESCHAEFTSHAUS' },
    { value: 'WOHNHAUS_MIT_GEWERBEANTEIL', viewValue: 'WOHNHAUS_MIT_GEWERBEANTEIL' },
    { value: 'GEWERBEIMMOBILIE', viewValue: 'GEWERBEIMMOBILIE' },
    { value: 'GEWERBEIMMOBILIE_MIT_WOHNANTEIL', viewValue: 'GEWERBEIMMOBILIE_MIT_WOHNANTEIL' },
    { value: 'UNBEKANNT', viewValue: 'UNBEKANNT' }
  ];


  constructor(
    private fb: FormBuilder,
    private pageTitleService: PageTitleService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private propService: PropertiesService,
    private toastr: ToastrService,
    private blockUI: BlockUIService,
    private router: Router) { }

  ngOnInit() {
    if (this.router.url.includes('new')) {
      this.contactID = this.route.snapshot.params.id;

      this.fObject = {
        property: [this.createRealEstate()]
      }

      if (this.contactID === undefined) {
        this.toastr.error("There was an error");
        this.router.navigate(['contacts/customers']);
      } else {
        this.propService.getContactById(this.contactID)
          .subscribe((result: any) => {
            this.fObject.customer = result;
            this.fillForm(this.fObject);
            if (!result.customer.surname) {
              this.fullName = result.name;
            } else {
              this.fullName = result.name + " " + result.surname;
            }
          }, error => {
            console.log(error);
            this.toastr.error("There was an error loading customer!");
          });
          
      this.getContacts();
      }
    } else {
      this.id = this.route.snapshot.params.id;

      this.propService.getFOById(this.id)
        .subscribe((data: any) => {

          this.fObject = data;
          for (let i = 0; i < this.fObject.owner.length; i++) {
            if (!this.fObject.owner[i].surname) {
              this.fObject.owner[i].name = this.fObject.owner[i].name;
            } else {
              this.fObject.owner[i].name = this.fObject.owner[i].name + " " + this.fObject.owner[i].surname;
            }
          }
          this.fillForm(this.fObject);

          this.pageTitleService.setTitle(this.fObject.name);

          if (!data.customer.surname) {
            this.fullName = data.customer.name;
          } else {
            this.fullName = data.customer.name + " " + data.customer.surname;
          }

        }, error => {
          console.log(error);
          this.toastr.error("There was an error while fetching financing object!");
        });

      this.getContacts();
    }

  }

  fillForm(data) {

    if (data.property.length > 0) {
      for (let i = 0; i < data.property.length; i++) {
        for (let j = 0; j < data.property[i].object.length; j++) {
          if (!this.fObject.property[i].object[j].purchaseData) {
            this.fObject.property[i].object[j].purchaseData = {
              amount: 0,
              date: null,
              comment: ''
            }
          }

          if (!this.fObject.property[i].object[j].objectWorth) {
            this.fObject.property[i].object[j].objectWorth = {
              amount: 0,
              comment: ''
            }
          }

          if (!this.fObject.property[i].object[j].collateralValue) {
            this.fObject.property[i].object[j].collateralValue = {
              amount: 0,
              comment: ''
            }
          }

          if (!this.fObject.property[i].object[j].investmentWorth) {
            this.fObject.property[i].object[j].investmentWorth = {
              amount: 0,
              comment: ''
            }
          }

          if (!this.fObject.property[i].object[j].investmentCost) {
            this.fObject.property[i].object[j].investmentCost = {
              amount: 0,
              comment: ''
            }
          }

          if (!this.fObject.property[i].object[j].rentalincome) {
            this.fObject.property[i].object[j].rentalincome = {
              amount: 0,
              comment: ''
            }
          }

          if (!this.fObject.property[i].object[j].zoningInterest) {
            this.fObject.property[i].object[j].zoningInterest = {
              amount: 0,
              comment: ''
            }
          }

        }
      }
    }

    return this.fObject;
  }


  updateFO() {
    this.blockUI.start('main-block');
    if (this.fObject.deactivatedSince) {
      this.fObject.deactivatedSince = moment(this.fObject.deactivatedSince).format('YYYY-MM-DD');
    } else {
      this.fObject.deactivatedSince = null;
    }

    if (this.fObject.property.length > 0) {
      for (let i = 0; i < this.fObject.property.length; i++) {
        for (let j = 0; j < this.fObject.property[i].object.length; j++) {
          if (this.fObject.property[i].object[j].saleDate) {
            this.fObject.property[i].object[j].saleDate = moment(this.fObject.property[i].object[j].saleDate).format('YYYY-MM-DD');
          } else {
            this.fObject.property[i].object[j].saleDate = null;
          }
        }
      }
    }

    if (this.fObject.id) {
      this.propService.updateFinancingObject(this.fObject, this.id)
      .subscribe(resolve => {
        this.toastr.success('Successfully updated!');
        this.router.navigate(['properties/', this.id]);
      }, error => {
        this.blockUI.stop('main-block');
        this.toastr.error("There was an error while updating financing object!");
        console.log(error);
      });
    } else {
      this.propService.createFinancingObject(this.fObject)
      .subscribe((resolve: any) => {
        this.toastr.success('Successfully created!');
        this.router.navigate(['properties/', resolve.id]);
      }, error => {
        this.blockUI.stop('main-block');
        this.toastr.error("There was an error while creating financing object!");
        console.log(error);
      });
    }
    
  }

  onChangePostcode(e) {
    console.log(e);
    if ((e.target.value.length < 4) || (e.target.value.length > 6)) {
      this.postCodeVal = true;
    } else {
      this.postCodeVal = false;
    }
  }

  changeRB(e) {
    this.showTypeOfProperty = e.value;
  }

  changeCB(e) {
    this.showSold = e.checked;
  }

  goBack() {
    window.history.back();
  }

  createRealEstate() {
    return {
      object: [
        {
          adress: {},
          purchaseData: {
            amount: 0,
            date: null,
            comment: ''
          },
          objectWorth: {
            amount: 0,
            comment: ''
          },
          collateralValue: {
            amount: 0,
            comment: ''
          },
          investmentWorth: {
            amount: 0,
            comment: ''
          },
          investmentCost: {
            amount: 0,
            comment: ''
          },
          rentalincome: {
            amount: 0,
            comment: ''
          },
          zoningInterest: {
            amount: 0,
            comment: ''
          }
        }
      ]
    }
  }

  addRealEstate() {
    this.fObject.property.push(this.createRealEstate());
  }

  removeRealEstate(prop) {
    let property = this.fObject.property;
    let index = property.indexOf(prop);
    property.splice(index, 1);
  }

  addProperty(prop) {
    console.log(prop);
    prop.object.push({
      adress: {},
      purchaseData: {
        amount: 0,
        date: null,
        comment: ''
      },
      objectWorth: {
        amount: 0,
        comment: ''
      },
      collateralValue: {
        amount: 0,
        comment: ''
      },
      investmentWorth: {
        amount: 0,
        comment: ''
      },
      investmentCost: {
        amount: 0,
        comment: ''
      },
      rentalincome: {
        amount: 0,
        comment: ''
      },
      zoningInterest: {
        amount: 0,
        comment: ''
      }
    });
  }

  removeProperty(prop, obj) {
    let index = prop.object.indexOf(obj);
    prop.object.splice(index, 1);
  };


  removeInvenstmentWorth(obj) {
    obj.investmentWorth = {};
  };

  getContacts() {
    this.propService.getContacts()
      .subscribe((data: any) => {
        this.owners = data.rows.map(item => {
          let fName
          if (!item.surname) {
            fName = item.name;
          } else {
            fName = item.name + " " + item.surname;
          }

          return {
            id: item.id,
            name: fName
          }
        });
      }, error => {
        console.log(error);
      });
  }

}
