import { Component, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { EntitySearchService } from './../../../service/core/entity-search.service';
import { CoreService } from './../../../service/core/core.service';  // RxJS 6 syntax
import { PropertiesService } from '../../properties.service';
import { Router } from "@angular/router";
import * as moment from 'moment';

import { MortgageGridHelperService } from './../../../service/core/mortgage-grid-helper.serice';

@Component({
   selector: 'ms-properties-overview',
   templateUrl: './properties-overview.component.html',
   styleUrls: ['./properties-overview.component.scss']
})
export class PropertiesOverviewComponent implements OnInit {

   constructor(
      private searchService: EntitySearchService,
      private propService: PropertiesService,
      private coreService: CoreService,
      private router: Router,
      private mortGridService: MortgageGridHelperService) { }

   //Doughnut Chart configuration
   public doughnatChartLabels: Array<any> = [];
   public doughnutChartData: number[] = [];
   /*---------- Doughnut Chart configuration ----------*/
   public doughnutChartType: string = 'doughnut';
   pieChartColors: any[] = [{
      backgroundColor: ['#661A13', '#EA7B71', '#E53A2A', '#664F4D', '#B22D21']
   }];
   PieChartOptions: any = {
      elements: {
         arc: {
            borderWidth: 0
         }
      },
      legend: {
         position: 'left'
      },
      responsive: true
   }
   doughnutLegend: boolean = true;
   showChart: boolean = false;

   searchQuery = "";
   eligibleProperties = [];
   epParams;
   epoParams;
   mortgages;
   mortgagesID = [];
   allMortgages;
   adaptedAllMortgages = [];
   overdueMortages = [];
   chartData: any = [];
   swapPerc: number = 0;
   showSwapped: boolean = false;
   groupedMortgages = [];
   reservedMortgages = [];

   currencySummMort = 0;
   currencySummOverMort = 0;
   currencySummResMort = 0;

   @ViewChild('mortgageSummaryCell') mortgageSummaryCell: TemplateRef<any>;
   @Input() financingObjectId: any;
   @Input() rentalIncome: any;
   @Input() objectWorth: any;
   @Input() colleteralValue: any;
   @Input() investmentWorth: any;
   enableSummary = true;
   summaryPosition = 'bottom';

   ngOnInit() {
      this.prepare();
      this.groupedOverdueMortgageParams();
      this.groupedReservedMortgage();
   }



   caclulateSumm(data) {
      data.forEach((item) => {
         this.currencySummMort += item.restVolume;
      });
   }

   initChar() {
      setTimeout(() => {
         this.showChart = true;
      }, 0)
   }

   prepareMortgages(data) {
      this.mortgages = data.rows
      for (let i = 0; i < this.mortgages.length; i++) {
         this.mortgagesID.push(this.mortgages[i].mortgageId);
      }
   }

   prepare() {
      this.mortgageParams();
      this.propService.getMortgages(this.epParams)
         .subscribe(data => {
            this.prepareMortgages(data);
            this.overdueMortgageParams();
            this.propService.getMortgages(this.epoParams)
               .subscribe(data => {
                  this.prepareArrays(this.mortgages, data);
                  this.populateChart();
                  this.initChar();
                  this.caclulateSumm(this.adaptedAllMortgages);
               });
         });
   }

   prepareArrays(mortgages, overdueM) {

      this.allMortgages = mortgages;
      this.adaptedAllMortgages = this.coreService.objectToArray(this.allMortgages);
      this.overdueMortages = this.coreService.objectToArray(overdueM.rows)
      this.adaptedAllMortgages = this.adaptedAllMortgages.concat(this.overdueMortages);

      this.adaptedAllMortgages = this.mortGridService.mapMortgagesDataForGrid(this.adaptedAllMortgages);
   }

   mortgageParams() {

      let mortgageSearch: any = {};

      mortgageSearch.entity = "financingobjects"; //TODO check will it be needed on new app like this   

      let entitySearchMortgages = this.searchService.createEntitySearch(mortgageSearch);
      this.searchService.pushPredicate(entitySearchMortgages, 'isGueltig', null);
      this.searchService.pushPredicate(entitySearchMortgages, 'hasFinanzierungsObjekt', [this.financingObjectId]);
      //prepare params to be provided for endpoint
      this.epParams = this.searchService.prepareParams(entitySearchMortgages);
   }

   overdueMortgageParams() {
      let mortgageSearchOverdue: any = {};

      mortgageSearchOverdue.entity = "financingobjects"; //TODO check will it be needed on new app like this JSON.stringify(0)

      let entitySearchMortgagesO = this.searchService.createEntitySearch(mortgageSearchOverdue);
      this.searchService.pushPredicate(entitySearchMortgagesO, 'isUeberfaellig', '0');
      this.searchService.pushPredicate(entitySearchMortgagesO, 'idIsNot', this.mortgagesID);
      this.searchService.pushPredicate(entitySearchMortgagesO, 'hasFinanzierungsObjekt', [this.financingObjectId]);
      //prepare params to be provided for endpoint
      this.epoParams = this.searchService.prepareParams(entitySearchMortgagesO);
   }

   populateChart() {

      for (let i = 0; i < this.adaptedAllMortgages.length; i++) {
         this.doughnutChartData.push(this.adaptedAllMortgages[i].restVolume);
         this.doughnatChartLabels.push(this.adaptedAllMortgages[i].lender);
      }
   }

   /**
 * Function will execute on any row event
 * @param event Received event
 */
   onActivate(event) {
      // If user clicks on row it should redirect him to form with user data 
      if (event.type == 'click') {
         if (event.row.productType == "ZINSSWAP") {
            this.router.navigate(['/credit/interest-rate-swaps/interest-rate-swaps-display/', event.row.mortgageId]);
         } else {
            this.router.navigate(['/credit/mortgage/', event.row.mortgageId]);
         }
      }
   }

   groupedOverdueMortgageParams() {

      let mortgageSearch: any = {};

      mortgageSearch.entity = "mortgage"; //TODO check will it be needed on new app like this   
      mortgageSearch.groupingPredicate = 'BY_LETZTE_LAUFZEIT_ENDE';

      let entitySearchMortgages = this.searchService.createEntitySearch(mortgageSearch);
      this.searchService.pushPredicate(entitySearchMortgages, '!isGueltig', null);
      this.searchService.pushPredicate(entitySearchMortgages, 'isUeberfaellig', '0');
      this.searchService.pushPredicate(entitySearchMortgages, 'hasFinanzierungsObjekt', [this.financingObjectId]);
      //prepare params to be provided for endpoint
      let groupedMortParams = this.searchService.prepareParams(entitySearchMortgages);

      this.propService.getGroupedOverdueMortgages(groupedMortParams)
         .subscribe((result: any) => {
            this.groupedMortgages = this.mortGridService.setGroupedByParam(result);
            this.groupedMortgages = this.mortGridService.mapMortgagesDataForGrid(this.groupedMortgages);
         });
   }

   onDetailToggle(event) {
      console.log('Detail Toggled', event);
   }


   groupedReservedMortgage() {

      let mortgageSearch: any = {};

      mortgageSearch.entity = "mortgage"; //TODO check will it be needed on new app like this   
      mortgageSearch.groupingPredicate = 'BY_LETZTE_LAUFZEIT_ENDE_RESERVIERT';

      let entitySearchMortgages = this.searchService.createEntitySearch(mortgageSearch);
      this.searchService.pushPathParam(entitySearchMortgages, 'referenceday', moment().format('YYYY-MM-DD'));
      this.searchService.pushPredicate(entitySearchMortgages, 'isGueltig', null);
      this.searchService.pushPredicate(entitySearchMortgages, 'isReserviert', null);
      this.searchService.pushPredicate(entitySearchMortgages, 'hasFinanzierungsObjekt', [this.financingObjectId]);
      //prepare params to be provided for endpoint
      let reserverdMortParams = this.searchService.prepareParams(entitySearchMortgages);

      this.propService.getGroupedReservedMortgages(reserverdMortParams)
         .subscribe((result: any) => {
            this.reservedMortgages = this.mortGridService.setGroupedByParam(result);
            this.reservedMortgages = this.mortGridService.mapMortgagesDataForGrid(this.reservedMortgages);
         });
   }

}
