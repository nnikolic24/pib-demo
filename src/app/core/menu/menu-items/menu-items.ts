import { Injectable } from '@angular/core';

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: 'dashboard',
    name: 'DASHBOARD',
    type: 'link',
    icon: 'explore'
  },
  {
    state: 'settings',
    name: 'main_menu_settings',
    type: 'sub',
    icon: 'build',
    children: [
      { state: '/ac/users', name: 'main_menu_settings_users' },
      { state: '/ac/roles', name: 'main_menu_settings_roles' },
      { state: '/ac/configvalues', name: 'main_menu_settings_configvalues' },
      { state: '/ac/peripherals', name: 'main_menu_settings_peripherals' },
    ]
  },
  {
    state: 'configurations',
    name: 'main_menu_configuration',
    type: 'sub',
    icon: 'view_list',
    children: [
      { state: '/interestrate', name: 'main_menu_configuration_zinsreihe' },
      { state: '/deactivated', name: 'main_menu_configuration_deactivated' }
    ]
  },
  {
    state: 'contacts',
    name: 'main_menu_kontakte',
    type: 'sub',
    icon: 'web',
    children: [
      { state: '/customers', name: 'main_menu_kontakte_kunden' },
      { state: '/lender', name: 'main_menu_kontakte_kreditgeber' },
      { state: '/contacts', name: 'main_menu_kontakte_kontakte' }
    ]
  },
  {
    state: 'orders',
    name: 'main_menu_auftraege',
    type: 'sub',
    icon: 'drag_indicator',
    children: [
      { state: '/orders', name: 'main_menu_auftraege' },
      { state: '/commissions', name: 'main_menu_auftraege_provisionen' }
    ]
  },
  {
    state: 'properties',
    name: 'main_menu_finanzierungsobjekte',
    type: 'link',
    icon: 'settings',
  },
  {
    state: 'credit',
    name: 'main_menu_kredite',
    type: 'sub',
    icon: 'credit_card',
    children: [
      { state: '/totalloans', name: 'main_menu_gesamtkredite' },
      { state: '/mortgages', name: 'main_menu_hypotheken' },
		{ state: '/interestrateswaps', name: 'main_menu_zinsswaps' },
		{ state: '/analyses', name: 'main_menu_hypotheken_auswertungen' }
    ]
  },
  {
    state: 'home-client',
    name: 'DASHBOARD',
    type: 'link',
    icon: '',
  },
  {
    state: 'properties-client',
    name: 'main_menu_finanzierungsobjekte',
    type: 'link',
    icon: '',
  },
  {
    state: 'credit-client',
    name: 'main_menu_kredite',
    type: 'sub',
    icon: '',
    children: [
      {state: '/total-loans', name: 'main_menu_gesamtkredite'},
      {state: '/interest-rate-swaps', name: 'main_menu_zinsswaps'}
    ]
  },
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
  add(menu: any) {
    MENUITEMS.push(menu);
  }
}
