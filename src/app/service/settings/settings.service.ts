import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreService } from '../core/core.service';
import { environment } from './../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    constructor(private http: HttpClient, private coreSvc: CoreService) { }

    getAllUsers() {
        return this.http.get(environment.apiURL + environment.apiSufix + '/users');
    }

    getRoles() {
        return this.http.get(environment.apiURL + environment.apiSufix + '/roles');
    }

    getUserById(id) {
        return this.http.get(environment.apiURL + environment.apiSufix + '/users/' + id);
    }

    getContactById(contactID) {
        return this.http.get(environment.apiURL + environment.apiSufix + '/contacts/' + contactID);
    }

    createUser(user) {
        return this.http.post(environment.apiURL + environment.apiSufix + '/users', user);
    }

    checkIfUserExist(username) {
        return this.http.head(environment.apiURL + environment.apiSufix + '/users/' + username);
    }

    updateUsers(user, userID) {
        return this.http.put(environment.apiURL + environment.apiSufix + '/users/' + userID, user);
    }

    setGoogleAuth(userID, _value) {
        return this.http.put(environment.apiURL + environment.apiSufix + `/users/${userID}/googleAuth`, { value: _value });
    }

    setMobileIDAuth(userID, _value) {
        return this.http.put(environment.apiURL + environment.apiSufix + `/users/${userID}/mobileID`, { value: _value });
    }

    updateCustomerList(entryID, customerIDs) {
        return this.http.put(environment.apiURL + environment.apiSufix + `/customerAccessPermission/${entryID}`, customerIDs);
    }

    parseUsers(result) {
        let data: any = [];
        if (!result || !result.rows) {
            return data
        }
        result.rows.forEach(user => {
            let roles;
            if (user.roles && user.roles.length) {
                roles = user.roles.map(role => role.name.toUpperCase()).join(' ');
            }
            data.push({
                "id": user.id,
                "username": user.username,
                "roles": roles
            });
        });
        return data;
    }

    getAccessUrles() {
        
        this.http.get(environment.apiURL + environment.apiSufix + '/accessrules/matrix/users/9c707395-335c-48d3-8f69-191f5d13095a/')
        .subscribe(data => console.log(data));
    }
}
