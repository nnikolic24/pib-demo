import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { MatDialogRef, MatDialog } from '@angular/material';

// Dialog components
import { ConfirmDialogComponent } from './../../widget-component/pop-up/confirm-dialog/confirm-dialog.component';

@Injectable({
	providedIn: 'root'
})

export class CoreService {

	sidenavOpen: boolean = true;
	sidenavMode: string = "side";
	horizontalStatus: boolean = false;
	horizontalSizeStatue: boolean = false;
	collapseSidebar: boolean = false;
	base_url: string = "http://localhost:9192";
	// Development base url below
	//base_url:string = "http://88.198.134.26:9192";

	constructor(private http: HttpClient, private matDialog: MatDialog) {
	}

	//Transforms object to array so you can use it in the table
	objectToArray(data) {
		let transformedData = [];
		for (let key in data) {
			let res = Object.assign(data[key], {});
			transformedData.push(res);
		}
		return transformedData;
	}

	//Formats date from endpoint to standard German format
	formatDate(dateToFormat, forParam?) {
		let m_num = new Array("1", "2", "3",
			"4", "5", "6", "7", "8", "9",
			"10", "11", "12");

		let date = dateToFormat.getDate();
		let month = dateToFormat.getMonth();
		let year = dateToFormat.getFullYear();
		let formatedDate: string;
		if (forParam) {
			formatedDate = year + '-' + m_num[month] + '-' + date;
		} else {
			formatedDate = date + '.' + m_num[month] + '.' + year;
		}

		return formatedDate;
	}

	closedRange(_lowerBound, _upperBound) {
		let lowerBound = moment(_lowerBound).format('YYYY-MM-DD');
		let upperBound = moment(_upperBound).format('YYYY-MM-DD')
		let toReturn: any = {};
		if (lowerBound) {
			toReturn.lowerBoundType = 'CLOSED';
			toReturn.lowerBound = lowerBound;
		}
		if (upperBound) {
			toReturn.upperBoundType = 'CLOSED';
			toReturn.upperBound = upperBound;
		}
		return toReturn;
	}

	/**
	 * Show confirm dialog
	 * @param title Dialog title
	 * @param data Dialog message 
	 */
	confirmDialog(title: string, data: string) {
		let dialogRef: MatDialogRef<ConfirmDialogComponent>;
		dialogRef = this.matDialog.open(ConfirmDialogComponent);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.data = data;
		return dialogRef.afterClosed();
	}
}