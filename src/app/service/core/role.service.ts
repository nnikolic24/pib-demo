import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) { }

    /**
     * fetchRoles will retrieve data from RoleRestfulService and return promise
     */
     fetchRoles(url) {
        return this.http.get(url);
     }

      /**
         *   parseRoles function will receive data
         *   and use only some properties from json
         */
        parseRoles(data){
            // empty array that will hold parsed data
            let parsedData:any=[];

            // if data with rows exist go through
            // all rows and get id and name property
            if(data && data.rows && data.rows.length!=0) {
                 data.rows.forEach(row => {
                         let role={};
                         if(row.name && String(row.name).trim()!="") {
                                role["id"] = row.id;
                                role["name"] = row.name;
                                parsedData.push(role);
                         }
                 });


            }

            return parsedData;
        }
}
