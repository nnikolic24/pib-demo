import { Injectable } from '@angular/core';
import * as lodash from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class EntitySearchService {

    constructor() { }

    /**
    * Factory for an EntitySearch instance whose constructor is the search configuration in searchConfiguration given becomes.
    *
    * @param searchConfiguration object with the search parameters. The object can contain the following attributes:
    * Entity - The smartResource whose search operation should be used for the search.
    * paging configuration for paging with the following entries:
    * page - int, default 1
    * pageSize - int, default 10
    * sortColumns - String Array, Default [id]
    * groupingPredicate - name of the predicament to be used for grouping the entries.
    * predicateSource - Function that returns predicate hashmap. The key is the predicate name that contains Value
    * the corresponding values, also as an array. If an empty string or an empty object as arguments
    * is passed, the predicate is ignored.
    * generateStats - Boolean flag, default false Specifies whether aggregations for the found entities are in the
    * Backend stored aggretation function should be called. If a grouping predicate is specified,
    * The statistics are created according to the deposited grouping, otherwise the given
    * Statistics used.
    */

    // createEntitySearch(searchConfiguration) {
    //     let entitySearch = this.EntitySearch(searchConfiguration);
    //     console.log("WHAT DID WE GOT AS PREPARED SEARCH:", entitySearch);
    //     return entitySearch;
    // };


    createEntitySearch(searchConfiguration) {
       
        let entitySearch: any = {};

        entitySearch.entity = searchConfiguration.entity ? searchConfiguration.entity : null;
        entitySearch.groupingPredicate = searchConfiguration.groupingPredicate ? searchConfiguration.groupingPredicate : null;
        entitySearch.paging = searchConfiguration.paging ? searchConfiguration.paging : null;
        entitySearch.generateStats = searchConfiguration.generateStats ? searchConfiguration.generateStats : false;
        entitySearch.callbackError = searchConfiguration.callbackError;
        entitySearch.predicateSource = searchConfiguration.predicateSource ? searchConfiguration.predicateSource : null;
        entitySearch.statsParams = searchConfiguration.statsParams ? searchConfiguration.statsParams : [];
        entitySearch.pathParams = searchConfiguration.pathParams ? searchConfiguration.pathParams : {};

        //TODO check will this be needed in new app
        // let callbackSuccess = searchConfiguration.callbackSuccess || function (response) {
        //     vm.result = {};
        //     vm.result.rows = response.rows;
        //     vm.result.total = response.total;
        // };

        return entitySearch;
    }


    preparePaging(searchConfiguration) {
        let paging: any = null;

        if (searchConfiguration.paging) {

            paging = {};
            paging.page = searchConfiguration.paging.page ? searchConfiguration.paging.page : 1;
            paging.max = searchConfiguration.paging.max ? searchConfiguration.paging.max : 10;

            if (searchConfiguration.paging.sortColumns) {
                paging.sortColumns = ((searchConfiguration.paging.sortColumns.constructor !== Array) ?
                    new Array(searchConfiguration.paging.sortColumns) : searchConfiguration.paging.sortColumns);
            } else {
                paging.sortColumns = ['id'];
            }
            paging.sortDirections = ['ASC'];

        }
        return paging;
    }


    /**
    * The private method already prefers the search parameters. These consist of paging parameters (page size, current
    * Page etc.) and predicates for restricting the result list. Accordingly, the method structured, first the paging parameters, then the predicates.
    *
    * The predicates are passed to the _initialize method as HashMap via the searchConfiguration parameter,
    * where the key is the predicate name and the value is the corresponding predicate value. This HashMap is then used for
    * processed the backend.
    */
    prepareParams(entitySearch) {

        var pushPredicate = true;
        // Reset search parameters
        let searchParams: any = {};

        // Paging-Parameter
        if (entitySearch.paging !== null) {
            searchParams.paged = this.preparePaging(entitySearch);
        }

        // Predicates
        searchParams.predicate = {};
        searchParams.predicate.predicate = [];

        if (entitySearch.predicateSource !== null) {
            let predicates = entitySearch.predicateSource;
            lodash.keys(predicates).forEach(predicateKey => {
                // Prepare parameters
                var parameter = predicates[predicateKey];

                // If undefined or null it is assumed that it is a predicate without parameters
                if (!parameter || parameter === null) {

                    searchParams.predicate.predicate.push({
                        name: predicateKey
                    });

                } else {
                    // If the predicate is an empty string, object or array, it will be ignored
                    // and '*' are converted to '%' at the string
                    switch (typeof parameter) {
                        case 'string':
                            parameter = parameter.replace('*', '%');
                            pushPredicate = parameter.length >= 0;
                            break;
                        case 'object':
                            // if we have an array and one of the elements is undefined it is ignored
                            if (parameter.constructor === Array && this.containsUndefinedValue(parameter)) {
                                pushPredicate = false;
                            } else {
                                pushPredicate = !(Object.keys(parameter).length === 0);
                            }
                            break;
                        default:
                    }

                    if (pushPredicate) {
                        searchParams.predicate.predicate.push({
                            name: predicateKey,
                            parameter: [JSON.stringify(parameter)]
                        });
                    }
                }

            });
        }

        if (entitySearch.groupingPredicate !== null) {
            searchParams.groupedBy = entitySearch.groupingPredicate;
        }

        if (entitySearch.statsParams && entitySearch.statsParams.length > 0) {
            searchParams.stat = { type: entitySearch.statsParams };
        }

        /*
        * Now prepare the path parameters. In contrast to the search params (message payload), the values ​​become
        * the path params are not JSON-stringified, because here are other criteria for transferring the parameters.
        * Accordingly, values ​​to be transferred in the path must be entered when the parameter is entered be JSON-stringified accordingly.
         */
        lodash.keys(entitySearch.pathParams).forEach(paramKey => {
            searchParams[paramKey] = entitySearch.pathParams[paramKey];
        });

        return searchParams;
    };

    /**Check if the value exits */
    containsUndefinedValue(array) {
        var i;
        for (i = 0; i < array.length; i++) {
            if (typeof array[i] === 'undefined') {
                return true;
            }
        }
        return false;
    };

    /**Set grouping predicate */
    setGroupingPredicate(searchParams, groupingPredicate) {
        searchParams.groupingPredicate = groupingPredicate;
        return searchParams;
    };

    //TODO in our case same as just calling prepareParams???
    generateSearchPayload(searchParams) {
        this.prepareParams(searchParams);
        return searchParams;
    };

    /**Delete privded predicate source param */
    deletePredicate(searchParams, name) {
        if (searchParams.predicateSource) {
            delete searchParams.predicateSource[name];
        }
        return searchParams;
    };

    //TODO self explanatory???
    resetPredicates(searchParams) {
        searchParams.predicateSource = {};
    };

    //TODO old client has "predicates" instead of predicateSource, but prepareParams will set predicates to {} investigate more???
    /**Add new predicate with name and value */
    pushPredicate(searchParams, name, parameter) {

        if (!searchParams.predicateSource) {
            searchParams.predicateSource = {};
        }
        searchParams.predicateSource[name] = parameter;

        return searchParams;
    };

    //TODO never used in old app but it's there???
    pushStatsParam(searchParams, name) {

        if (!searchParams.statsParams) {
            searchParams.statsParams = {};
        }

        searchParams.statsParams[searchParams.statsParams.length] = name;
    };

    /**
    * Writes a new key/value pair to the list of path params. In case the parameter already exists, the existing
    * value is overwritten.
    * @param name The name or key of the path parameter
    * @param parameter The value of the path parameter. Must be manually JSON-stringified!
    */
    pushPathParam(searchParams, name, parameter) {

        if (!searchParams.pathParams) {
            searchParams.pathParams = {};
        }
        searchParams.pathParams[name] = parameter;

        return searchParams;
    };

    //TODO this is called on grid paging click
    pageChanged(preparedPaging) {
        this.prepareParams(preparedPaging)
      };

    //TODO says it all???
    goToFirstPage(searchParams) {
        searchParams.paged.page = 1;
        return searchParams;
    };




}
