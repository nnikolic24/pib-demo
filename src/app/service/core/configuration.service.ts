import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor() { }

    /**
     * fetchRoles will retrieve data from RoleRestfulService
     */
     fetchRoles(url) {
        return new Promise((resolve,reject)=>{
          const req = new XMLHttpRequest();
          req.open('GET', url);
          req.onload = () => resolve(JSON.parse(req.response));
          req.onerror = ()=> reject(req.statusText);
          req.send();
        });
     }
}
