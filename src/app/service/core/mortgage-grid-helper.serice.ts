import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class MortgageGridHelperService {

    constructor() { }


    setGroupedByParam(data) {
        let groupedMortgages = [];
        Object.keys(data.map).forEach(mapKey => {
            let groupedByYear = mapKey;
            let groupedByYearData = data.map[mapKey];
            groupedByYearData.forEach(item => {
                item.groupedBy = groupedByYear;
                groupedMortgages.push(item);
            });
        });
        return groupedMortgages;
    }


    /**
    * Recive array of morgtgages and returns mapped array to be shown in grid
    * @param data 
    */
    mapMortgagesDataForGrid(data) {
        let mappedMortgages: any = [];
        if (data && data.length) {
            data.forEach(mortgage => {

                let mortgageId = mortgage.mortgageId ? mortgage.mortgageId : "";
                let mortgageName = mortgage.name ? mortgage.name : "";

                //Grid grouped by value indicator
                let groupedBy = mortgage.groupedBy ? mortgage.groupedBy : "";

                //Credit volume CHF column
                let restVolume = mortgage.restVolume ? mortgage.restVolume : "";

                //TODO needs to be translated with lang change
                let productType = mortgage.productType ? mortgage.productType : "";

                let lenderName;
                if (!mortgage.creditor.surname) {
                    lenderName = mortgage.creditor.name;
                } else {
                    lenderName = mortgage.creditor.name + " " + mortgage.creditor.surname;
                }

                //Interest rate % column
                let interest;
                if (mortgage.todayValidTerm == null) {
                    interest = mortgage.lastValidInterestTerm.interest;
                } else {
                    interest = mortgage.todayValidTerm.interest;
                }

                //Interest term column
                let todayValidTermDuration = "";
                if (mortgage.todayValidTerm == null) {
                    if (mortgage.lastValidInterestTerm.start && mortgage.lastValidInterestTerm.ende) {
                        todayValidTermDuration = moment(mortgage.lastValidInterestTerm.start).format("DD.MM.YYYY") + " - " + moment(mortgage.lastValidInterestTerm.ende).format("DD.MM.YYYY");
                    } else if (!mortgage.lastValidInterestTerm.ende && mortgage.lastValidInterestTerm.start) {
                        todayValidTermDuration = moment(mortgage.lastValidInterestTerm.start).format("DD.MM.YYYY") + " - ";
                    }
                } else {
                    if (mortgage.todayValidTerm.start && mortgage.todayValidTerm.ende) {
                        todayValidTermDuration = moment(mortgage.todayValidTerm.start).format("DD.MM.YYYY") + " - " + moment(mortgage.todayValidTerm.ende).format("DD.MM.YYYY");
                    } else if (!mortgage.todayValidTerm.ende && mortgage.todayValidTerm.start) {
                        todayValidTermDuration = moment(mortgage.todayValidTerm.start).format("DD.MM.YYYY") + " - ";
                    }
                }

                //Binding column
                let frameTermDuration: string = "";
                if (mortgage.frameTerm) {
                    if (mortgage.frameTerm.ende && mortgage.frameTerm.start) {
                        frameTermDuration = moment(mortgage.frameTerm.start).format("DD.MM.YYYY") + " - " + moment(mortgage.frameTerm.ende).format("DD.MM.YYYY");
                    } else if (!mortgage.frameTerm.ende && mortgage.frameTerm.start) {
                        frameTermDuration = moment(mortgage.frameTerm.start).format("DD.MM.YYYY") + " - ";
                    }
                }

                //If mortgage is overdue flag to show "chip text"
                let isOverdue;
                if (mortgage.todayValidTerm == null) {
                    isOverdue = true;
                } else {
                    isOverdue = false;
                }

                //Get isSwapped flag and if true calcualte percent value
                let swapped: any = {};
                if (mortgage.isSwapped) {
                    swapped.isSwapped = true;
                    if (mortgage.productType !== "ZINSSWAP") {
                        swapped.swapPercent = Math.round((100 - mortgage.notSwappedShare));
                    }
                } else {
                    swapped.isSwapped = false;
                }

                let clientAdvisor = mortgage.clientAdvisor.name ? mortgage.clientAdvisor.name : "";

                //Interest per year (CHF) column
                let interestPY = mortgage.interestPA ? mortgage.interestPA : "";

                let customer;
                if (!mortgage.customer.surname) {
                    customer = mortgage.customer.name;
                } else {
                    customer = mortgage.customer.name + " " + mortgage.customer.surname;
                } 

                let customerID;
                customerID = mortgage.customer.id ? mortgage.customer.id : "";


                mappedMortgages.push({
                    "mortgageId": mortgageId,
                    "groupedBy": groupedBy,
                    "mortgageName": mortgageName,
                    "productType": productType,
                    "todayValidTermDuration": todayValidTermDuration,
                    "interestRate": interest,
                    "restVolume": restVolume,
                    "lender": lenderName,
                    "frameTermDuration": frameTermDuration,
                    "isOverdue": isOverdue,
                    "swapped": swapped,
                    "clientAdvisor": clientAdvisor,
                    "interestPY": interestPY,
                    "customer": customer,
                    "customerID": customerID
                });

            });
        }
        return mappedMortgages;
    }



}