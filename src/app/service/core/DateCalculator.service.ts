import { Injectable } from "@angular/core";

@Injectable()

export class DateCalculator{
  

     isLeapYear(year)  {
      return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
    };

    getDaysInMonth(year, month) {
      var daysInFebruar = (this.isLeapYear(year) ? 29 : 28);
      return [31, daysInFebruar, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    };

    addMonths(date, months){
      var n = date.getDate(), dateToReturn;
      dateToReturn = new Date();
      dateToReturn.setDate(1);
      dateToReturn.setMonth(date.getMonth() + months);
      dateToReturn.setDate(Math.min(n, this.getDaysInMonth(date.getFullYear(), date.getMonth())));

      return dateToReturn;
    };

    getDatePlusMonthsString(months){
      var dateToDisplay = this.addMonths(new Date(), months);
      return dateToDisplay;
    };

    addDaysToDate(referenceDate, countOfDaysToAdd) {
      var d = new Date(referenceDate);
      d.setDate(d.getDate() + countOfDaysToAdd);
      return d;
    };

    dateIsWithinRange(rangeStart, analyzedDate, rangeEnd){

      var isAfterStart = true;
      var isBeforeEnd = true;

      if (!analyzedDate || analyzedDate === null) {
        return false;
      }

      if (rangeStart && rangeStart !== null) {
        isAfterStart = (rangeStart.setHours(0, 0, 0, 0) <= analyzedDate);
      }

      if (rangeEnd && rangeEnd !== null) {
        isBeforeEnd = (analyzedDate <= rangeEnd.setHours(23, 59, 59, 999));
      }

      return (isAfterStart && isBeforeEnd);
    };

    /**
     * Ermittelt das Datum des heutigen Tages, wobei die Uhrzeit nicht berücksichtigt wird.
     */
    getToday(){
      var today = new Date();
      today.setHours(0, 0, 0, 0);
      return today;
    };

    /**
     * Wenn Datumsobjekte verglichen werden, spielt die Uhrzeit eine wichtige Rolle. Um hier immer nur tagesgenau zu
     * vergleichen, normalisiert die Methode auf den Tag und entfernt Stunden, Minuten, Sekunden.
     * @param date
     */
    normalizeDateToDay(date){

      var processedDate;

      if (date instanceof Date) {
        processedDate = date;
      } else {
        processedDate = new Date(date);
      }

      processedDate.setHours(0, 0, 0, 0);
      return processedDate;

    };

  
  


  

      

  }