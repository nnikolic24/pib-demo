import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CreditService {

    constructor(private http: HttpClient) { }

    /**
    * Get Contacts from Contacts endpoint with id
    */
    fetchContact(id) {
        return this.http.get(environment.apiURL + environment.apiSufix + "/contacts/" + id);
    }

    /**
     * Get total loans
     * Depending on params lonas can be for client or admin user
     * @param preparedSearchParams 
     */
    fetchTotalLoansOnInit(preparedSearchParams) {
        let paged = encodeURIComponent(JSON.stringify(preparedSearchParams.paged));
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));

        return this.http.get(environment.apiURL + environment.apiSufix + `/totalloans/search?paged=${paged}&predicate=${predicates}`);
    }

    /**
     * Get single loan item for provided id
     * @param id of loan
     */
    fetchLoanData(id) {
        return this.http.get(environment.apiURL + environment.apiSufix + "/totalloans/" + id);
    }

    /**
    * Create new total loan item
    * @param id of loan
    */
    createTotalLoan(data) {
        return this.http.post(environment.apiURL + environment.apiSufix + "/totalloans/", data);
    }

    /**
    * Udate total loan item
    * @param id of loan
    */
    updateTotalLoanData(id, data) {
        return this.http.put(environment.apiURL + environment.apiSufix + "/totalloans/" + id, data);
    }

    /**
    * Get total loans
    * @param id of loan
    */
    fetchTotalLoans(preparedSearchParams) {
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/totalloans/search?predicate=${predicates}`);
    }

    /**
     * Get valid mortgages for user
     * @param preparedSearchParams 
     */
    fetchValidMortgages(preparedSearchParams) {
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search?predicate=${predicates}`);
    }

    /**
    * Get lenders
    * @param preparedSearchParams 
    */
    fetchLenders(preparedSearchParams) {
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/contacts/search?predicate=${predicates}`);
    }

    /**
     * Get overdue mortgages
     * In params there are id's of valid mortgages, and overdue must not be one of the valid
     * @param preparedSearchParams 
     */
    fetchOverdueMortgages(preparedSearchParams) {
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search?predicate=${predicates}`);
    }

    //Get mortgages grouped by last runuing end
    fetchGroupedOverdueMortgages(preparedSearchParams) {
        let grouped = preparedSearchParams.groupedBy;
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search/grouped?groupedBy=${grouped}&predicate=${predicates}`);
    }

    //Get mortgages grouped by last reserved end
    fetchGroupedReservedMortgages(preparedSearchParams) {
        let grouped = preparedSearchParams.groupedBy;
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));
        let referanceDay = preparedSearchParams.referenceday;
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search/grouped?groupedBy=${grouped}&predicate=${predicates}&referenceday=${referanceDay}`);
    }

    /**
    * Call that retrieves a single role by a name
    * @param role Roles name by which we serach it
    */
    retrieveUserByRole(role) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/users/byRoleName?role=${role}`);
    }


    retrieveGroupedMortgages(params) {
        let groupedBy = params.groupedBy;
        let predicate = encodeURIComponent(JSON.stringify(params.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search/grouped?groupedBy=${groupedBy}&predicate=${predicate}`);
    }

    getExcel(mortParams, totalLoanParams) {
        let predicate = encodeURIComponent(JSON.stringify(mortParams.predicate));
        let predicatesTotalLoan = encodeURIComponent(JSON.stringify(totalLoanParams.predicate));
        return environment.apiURL + environment.apiSufix + `/mortgage/listen/creditor?predicate=${predicate}&gesamtkreditpredicate=${predicatesTotalLoan}`;
    }


    /**
    * Get financing objects for provided query params
    * @param preparedSearchParams 
    */
    fetchFinanceObject(preparedSearchParams) {
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));
        return this.http.get(environment.apiURL + environment.apiSufix + `/financingobjects/search?predicate=${predicates}`);
    }

    getMortgageById(mortgageId) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/${mortgageId}`);
	}
	getAmortizationsByMortgageId(mortgageId) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/${mortgageId}/amortization`);
	}
	getRegularAmortizationsByMortgageId(mortgageId) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/${mortgageId}/regularamortization`);
	}
	addAmortizationToMortgage(mortgageId, amortizationObject) {
		return this.http.post(environment.apiURL + environment.apiSufix + `/mortgage/${mortgageId}/amortization`, amortizationObject);
	}
	deleteAmortizationFromMortgage(mortgageId, amortizationId) {
		return this.http.delete(environment.apiURL + environment.apiSufix + `/mortgage/${mortgageId}/amortization/${amortizationId}`);
	}
	updateAmortizationFromMortgage(mortgageId, amortizationObject) {
		return this.http.put(environment.apiURL + environment.apiSufix + `/mortgage/${mortgageId}/amortization/${amortizationObject.id}`, amortizationObject);
	}
	getMortgageVolume(mortgageId) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/${mortgageId}/volume`);
	}
	getMortgageInterestMaturities(mortgageId) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/${mortgageId}/interestMaturities`);
	}

}

