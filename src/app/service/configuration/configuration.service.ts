import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ConfigurationService {

    constructor(private http: HttpClient) { }

    getInterestRates() {
        return this.http.get(environment.apiURL + environment.apiSufix + '/interestSeries');
    }

    createNewRate(formData) {
        return this.http.post(environment.apiURL + environment.apiSufix + '/interestSeries', formData);
    }

    deleteRate(rate) {
        return this.http.delete(environment.apiURL + environment.apiSufix + '/interestSeries/' + rate.id);
    }

    updatRate(rate) {
        return this.http.put(environment.apiURL + environment.apiSufix + '/interestSeries/' + rate.id, rate);
    }

    createRateEntry(parentID, entry) {
        return this.http.post(environment.apiURL + environment.apiSufix + `/interestSeries/${parentID}/entries`, entry);
    }

    deleteRateEntry(parentID, entryID) {
        return this.http.delete(environment.apiURL + environment.apiSufix + `/interestSeries/${parentID}/entries/${entryID}`);
    }

    updateRateEntry(parentID, entry) {
        return this.http.put(environment.apiURL + environment.apiSufix + `/interestSeries/${parentID}/entries`, entry);
    }

    getAllInterestSeries(parentID) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/interestSeries/${parentID}/entries`);
    }

    search(params) {
        let url = environment.apiURL + environment.apiSufix + `/interestSeries/${params.parentId}/entries/search`;
        url += '?predicate=' + encodeURIComponent(JSON.stringify(params.predicate));
        return this.http.get(url);
    }
}

