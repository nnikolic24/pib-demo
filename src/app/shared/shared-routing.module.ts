import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreditGrowthComponent } from './credit-growth/credit-growth.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DueCreditsComponent } from './due-credits/due-credits.component';
import { InterestTermComponent } from './interest-term/interest-term.component';
import { InterestsComponent } from './interests/interests.component';
import { LenderStructureComponent } from './lender-structure/lender-structure.component';
import { OwnershipStructureComponent } from './ownership-structure/ownership-structure.component';
import { DurationComponent } from './duration/duration.component';

const routes: Routes = [{
    path: '',
    children: [
        {
            path: 'credit-growth',
            component: CreditGrowthComponent
        },
        {
            path: 'dashboard',
            component: DashboardComponent
        },
        {
            path: 'due-credits',
            component: DueCreditsComponent
        },
        {
            path: 'interest-term',
            component: InterestTermComponent
        },
        {
            path: 'interests',
            component: InterestsComponent
        },
        {
            path: 'lender-structure',
            component: LenderStructureComponent
        },
        {
            path: 'ownership-structure',
            component: OwnershipStructureComponent
        },
        {
            path: 'duration',
            component: DurationComponent
        },
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SharedRoutingModule { }
