import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'tab-interest-term',
  templateUrl: './interest-term.component.html',
  styleUrls: ['./interest-term.component.scss']
})
export class InterestTermComponent implements OnInit {

  @ViewChild('loansDueTable') loansDueTable: any;
  @ViewChild('openLimitsTable') openLimitsTable: any;
  
  public showChart: boolean = false;

  /*
     ---------- Bar Chart ----------
   */
  public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;

  public barChartData: any[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
  ];

  barChartColors: Array<any> = [{
    backgroundColor: '#661a13',
    borderColor: '#661a13',
    pointBackgroundColor: '#661a13',
    pointBorderColor: '#661a13',
    pointHoverBackgroundColor: '#661a13',
    pointHoverBorderColor: '#661a13'
  }];

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

 

  //TODO example data remove later
  totalLoansData: any = [
    {
      year: '2017',
      credit: "45000",
      proportion: "25"
    },
    {
      year: '2018',
      credit: "78000",
      proportion: "50"
    },
    {
      year: '2019',
      credit: "150000",
      proportion: "25"
    }
  ];
  public creditCount: number = 155206;
  public percentCount: string = "100%";

  //TODO example data remove later
  morgagesSampleData: any = [
    {
      year: '2017',
      financingProperty: '8000 Zürich, Glücksstrasse 17',
      credit: "750000",
      product: "Libor Mortgage",
      interestTerm: "01.03.2019 - 31.08.2019",
      remaningTime: "2 Months",
      nominalInterest: "0.750",
      interestRate: "0.760",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "5.495",
      lender: "Grosszügigkeitsbank",
      remarks: "Offene Limite für Umbauvorhaben"
    },
    {
      year: '2017',
      financingProperty: '7000 Chur, Forsterstrasse 1',
      credit: "750000",
      product: "Fix Rate Mortgage",
      interestTerm: "01.03.2019 - 31.08.2019",
      remaningTime: "2 Months",
      nominalInterest: "0.050",
      interestRate: "0.760",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "10.495",
      lender: "Heldenbank",
      remarks: "Offene Limite für Umbauvorhaben offene Limite für Umbauvorhaben."
    },
    {
      year: '2019',
      financingProperty: 'Zinsswaps Heldenbank (Swap)',
      credit: "350000",
      product: "Interest Rate Swapped",
      interestTerm: "01.05.2018 - 30.04.2024",
      remaningTime: "4 Years and 10 Months",
      nominalInterest: "1.570",
      interestRate: "1.570",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "7.495",
      lender: "Heldenbank Zinsswap",
      remarks: "Offene Limite für Umbauvorhaben"
    }
  ];
  enableSummary = true;
  summaryPosition = 'bottom';

  
  constructor() { }

  ngOnInit() {
  }


  /**TODO sum of all credist example */
  summaryForMortgagesCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForInterestPerYear(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForInterestRate(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForLoansCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForLoansProportion(cells: number[]) {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit + " %";
  }

  summaryForOverdueCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }


  initChart(){
    setTimeout(() => {
      this.showChart = true;
    }, 0);
  }

  //TODO expand grid row for mobile version
  toggleExpandRowOpenLimitsTable(row) {
    console.log('Toggled Expand Row!', row);
    this.openLimitsTable.rowDetail.toggleExpandRow(row);
  }
  
  //TODO expand grid row for mobile version
  toggleExpandLoansDueTable(row) {
    console.log('Toggled Expand Row!', row);
    this.loansDueTable.rowDetail.toggleExpandRow(row);
  }

  //TODO 
  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

}
