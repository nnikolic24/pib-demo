import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'user-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent implements OnInit {

  constructor() { }

   //TODO example data remove later
   morgagesSampleData: any = [
    {
      year: '2017',
      financingProperty: '8000 Zürich, Glücksstrasse 17',
      credit: "750000",
      product: "Libor Mortgage",
      interestTerm: "01.03.2019 - 31.08.2019",
      remaningTime: "2 Months",
      nominalInterest: "0.750",
      interestRate: "0.760",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "5.495",
      lender: "Grosszügigkeitsbank",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "70000",
      payments: "70000",
      date: "01.03.2019"
    },
    {
      year: '2017',
      financingProperty: '7000 Chur, Forsterstrasse 1',
      credit: "750000",
      product: "Fix Rate Mortgage",
      interestTerm: "01.03.2019 - 31.08.2019",
      remaningTime: "2 Months",
      nominalInterest: "0.050",
      interestRate: "0.760",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "10.495",
      lender: "Heldenbank",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "88000",
      payments: "70000",
      date: "13.08.2031"
    },
    {
      year: '2019',
      financingProperty: 'Zinsswaps Heldenbank (Swap)',
      credit: "350000",
      product: "Interest Rate Swapped",
      interestTerm: "01.05.2018 - 30.04.2024",
      remaningTime: "4 Years and 10 Months",
      nominalInterest: "1.570",
      interestRate: "1.570",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "7.495",
      lender: "Heldenbank Zinsswap",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "26300",
      payments: "70000",
      date: "27.03.2023"
    }
  ];


  

  ngOnInit() {
  }

}
