import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'tab-due-credits',
  templateUrl: './due-credits.component.html',
  styleUrls: ['./due-credits.component.scss']
})
export class DueCreditsComponent implements OnInit {
 
  @ViewChild('openLimitsTable') openLimitsTable: any;

  public showChart: boolean = false;

  /*
  ---------- Bar Chart ----------
*/
  public barChartLabelsDueCredits: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartTypeDueCredits: string = 'bar';
  public barChartLegendDueCredits: boolean = false;

  public barChartDataDueCredits: any[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
  ];

  barChartColorsDueCredits: Array<any> = [{
    backgroundColor: '#661a13',
    borderColor: '#661a13',
    pointBackgroundColor: '#661a13',
    pointBorderColor: '#661a13',
    pointHoverBackgroundColor: '#661a13',
    pointHoverBorderColor: '#661a13'
  }];

  public barChartOptionsDueCredits: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  //TODO example data remove later
  totalLoansData: any = [
    {
      month: '06.2019',
      credit: "10840000"
    },
    {
      month: '08.2019',
      credit: "750000"
    },
    {
      month: '12.2019',
      credit: "2450000"
    }
  ];
  public creditCount: number = 14040000;


  openLimitsData: any = [
    {
      creditVolume: '260000',
      lender: "Grosszügigkeitsbank",
      remarks: "Offene Limite für Umbauvorhaben"
    }
  ];

  //TODO example data remove later
  morgagesSampleData: any = [
    {
      year: '2017',
      financingProperty: '8000 Zürich, Glücksstrasse 17',
      credit: "750000",
      product: "Libor Mortgage",
      interestTerm: "01.03.2019 - 31.08.2019",
      remaningTime: "2 Months",
      nominalInterest: "0.750",
      interestRate: "0.760",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "5.495",
      lender: "Grosszügigkeitsbank",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "70000",
      date: "01.03.2019"
    },
    {
      year: '2017',
      financingProperty: '7000 Chur, Forsterstrasse 1',
      credit: "750000",
      product: "Fix Rate Mortgage",
      interestTerm: "01.03.2019 - 31.08.2019",
      remaningTime: "2 Months",
      nominalInterest: "0.050",
      interestRate: "0.760",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "10.495",
      lender: "Heldenbank",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "88000",
      date: "13.08.2031"
    },
    {
      year: '2019',
      financingProperty: 'Zinsswaps Heldenbank (Swap)',
      credit: "350000",
      product: "Interest Rate Swapped",
      interestTerm: "01.05.2018 - 30.04.2024",
      remaningTime: "4 Years and 10 Months",
      nominalInterest: "1.570",
      interestRate: "1.570",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "7.495",
      lender: "Heldenbank Zinsswap",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "26300",
      date: "27.03.2023"
    }
  ];
  enableSummary = true;
  summaryPosition = 'bottom';

  public creditVolumeCount: number = 260000;

  constructor() { }

  ngOnInit() {
  }

  /**TODO sum of all credist example */
  summaryForMortgagesCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForOverdueCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForLoansCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForInterestRate(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForInterestPerYear(cells: number[]): number {
    console.log(cells);
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }


  initChart() {
    setTimeout(() => {
      this.showChart = true;
    }, 0);
  }

  //TODO expand grid row for mobile version
  toggleExpandRowOpenLimitsTable(row) {
    console.log('Toggled Expand Row!', row);
    this.openLimitsTable.rowDetail.toggleExpandRow(row);
  }

  //TODO 
  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }


}
