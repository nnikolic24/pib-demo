import { Component, Input, ViewChild, TemplateRef } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'grouped-mortgage-table',
  templateUrl: './grouped-mortgage-table.component.html',
  styleUrls: ['./grouped-mortgage-table.component.scss']
})
export class GroupedMortgageTableComponent {

  @Input() public data: any; //Array for table
  @Input() public showSwapped: boolean; //Is it needed to show swapped field or not
  @Input() public currencySumm; //Value of currency summary
  @Input() public swappedPerc; //Value of swapped percentage
  @Input() public overdue; //Variable to shop different columns depending on the table
  @Input() public gridType: string; //Depending on grid type display different columns


  @ViewChild('overdueMortgageSummaryCell') overdueMortgageSummaryCell: TemplateRef<any>;
  @ViewChild('overdueMortgagesTable') overdueMortgagesTable: any;

  constructor(private router: Router) { }

  enableSummary = true;
  summaryPosition = 'bottom';

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  //Crediv olume column summary
  summaryRestVolume(cells: number[]) {
    let sumOfCredit: number = 0;
    let formatedSum: string = "";
    cells.forEach(item => sumOfCredit += Number(item));
    formatedSum = sumOfCredit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "'");
    return formatedSum;
  }

  /**
   * Go to mortgage display page
   * @param event on grid row click get id of mortgage
   */
  goToMortgage(event) {
    if (event.type == 'click') {
      this.router.navigate(['/credit/mortgage/', event.row.mortgageId]);
      console.log(event);
    }
  }

}
