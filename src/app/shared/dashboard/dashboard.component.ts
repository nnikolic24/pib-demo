import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tab-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public showChart: boolean = false;

  /*
    ---------- Bar Chart ----------
  */
  public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;

  public barChartData: any[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
  ];

  barChartColors: Array<any> = [{
    backgroundColor: '#661a13',
    borderColor: '#661a13',
    pointBackgroundColor: '#661a13',
    pointBorderColor: '#661a13',
    pointHoverBackgroundColor: '#661a13',
    pointHoverBorderColor: '#661a13'
  }];

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };


  /**TODO line chart options */
  public lineChartLabels: Array<any> = ['05.2019', '06.2019', '07.2019', '08.2019'];
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Volume amaunt (CHF)'
        }
      }]
    }
  };
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';

  public lineChartSteppedData: Array<any> = [{
    data: [65, 59, 80, 81, 56, 55, 40],
    label: 'Series A',
    borderWidth: 1,
    fill: false,
    steppedLine: true
  }, {
    data: [28, 48, 40, 19, 86, 27, 90],
    label: 'Series B',
    borderWidth: 1,
    fill: false,
    steppedLine: true
  }, {
    data: [18, 48, 77, 9, 100, 27, 40],
    label: 'Series C',
    borderWidth: 1,
    fill: false,
    steppedLine: true
  }];

  /*
    ---------- Doughnut Chart ----------
  */
  public doughnutChartData: number[] = [350, 100];
  public doughnutChartType: string = 'doughnut';
  public pieChartColors: any[] = [{
    backgroundColor: ['#661a13', '#b74137']
  }];
  public PieChartOptions: any = {
    elements: {
      arc: {
        borderWidth: 0
      }
    },
    legend: {
      position: "left"
    }
  }
  public doughnatChartLabels: Array<any> = ['Label one', 'Label two'];
  public doughnutLegend: boolean = true;


  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.showChart = true;
    }, 0);
  }

}
