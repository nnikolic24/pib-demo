import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatIconModule, 
  MatListModule, MatDatepickerModule, MatCheckboxModule, MatTableModule, MatTabsModule, 
  MatDividerModule, MatProgressBarModule, MatRadioModule, MatSelectModule, MatDialogModule, MatChipsModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChartsModule } from 'ng2-charts';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
//import { SharedRoutingModule } from './shared-routing.module';


import { CreditGrowthComponent } from './credit-growth/credit-growth.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DueCreditsComponent } from './due-credits/due-credits.component';
import { InterestTermComponent } from './interest-term/interest-term.component';
import { InterestsComponent } from './interests/interests.component';
import { LenderStructureComponent } from './lender-structure/lender-structure.component';
import { OwnershipStructureComponent } from './ownership-structure/ownership-structure.component';
import { DurationComponent } from './duration/duration.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { CockpitComponent } from './cockpit/cockpit.component';

import { RegisterUserDialogComponent } from './dialogs/registration-dialog';
import { SupportInfoDialogComponent } from './dialogs/support-info-dialog';
import { ConsultantDialogComponent } from './dialogs/consultant-info-dialog';

import { CapitalizeFirstPipe } from './pipes/capitalizefirst.pipe';
import { GroupedMortgageTableComponent } from './grouped-mortgage-table/grouped-mortgage-table.component';

@NgModule({
  declarations: [
    CreditGrowthComponent,
    DashboardComponent,
    DueCreditsComponent,
    InterestTermComponent,
    InterestsComponent,
    LenderStructureComponent,
    OwnershipStructureComponent,
    DurationComponent,
    CustomerDetailsComponent,
    CockpitComponent,
    RegisterUserDialogComponent,
    SupportInfoDialogComponent,
    ConsultantDialogComponent,
    CapitalizeFirstPipe,
    GroupedMortgageTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    //SharedRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatTableModule,
    MatTabsModule,
    NgxDatatableModule,
    ChartsModule,
    NgSelectModule,
    TranslateModule,
    MatDividerModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSelectModule,
    MatDialogModule,
    MatChipsModule
  ],
  exports: [
    CreditGrowthComponent,
    DashboardComponent,
    DueCreditsComponent,
    InterestTermComponent,
    InterestsComponent,
    LenderStructureComponent,
    OwnershipStructureComponent,
    DurationComponent,
    CustomerDetailsComponent,
    CockpitComponent,
    RegisterUserDialogComponent,
    SupportInfoDialogComponent,
    ConsultantDialogComponent,
    CapitalizeFirstPipe,
    GroupedMortgageTableComponent
  ]
})
export class SharedModule { }
