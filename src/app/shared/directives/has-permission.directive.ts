import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
    selector: '[secure-show]'
})
export class HasPermissionDirective {

    @Input() resource: string;
    @Input() privilege: string;

    constructor(el: ElementRef) { }

    ngOnInit() { }
}