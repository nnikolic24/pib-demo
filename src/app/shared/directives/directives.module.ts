import { HasPermissionDirective } from './../directives/has-permission.directive';

import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        HasPermissionDirective
    ],
    imports: [],
    exports: [
        HasPermissionDirective
    ]
})
export class DirectivesModule { }
