import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
    template: `
    <h3>Contact support</h3>

    <mat-dialog-content class="dialog-w">   
    <div class="gene-card-content">

        <div fxLayout="row wrap" fxLayoutAlign="start center">
            <div fxFlex.gt-xs="100" fxFlex="100">
                <mat-card class="full-wid mrgn-b-md" style="margin: 5px 0px !important; padding: 10px !important" (click)="sendEmail()">
                    <div class="gene-card-content label-pdng">
                        <label style="cursor: pointer">Technical problems?</label>
                    </div>
                </mat-card>
            </div>
        </div>

        <div fxLayout="row wrap" fxLayoutAlign="start center">
            <div fxFlex.gt-xs="100" fxFlex="100">
                <mat-card class="full-wid mrgn-b-md" style="margin: 5px 0px !important; padding: 10px !important" (click)="sendEmail()">
                    <div class="gene-card-content label-pdng">
                        <label style="cursor: pointer">Customize profile?</label>
                    </div>
                </mat-card>
            </div>
        </div>

        <div fxLayout="row wrap" fxLayoutAlign="start center">
            <div fxFlex.gt-xs="100" fxFlex="100">
                <mat-card class="full-wid mrgn-b-md" style="margin: 5px 0px !important; padding: 10px !important" (click)="sendEmail()">
                    <div class="gene-card-content label-pdng">
                        <label style="cursor: pointer">Report any discrepancies?</label>
                    </div>
                </mat-card>
            </div>
        </div>

        <div fxLayout="row wrap" fxLayoutAlign="start center">
            <div fxFlex.gt-xs="100" fxFlex="100">
                <mat-card class="full-wid mrgn-b-md" style="margin: 5px 0px !important; padding: 10px !important" (click)="sendEmail()">
                    <div class="gene-card-content label-pdng">
                        <label style="cursor: pointer">Do you have suggestions for improvement?</label>
                    </div>
                </mat-card>
            </div>
        </div>

        <div fxLayout="row wrap" fxLayoutAlign="start center" style="margin-top: 20px">
            <div fxFlex.gt-xs="100" fxFlex="100">
                <div class="gene-card-content label-pdng">
                    <mat-card class="full-wid mrgn-b-md" style="margin: 5px 0px !important; padding: 10px !important" (click)="sendEmail()">
                        <label style="cursor: pointer">info@oxifina.ch</label>
                    </mat-card>
                </div>
                <div class="gene-card-content label-pdng">
                    <label>T 043 333 111 222</label>
                </div>
                <div class="gene-card-content label-pdng">
                    <label>(weekdays, 8:00 am - 12:00 pm / 2:00 pm - 5:00 pm)</label>
                </div>
            </div>
        </div>
  
    </div>
    </mat-dialog-content>

    <mat-dialog-actions style="text-align: right">
      <button type="button" mat-raised-button color="default" style="margin-right: 0px" (click)="dialogRef.close()">Close</button>
    </mat-dialog-actions>
  `,
    styles: ['.dialog-w{ width: 450px !important } ::ng-deep .mat-dialog-container{ background: #f0f2f7 !important } .label-pdng{ padding-top: 0px !important; padding-bottom: 0px !important }  @media screen and (max-width: 400px) { .dialog-w{ width: 350px !important } }']
})
export class SupportInfoDialogComponent {

    constructor(public dialogRef: MatDialogRef<SupportInfoDialogComponent>) {
        dialogRef.disableClose = true;
    }

    // Open MS Outlook to send an email
    sendEmail() {
        let email = "mailto:info@oxifina.ch"
        window.location.href = email;
    }
}