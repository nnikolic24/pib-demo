import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AuthService } from 'app/service/auth-service/auth.service';

@Component({
    template: `
    <mat-dialog-content class="dialog-w">   
    <div class="gene-card-content">

        <div fxLayout="row wrap" fxLayoutAlign="start center">
            <div fxFlex.gt-xs="100" fxFlex="100">
                <img src="assets/img/avatar.jpg" alt="Consultant" width="250" height="250">
            </div>
        </div>

        <div fxLayout="row wrap" fxLayoutAlign="start center">
            <div fxFlex.gt-xs="100" fxFlex="100">
                <h3>{{ userData.firstName + ' ' + userData.lastName }}</h3>
            </div>
        </div>

        <div fxLayout="row wrap" fxLayoutAlign="start center">
            <div fxFlex.gt-xs="100" fxFlex="100">
                <div class="gene-card-content label-pdng">
                    <mat-card class="full-wid mrgn-b-md" style="margin: 5px 0px !important; padding: 10px !important" (click)="sendEmail()">
                        <label style="cursor: pointer">{{ userData.email }}</label>
                    </mat-card>
                </div>
                <div class="gene-card-content label-pdng">
                    <label>T {{ userData.mobileNumber }}</label>
                </div>
                <div class="gene-card-content label-pdng" style="margin-top: 20px">
                    <label style="font-weight: bold">Address:</label>
                </div>
                <div class="gene-card-content label-pdng">
                    <label>Lorem Ipsum</label>
                </div>
                <div class="gene-card-content label-pdng">
                    <label>Lorem Ipsum</label>
                </div>
                <div class="gene-card-content label-pdng">
                    <label>Lorem Ipsum</label>
                </div>
            </div>
        </div>
  
    </div>
    </mat-dialog-content>

    <mat-dialog-actions style="text-align: right">
      <button type="button" mat-raised-button color="default" style="margin-right: 0px" (click)="dialogRef.close()">Close</button>
    </mat-dialog-actions>
  `,
    styles: ['.dialog-w{ width: 400px !important } ::ng-deep .mat-dialog-container{ background: #f0f2f7 !important } .label-pdng{ padding-top: 0px !important; padding-bottom: 0px !important } @media screen and (max-width: 400px) { .dialog-w{ width: 350px !important }']
})
export class ConsultantDialogComponent implements OnInit {

    userData: any;

    constructor(public dialogRef: MatDialogRef<ConsultantDialogComponent>, 
                private authService: AuthService) {
        dialogRef.disableClose = true;
    }

    ngOnInit() {
        this.userData = this.authService.getLocalStorageUser();
    }

    sendEmail() {
        let email = 'mailto: ' + this.userData.email;
        window.location.href = email;
    }
}