import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    template: `
    <h1 mat-dialog-title>Register</h1>

    <mat-dialog-content class="dialog-w">   
    <form [formGroup]="registrationForm" (ngSubmit)="onSubmit()">
        <div class="gene-card-content">
            <div fxLayout="row wrap" fxLayoutAlign="start center">
                <div fxFlex.gt-xs="100" fxFlex="100">
                    <mat-form-field class="full-wid mrgn-b-md">
                        <input matInput 
                               placeholder="Firstname" 
                               formControlName="firstName" 
                               required>
                        <mat-hint *ngIf="registrationForm.controls['firstName'].hasError('required') && registrationForm.controls['firstName'].touched">First name is required field.</mat-hint>           
                    </mat-form-field>
                </div>
            </div>
    
            <div fxLayout="row wrap" fxLayoutAlign="start center">
                <div fxFlex.gt-xs="100" fxFlex="100">
                    <mat-form-field class="full-wid mrgn-b-md">
                        <input matInput 
                               placeholder="Lastname" 
                               formControlName="lastName"
                               required>
                        <mat-hint *ngIf="registrationForm.controls['lastName'].hasError('required') && registrationForm.controls['lastName'].touched">First name is required field.</mat-hint>                  
                    </mat-form-field>
                </div>                
            </div>
    
            <div fxLayout="row wrap" fxLayoutAlign="start center">
                <div fxFlex.gt-xs="100" fxFlex="100">
                    <mat-form-field class="full-wid mrgn-b-md">
                        <input matInput 
                               placeholder="Email" 
                               formControlName="email"
                               required>                
                        <mat-hint *ngIf="registrationForm.controls['email'].hasError('required') && registrationForm.controls['email'].touched">Email address is required field.</mat-hint>
                        <mat-hint *ngIf="registrationForm.controls['email'].errors && !(registrationForm.controls['email'].hasError('required')) && registrationForm.controls['email'].touched">Please enter a valid email address.</mat-hint>
                    </mat-form-field>
                  </div>
            </div>
    
            <div fxLayout="row wrap" fxLayoutAlign="start center">
                <div fxFlex.gt-xs="100" fxFlex="100">
                    <mat-form-field class="full-wid">
                        <input matInput 
                               placeholder="Phone number" 
                               formControlName="phoneNumber"
                               required>
                        <mat-hint *ngIf="registrationForm.controls['phoneNumber'].hasError('required') && registrationForm.controls['phoneNumber'].touched">Phone number is required field.
                        </mat-hint>
                    </mat-form-field>
                </div>
            </div>
        </div>
        <mat-dialog-actions style="text-align: right">
            <button type="button" mat-raised-button color="default" type="submit" [disabled]="!registrationForm.valid">Register</button>
            <button type="button" mat-raised-button color="default" style="margin-right: 0px" (click)="dialogRef.close()">Close</button>
        </mat-dialog-actions>
    </form>    
    </mat-dialog-content>

  `,
    styles: ['.dialog-w{ width: 400px !important } ::ng-deep .mat-dialog-container{ background: #f0f2f7 !important } @media screen and (max-width: 400px) { .dialog-w{ width: 350px !important } { small { margin-top: -20px !important ; } }']
})
export class RegisterUserDialogComponent implements OnInit {

    registrationForm: FormGroup;

    constructor(public dialogRef: MatDialogRef<RegisterUserDialogComponent>) {
        dialogRef.disableClose = true;
    }

    ngOnInit() {
        this.registerUser();
    }

    private registerUser() {
        this.registrationForm = new FormGroup({
            firstName: new FormControl('', Validators.required),
            lastName: new FormControl('', Validators.required),
            email: new FormControl('', [Validators.required, Validators.email]),
            phoneNumber: new FormControl('', Validators.required)
        })
    }

    onSubmit() {
        console.log(this.registrationForm.value);
    }
}