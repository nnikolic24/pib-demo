import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ms-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.scss']
})
export class CockpitComponent implements OnInit {


  showChart : boolean;

   /*
      ---------- Doughnut Chart configuration ----------
   */
  public doughnutChartData:number[] = [350, 450, 100];
  public doughnutChartType:string = 'doughnut';
  pieChartColors: any[] = [{
     backgroundColor: ['#3B55E6', '#EB4E36', '#43D29E']
  }];
  PieChartOptions: any = {
     elements: {
        arc: {
           borderWidth: 0
        }
     },
     legend:{
        position:"left"
     }
  }

  public doughnatChartLabels: Array<any> = ['05.2019', '06.2019', '07.2019'];

  doughnutLegend: boolean = true;


   /*
  ---------- Bar Chart ----------
*/
public barChartLabelsMaturity: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
public barChartTypeMaturity: string = 'bar';
public barChartLegendMaturity: boolean = false;

public barChartMaturityData: any[] = [
  { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
];

barChartColorsMaturity: Array<any> = [{
  backgroundColor: '#661a13',
  borderColor: '#661a13',
  pointBackgroundColor: '#661a13',
  pointBorderColor: '#661a13',
  pointHoverBackgroundColor: '#661a13',
  pointHoverBorderColor: '#661a13'
}];

public barChartOptionsMaturity: any = {
  scaleShowVerticalLines: false,
  responsive: true
};

 /**TODO line chart options */
 public lineChartLabels: Array<any> = ['05.2019', '06.2019', '07.2019', '08.2019'];
 public lineChartOptions: any = {
   responsive: true,
   scales: {
     yAxes: [{
       scaleLabel: {
         display: true,
         labelString: 'Volume amaunt (CHF)'
       }
     }]
   }
 };
 public lineChartLegend: boolean = true;
 public lineChartType: string = 'line';

 public lineChartSteppedData: Array<any> = [{
   data: [65, 59, 80, 81, 56, 55, 40],
   label: 'Series A',
   borderWidth: 1,
   fill: false,
   steppedLine: true
 }, {
   data: [28, 48, 40, 19, 86, 27, 90],
   label: 'Series B',
   borderWidth: 1,
   fill: false,
   steppedLine: true
 }, {
   data: [18, 48, 77, 9, 100, 27, 40],
   label: 'Series C',
   borderWidth: 1,
   fill: false,
   steppedLine: true
 }];

  constructor() { }

  ngOnInit() {
  }



  initChart() {
    setTimeout(() => {
      this.showChart = true;
    }, 0);
  }

}
