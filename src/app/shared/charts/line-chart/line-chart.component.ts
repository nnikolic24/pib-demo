import {
    Component,
    Input,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    NgZone,
    ViewChild,
    Output,
    EventEmitter
} from '@angular/core';

import {
    LineChartMethods,
    LineChartInput,
    LineChartOutput,
    LineChartOutputEvent
} from './line-chart.misc';

import * as moment from 'moment';
import { FormControl } from '@angular/forms';

declare var Chart: any;

@Component({
    selector: 'line-chart',
    templateUrl: './line-chart.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LineChartComponent {

    @ViewChild('myCanvas') canvas: any;

    @Output() output = new EventEmitter<LineChartOutput>();
    @Input() input: EventEmitter<LineChartInput>;
    @Input() serverSideFilter: boolean = false;
    @Input() height: number = 300;
    @Input() yLabel: string;

    public from = new FormControl();
    public to = new FormControl();

    public steppedLine: boolean = true;
    private dataset: Array<any> = [];
    private options: any;

    private chartColors = ['#661A13', '#EA7B71', '#E53A2A', '#664F4D', '#B22D21'];
    private _dataset;
    private windowKey = "CHART_REFS";
    private chartKey = "";
    private xAxisType = "time";
    private xAxisDateFormat = "MM. YYYY";
    private xAxisUnit = "year";
    private yAxisLabel;
    private logsEnabled: boolean = false;

    constructor(private cd: ChangeDetectorRef,
        private zone: NgZone) {
        this.chartKey = '_' + Math.random().toString(36).substr(2, 9);;
        if (!window[this.windowKey]) {
            window[this.windowKey] = {};
        }
    }

    ngOnChanges() {
        if (this.yAxisLabel && this.yAxisLabel !== this.yLabel) {
            this.yAxisLabel = this.yLabel;
            this.updateYLabel();
        }
    }

    private updateYLabel() {
        this.inform("Updating y label");
        if (this.options.scales.yAxes[0].scaleLabel) {
            this.options.scales.yAxes[0].scaleLabel.labelString = this.yAxisLabel;
            window[this.windowKey][this.chartKey].options.scales.yAxes[0].scaleLabel.labelString = this.yAxisLabel;
            this.zone.runOutsideAngular(() => {
                window[this.windowKey][this.chartKey].update();
            });
        }

    }

    ngOnInit() {
        this.yAxisLabel = this.yLabel;
    }

    ngOnDestroy() {
        this.destoryChart();
    }

    ngAfterViewInit() {
        this.input.subscribe((event: LineChartInput) => {
            switch (event.method) {
                case LineChartMethods.INIT: this.init(event.payload); break;
                case LineChartMethods.UPDATE_DATA: this.processUpdateEvent(event.payload.dataset); break;
            }
        })
        this.output.emit({
            event: LineChartOutputEvent.COMPONENT_READY,
            payload: null
        });
    }

    private destoryChart() {
        if (window[this.windowKey][this.chartKey]) {
            window[this.windowKey][this.chartKey].destroy();
            delete window[this.windowKey][this.chartKey];
            this.warn("Chart destoryed");
        }
    }

    private init(payload) {
        this.logsEnabled = payload.enableLogger;
        this.destoryChart();
        this.inform("Running initialization");
        if (payload.dataset) {
            this.dataset = payload.dataset;
        } else {
            this.warn("Data set is missing");
        }
        if (!payload.options) {
            this.options = {};
            let xAxisDisplayFormat: any = {};
            xAxisDisplayFormat[this.xAxisUnit] = this.xAxisDateFormat;
            let yAxisOptions: any = {};
            yAxisOptions.ticks = {
                suggestedMax: 6
            }
            if (this.yAxisLabel) {
                yAxisOptions.scaleLabel = {
                    display: true,
                    labelString: this.yAxisLabel
                }
            }
            this.options.elements = {
                line: {
                    tension: 0
                }
            }
            this.options.scales = {
                xAxes: [{
                    type: this.xAxisType,
                    time: {
                        displayFormats: xAxisDisplayFormat,
                        unit: this.xAxisUnit
                    }
                }],
                yAxes: [yAxisOptions]
            }
            this.options.tooltips = {
                callbacks: {
                    title: this.labelTitleCorrectionCallback,
                    label: this.labelBodyCorrectionCallback
                }
            }
            this.options.responsive = true;
            this.options.maintainAspectRatio = false
            this.warn("Options are missing. Using default instead");
        } else {
            // Otherwise use users config if present
            this.options = payload.options;
        }
        if (payload.steppedLine !== undefined) {
            this.steppedLine = payload.steppedLine;
        } else {
            this.warn("Stepped line option is missing. Using default (true) instead");
        }
        this._dataset = JSON.parse(JSON.stringify(this.dataset));
        this.processData();
        this.setUpDateFilters();
        this._init();
    }

    private processData() {
        this.correctTimes();
        this.sortByDate();
        this.applyStyles();
    }

    private labelTitleCorrectionCallback(tooltipItem, data) {
        let label = data.datasets[tooltipItem[0].datasetIndex].data[tooltipItem[0].index].t.format('DD.MM.YYYY');
        return label;
    }

    private setUpDateFilters() {
        let bounds: any = this.findDateBounds();
        this.from.setValue(bounds.first.toDate());
        this.to.setValue(bounds.last.toDate());
    }

    private findDateBounds() {
        let _first = moment();
        let _last = moment();
        this._dataset.forEach(element => {
            if (element.data.length) {
                if (moment(element.data[0].t).isBefore(_first)) {
                    _first = moment(element.data[0].t);
                }
                // let lastElementIndex = element.data.length - 1;
                // if (moment(element.data[lastElementIndex].t).isAfter(_last)) {
                //     _last = moment(element.data[lastElementIndex].t);
                // }
            }
        });
        return {
            first: _first,
            last: _last
        };
    }

    private sortByDate() {
        this._dataset.forEach(element => {
            element.data = element.data.sort((a, b) => {
                let dateA = moment(a.t).toDate();
                let dateB = moment(b.t).toDate();
                return dateA.valueOf() - dateB.valueOf()
            })
        });
    }

    private labelBodyCorrectionCallback(tooltipItem, data) {
        var ogLabel = data.datasets[tooltipItem.datasetIndex].label || '';
        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].y;
        return ogLabel + " " + value + "%";
    }

    private applyStyles() {
        let colorIndex = 0;
        this._dataset.forEach((element) => {
            element.steppedLine = this.steppedLine;
            element.fill = false;
            element.borderWidth = 1;
            element.borderColor = this.chartColors[colorIndex];
            element.backgroundColor = this.adjustBrightness(this.chartColors[colorIndex], 90);
            colorIndex++;
            if (colorIndex > 4) {
                colorIndex = 0;
            }
        });
    }

    private correctTimes() {
        this._dataset.forEach(element => {
            element.data.forEach(dataElement => {
                dataElement.t = moment(dataElement.t);
            });
        });
    }

    private _init() {
        this.zone.runOutsideAngular(() => {
            let canvas: any = this.canvas.nativeElement;
            let ctx = canvas.getContext('2d');
            window[this.windowKey][this.chartKey] = new Chart(ctx, {
                type: "line",
                data: {
                    datasets: this._dataset
                },
                options: this.options
            });
            this.cd.detectChanges();
        });
    }

    private adjustBrightness(col, amt) {
        let usePound = false;
        if (col[0] == "#") {
            col = col.slice(1);
            usePound = true;
        }
        let R = parseInt(col.substring(0, 2), 16);
        let G = parseInt(col.substring(2, 4), 16);
        let B = parseInt(col.substring(4, 6), 16);
        R = R + amt;
        G = G + amt;
        B = B + amt;
        if (R > 255) R = 255;
        else if (R < 0) R = 0;
        if (G > 255) G = 255;
        else if (G < 0) G = 0;
        if (B > 255) B = 255;
        else if (B < 0) B = 0;
        let RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
        let GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
        let BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));
        return (usePound ? "#" : "") + RR + GG + BB;
    }

    private processUpdateEvent(dataset) {
        this.inform("Preparing data for update");
        this._dataset = dataset;
        this.processData();
        this.updateGraph(this._dataset);
    }

    private updateGraph(dataset) {
        this.inform("Updating view");
        this.zone.runOutsideAngular(() => {
            window[this.windowKey][this.chartKey].data.datasets = dataset;
            window[this.windowKey][this.chartKey].update();
        });
    }

    public changeLineStyle(event) {
        this.inform("Changing line type");
        this.zone.runOutsideAngular(() => {
            window[this.windowKey][this.chartKey].data.datasets.forEach(dataset => {
                dataset.steppedLine = !dataset.steppedLine;
            })
            window[this.windowKey][this.chartKey].update();
        });
    }

    private inform(message) {
        if (this.logsEnabled) {
            console.log('[CHART WITH ID: ' + this.chartKey + ']: ' + message);
        }
    }

    private warn(message) {
        if (this.logsEnabled) {
            console.warn('[CHART WITH ID: ' + this.chartKey + ']: ' + message);
        }
    }

    public dateFilterChanged(event) {
        if (!this.serverSideFilter) {
            this.applyDateFilter(this.from.value, this.to.value);
        } else {
            this.output.emit({
                event: LineChartOutputEvent.DATE_FILTER_CHANGED,
                payload: {
                    from: moment(this.from.value),
                    to: moment(this.to.value)
                }
            });
        }
    }

    private applyDateFilter(from, to) {
        let momentFrom = moment(from);
        let momentTo = moment(to);
        this.inform(`Prepare to filter data [${momentFrom.format('DD.MM.YYYY')}, ${momentTo.format('DD.MM.YYYY')}]`);
        let subset = [];
        this._dataset.forEach(element => {
            let copy: any = {
                backgroundColor: element.backgroundColor,
                borderColor: element.borderColor,
                borderWidth: element.borderWidth,
                fill: element.fill,
                label: element.label,
                steppedLine: element.steppedLine,
                data: []
            };
            element.data.forEach(node => {
                if (moment(node.t).isBetween(momentFrom, momentTo, null, '[]')) {
                    copy.data.push(Object.assign({}, node));
                }
            });
            subset.push(copy);
        });
        this.updateGraph(subset);
    }
}
