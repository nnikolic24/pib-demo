export enum LineChartMethods {
    INIT = 0,
    UPDATE_DATA = 1
}

export enum LineChartOutputEvent {
    COMPONENT_READY = 0,
    DATE_FILTER_CHANGED = 1
}

export interface LineChartInput {
    method: LineChartMethods;
    payload: any;
}

export interface LineChartOutput {
    event: LineChartOutputEvent;
    payload: any;
}