import { NgModule } from '@angular/core';

import {
    MatDatepickerModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

import { LineChartComponent } from './line-chart/line-chart.component';

@NgModule({
    declarations: [
        LineChartComponent
    ],
    imports: [
        FlexLayoutModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatCheckboxModule,
        FormsModule,
        MatInputModule,
        ReactiveFormsModule,
        TranslateModule
    ],
    exports: [
        LineChartComponent
    ]
})
export class ChartsModule { }
