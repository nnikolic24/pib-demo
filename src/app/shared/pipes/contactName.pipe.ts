import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'contactName'
})
export class ContactNamePipe implements PipeTransform {
    transform(value: any, args: any[]): string {
        if (!value) return '';
        if (value.surname) {
            return value.name + ' ' + value.surname;
        } else {
            return value.name;
        }
    }
}