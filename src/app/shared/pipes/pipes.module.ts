// import { CapitalizeFirstPipe } from './../pipes/capitalizefirst.pipe';

import { ContactNamePipe } from './contactName.pipe';

import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        // CapitalizeFirstPipe
        ContactNamePipe
    ],
    imports: [],
    exports: [
        // CapitalizeFirstPipe
        ContactNamePipe
    ]
})
export class PipesModule { }
