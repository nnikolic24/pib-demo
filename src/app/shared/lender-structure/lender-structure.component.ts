import { Component, OnInit,ViewChild } from '@angular/core';

@Component({
  selector: 'tab-lender-structure',
  templateUrl: './lender-structure.component.html',
  styleUrls: ['./lender-structure.component.scss']
})
export class LenderStructureComponent implements OnInit {

  @ViewChild('payoutsTable') payoutsTable: any;
  @ViewChild('amortizationTable') amortizationTable: any;
  @ViewChild('openLimitsTable') openLimitsTable: any;
  @ViewChild('lendersTable') lendersTable: any;

  public showChart: boolean = false;

  public screenWidth: any;

  /*
    ---------- Doughnut Chart ----------
  */
  public doughnutChartData: number[] = [350, 100];
  public doughnutChartType: string = 'doughnut';
  public pieChartColors: any[] = [{
    backgroundColor: ['#661a13', '#b74137']
  }];
  public PieChartOptions: any = {
    elements: {
      arc: {
        borderWidth: 0
      }
    },
    legend: {
      position: "left"
    }
  }
  public doughnatChartLabels: Array<any> = ['Grosszügigkeitsbank', 'Heldenbank Zinsswap'];
  public doughnutLegend: boolean = true;

  //TODO example data remove later
  morgagesSampleData: any = [
    {
      year: '2017',
      financingProperty: '8000 Zürich, Glücksstrasse 17',
      credit: "750000",
      product: "Libor Mortgage",
      interestTerm: "01.03.2019 - 31.08.2019",
      remaningTime: "2 Months",
      nominalInterest: "0.750",
      interestRate: "0.760",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "5.495",
      lender: "Grosszügigkeitsbank",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "70000",
      payments: "70000",
      date: "01.03.2019",
      proportion: "35"
    },
    {
      year: '2017',
      financingProperty: '7000 Chur, Forsterstrasse 1',
      credit: "750000",
      product: "Fix Rate Mortgage",
      interestTerm: "01.03.2019 - 31.08.2019",
      remaningTime: "2 Months",
      nominalInterest: "0.050",
      interestRate: "0.760",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "10.495",
      lender: "Heldenbank",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "88000",
      payments: "70000",
      date: "13.08.2031",
      proportion: "35"
    },
    {
      year: '2019',
      financingProperty: 'Zinsswaps Heldenbank (Swap)',
      credit: "350000",
      product: "Interest Rate Swapped",
      interestTerm: "01.05.2018 - 30.04.2024",
      remaningTime: "4 Years and 10 Months",
      nominalInterest: "1.570",
      interestRate: "1.570",
      binding: "01.03.2019 - 31.08.2019",
      interestPerYear: "7.495",
      lender: "Heldenbank Zinsswap",
      remarks: "Offene Limite für Umbauvorhaben",
      amortization: "26300",
      payments: "70000",
      date: "27.03.2023",
      proportion: "30"
    }
  ];
  enableSummary = true;
  summaryPosition = 'bottom';

  constructor() { }

  ngOnInit() {
    this.screenWidth = window.innerWidth;
    console.log("this.innerWidth", this.screenWidth);
  }

  /**TODO sum of all credist example */
  summaryForMortgagesCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForInterestPerYear(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForInterestRate(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForLoansCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  summaryForLoansProportion(cells: number[]) {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit + " %";
  }

  summaryForOverdueCredit(cells: number[]): number {
    let sumOfCredit: number = 0;
    cells.forEach(item => sumOfCredit += Number(item));
    return sumOfCredit;
  }

  toggleExpandGroup(group) {
    this.payoutsTable.groupHeader.toggleExpandGroup(group);
    this.amortizationTable.groupHeader.toggleExpandGroup(group);
  }

  initChart() {
    setTimeout(() => {
      this.showChart = true;
    }, 0);
  }


  //TODO expand grid row for mobile version
  toggleExpandRowOpenLimitsTable(row) {
    console.log('Toggled Expand Row!', row);
    this.openLimitsTable.rowDetail.toggleExpandRow(row);
  }

  //TODO expand grid row for mobile version
  toggleExpandLendersTable(row) {
    console.log('Toggled Expand Row!', row);
    this.lendersTable.rowDetail.toggleExpandRow(row);
  }

  //TODO 
  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }
  
}
