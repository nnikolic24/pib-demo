import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AuthGuard } from './core/guards/auth.guard';

const appRoutes: Routes = [
   {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full',
   },
   {
      path: '',
      component: MainComponent,
      canActivate: [AuthGuard],
      runGuardsAndResolvers: 'always',
      children: [
         { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
         { path: 'settings', loadChildren: './settings/settings.module#SettingsModule' },
         { path: 'configurations', loadChildren: './configuration/configuration.module#ConfigurationModule' },
         { path: 'contacts', loadChildren: './contacts/contacts.module#ContactsModule' },
         { path: 'orders', loadChildren: './orders/orders.module#OrdersModule' },
         { path: 'properties', loadChildren: './properties/properties.module#PropertiesModule' },
         { path: 'credit', loadChildren: './credit/credit.module#CreditModule' },
         { path: 'credit-client', loadChildren: './credit-client/credit-client.module#CreditClientModule' },
         { path: 'home-client', loadChildren: './home-client/home-client.module#HomeClientModule' },
         { path: 'properties-client', loadChildren: './properties-client/properties-client.module#PropertiesClientModule' }
      ]
   },

   {
      path: 'login',
      loadChildren: './session/session.module#SessionModule'
   },
  

   {
      path: '**',
      redirectTo: 'login',
      pathMatch: 'full'
   }

]

@NgModule({
   imports: [RouterModule.forRoot(appRoutes)],
   exports: [RouterModule],
   providers: []
})
export class RoutingModule { }
