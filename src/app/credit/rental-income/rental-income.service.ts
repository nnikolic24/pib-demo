import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreService } from "../../service/core/core.service";
import { environment } from '../../../environments/environment'
import { ToastrService } from "ngx-toastr";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
    providedIn: 'root'
})
export class RentalIncomeService {

    constructor(private http: HttpClient,
        private coreSvc: CoreService,
        private toastr: ToastrService,
        private translate: TranslateService ) { }


    // Create New Rental Income
    createRentalIncome(data) {
        return this.http.post(environment.apiURL + environment.apiSufix + "/rentalIncome", data)
    }
}
