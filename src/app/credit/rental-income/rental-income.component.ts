import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { RentalIncomeService } from './rental-income.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { BlockUIService } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'ms-rental-income',
    templateUrl: './rental-income.component.html',
    styleUrls: ['./rental-income.component.scss']
})
export class RentalIncomeComponent implements OnInit {


    public form: FormGroup;
    public currentDate = new Date();
    dateCurrent = new FormControl(this.currentDate);
    public contactId: any;


    constructor(
        private fb: FormBuilder,
        private rentalIncomeService: RentalIncomeService,
        private route: ActivatedRoute,
        private blockUI: BlockUIService,
        private toastrService: ToastrService,
        private translate: TranslateService,
    ) {

    }


    ngOnInit() {
        //Fetch contact id for generating new data
        this.contactId = this.route.snapshot.params.id;

        //Form creation, setting date and validating it
        this.form = this.fb.group({
            rentalIncome: ['', Validators.required],
            activatedSince: [new Date(), Validators.required],
            id: [null],
        });
    }

    //Allows only number for input rental income field
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }


    submitForm() {
        let preparedObject: any = {};

        preparedObject.rentalincome = this.form.value.rentalIncome;
        if (this.form.value.activatedSince) {
            preparedObject.activatedSince = moment(this.form.value.activatedSince).format("YYYY-MM-DD");
        }
        preparedObject.contact = { id: this.contactId };

        this.createNewRentalIncome(preparedObject);
    }

    //Create new Rental Income
    createNewRentalIncome(preparedObject) {

        this.blockUI.start('main-block');
        this.rentalIncomeService
            .createRentalIncome(preparedObject)
            .subscribe(() => {
                this.blockUI.stop('main-block');
                window.history.back();
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translate.instant("Ne radi update!!!");
                this.toastrService.error(message);
            })
    }

    goBack() {
        // window.history.back();
        window.history.back();
    }
}
