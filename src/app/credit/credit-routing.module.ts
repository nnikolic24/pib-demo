import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalysesComponent } from './analyses/analyses.component';
import { InterestRateSwapsComponent } from './interest-rate-swaps/interest-rate-swaps.component';
import { MortgagesComponent } from './mortgages/mortgages.component';
import { TotalLoansComponent } from './total-loans/total-loans.component';
import { TotalLoanDisplayComponent } from './total-loans/total-loan-display/total-loan-display.component';
import { TotalLoanEditComponent } from './total-loans/total-loan-edit/total-loan-edit.component';
import { NewMortgageComponent } from './mortgages//new-mortgage/new-mortgage.component';
import { MortgageDisplayComponent } from './mortgages/mortgage-display/mortgage-display.component';
import { InterestRateSwapsDisplayComponent } from './interest-rate-swaps/interest-rate-swaps-display/interest-rate-swaps-display.component';
import { InterestRateSwapsEditComponent } from './interest-rate-swaps/interest-rate-swaps-edit/interest-rate-swaps-edit.component';
import { RentalIncomeComponent } from './rental-income/rental-income.component';

const routes: Routes = [{
	path: '',
	children: [
		{
			path: 'analyses',
			component: AnalysesComponent
		},
		{
			path: 'interestrateswaps',
			component: InterestRateSwapsComponent
		},
		{
			path: 'mortgages',
			component: MortgagesComponent
		},
		{
			path: 'mortgage/:id',
			component: MortgageDisplayComponent
		},
		{
			path: 'mortgages/mortgage/new/:id',
			component: NewMortgageComponent
		},
		{
			path: 'mortgages/mortgage/edit/:id',
			component: NewMortgageComponent
		},
		{
			path: 'totalloans',
			component: TotalLoansComponent
		},
		{
			path: 'totalloans/total-loan-display/:id',
			component: TotalLoanDisplayComponent
		},
		{
			path: 'totalloans/total-loan/edit/:id',
			component: TotalLoanEditComponent
		},
		{
			path: 'totalloans/total-loan/new/:id',
			component: TotalLoanEditComponent
		},
		{
			path: 'interest-rate-swaps/interest-rate-swaps-display/:id',
			component: InterestRateSwapsDisplayComponent
		},
		{
			path: 'interest-rate-swaps/interest-rate-swaps-edit/:id',
			component: InterestRateSwapsEditComponent
		},
		{
			path: 'rental-income/:id',
			component: RentalIncomeComponent
		}
	]
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class CreditRoutingModule { }
