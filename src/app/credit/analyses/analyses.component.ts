import { Component, OnInit, ViewChild } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';

//Tab components imports
import { InterestTermComponent } from 'app/shared/interest-term/interest-term.component';
import { DueCreditsComponent } from 'app/shared/due-credits/due-credits.component';
import { OwnershipStructureComponent } from 'app/shared/ownership-structure/ownership-structure.component';
import { LenderStructureComponent } from 'app/shared/lender-structure/lender-structure.component';
import { CreditGrowthComponent } from 'app/shared/credit-growth/credit-growth.component';
import { InterestsComponent } from 'app/shared/interests/interests.component';


@Component({
  selector: 'ms-analyses',
  templateUrl: './analyses.component.html',
  styleUrls: ['./analyses.component.scss']
})
export class AnalysesComponent implements OnInit {

  public customers = [
    {id: 1, name: 'Number one'},
    {id: 2, name: 'Number two'},
    {id: 4, name: 'Number three'},
    {id: 5, name: 'Number four'}
];

  @ViewChild("analysesInterestTerm") analysesInterestTerm: InterestTermComponent;
  @ViewChild("analysesDueCredits") analysesDueCredits: DueCreditsComponent;
  @ViewChild("analysesOwnershipStructure") analysesOwnershipStructure: OwnershipStructureComponent;
  @ViewChild("analysesLenderStructure") analysesLenderStructure: LenderStructureComponent;
  @ViewChild("analysesInterests") analysesInterests: InterestsComponent;
  @ViewChild("analysesCreditGrowth") analysesCreditGrowth: CreditGrowthComponent;

  showInterestTermChart:string = '';

  constructor(private pageTitleService: PageTitleService) { }

  ngOnInit() {
    this.pageTitleService.setTitle("Analyses");
  }


  tabChanged(event) {
    switch (event.index) {
      case 1:
      this.analysesInterestTerm.initChart();
      break;
      case 2:
      this.analysesCreditGrowth.initChart();
      break;
      case 3:
      this.analysesInterests.initChart();
      break;
      case 4:
      this.analysesLenderStructure.initChart();
      break;
      case 5:
      this.analysesOwnershipStructure.initChart();
      break;
      case 6:
      this.analysesDueCredits.initChart();
      break;
    }
  }

}
