import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreditRoutingModule } from './credit-routing.module';
import { TotalLoansComponent } from './total-loans/total-loans.component';
import { MortgagesComponent } from './mortgages/mortgages.component';
import { InterestRateSwapsComponent } from './interest-rate-swaps/interest-rate-swaps.component';
import { AnalysesComponent } from './analyses/analyses.component';
import { TotalLoanDisplayComponent } from './total-loans/total-loan-display/total-loan-display.component';
import { TotalLoanEditComponent } from './total-loans/total-loan-edit/total-loan-edit.component';
import { NewMortgageComponent } from './mortgages/new-mortgage/new-mortgage.component';
import { MortgageDisplayComponent } from './mortgages/mortgage-display/mortgage-display.component';

import {
	MatInputModule,
	MatFormFieldModule,
	MatCardModule,
	MatButtonModule,
	MatIconModule,
	MatDividerModule,
	MatCheckboxModule,
	MatProgressBarModule,
	MatTabsModule,
	MatListModule,
	MatDatepickerModule,
	MatTableModule,
	MatRadioModule,
	MatSelectModule,
	MatChipsModule,
	MatTooltipModule
} from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChartsModule } from 'ng2-charts';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { InterestRateSwapsDisplayComponent } from './interest-rate-swaps/interest-rate-swaps-display/interest-rate-swaps-display.component';
import { InterestRateSwapsDisplayDetailsTabComponent } from './interest-rate-swaps/interest-rate-swaps-display/interest-rate-swaps-display-details-tab/interest-rate-swaps-display-details-tab.component';
import { InterestRateSwapsDisplayReductionsTabComponent } from './interest-rate-swaps/interest-rate-swaps-display/interest-rate-swaps-display-reductions-tab/interest-rate-swaps-display-reductions-tab.component';
import { InterestRateSwapsEditComponent } from './interest-rate-swaps/interest-rate-swaps-edit/interest-rate-swaps-edit.component';
import { MortgageOverviewComponent } from './mortgages/mortgage-display/mortgage-overview/mortgage-overview.component';
import { MortgageCreditDetailsComponent } from './mortgages/mortgage-display/mortgage-credit-details/mortgage-credit-details.component';
import { MortgageAmortizationComponent } from './mortgages/mortgage-display/mortgage-amortization/mortgage-amortization.component';

import { SharedModule } from '../shared/shared.module';
import { RentalIncomeComponent } from './rental-income/rental-income.component';
import { PipesModule } from '../shared/pipes/pipes.module';

@NgModule({
	declarations: [
		TotalLoansComponent,
		MortgagesComponent,
		InterestRateSwapsComponent,
		AnalysesComponent,
		TotalLoanDisplayComponent,
		TotalLoanEditComponent,
		NewMortgageComponent,
		InterestRateSwapsDisplayComponent,
		InterestRateSwapsEditComponent,
		InterestRateSwapsDisplayDetailsTabComponent,
		InterestRateSwapsDisplayReductionsTabComponent,
		MortgageDisplayComponent,
		MortgageOverviewComponent,
		MortgageCreditDetailsComponent,
		MortgageAmortizationComponent,
		RentalIncomeComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		CreditRoutingModule,
		MatCardModule,
		MatFormFieldModule,
		MatInputModule,
		FlexLayoutModule,
		MatButtonModule,
		MatIconModule,
		MatListModule,
		MatDatepickerModule,
		MatCheckboxModule,
		MatTableModule,
		MatTabsModule,
		NgxDatatableModule,
		ChartsModule,
		NgSelectModule,
		TranslateModule,
		MatDividerModule,
		MatProgressBarModule,
		MatRadioModule,
		MatSelectModule,
		SharedModule,
		MatChipsModule,
		MatTooltipModule,
		PipesModule		
	],
	exports: [
		//AnalysesInterestTermComponent
	]
})
export class CreditModule { }
