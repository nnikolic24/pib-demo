import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Object } from 'core-js';
import { CreditService } from '../../service/credit/credit.service';

@Injectable({
    providedIn: 'root'
})
export class CreditTotalLoansService extends CreditService {

    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * Check if provided role is in array of rows
     * @param userRoles roles curent user has
     * @param role to check
     */
    checkRole(userRoles: Array<string>, role: string) {
        let foundCounter: number = 0;
        userRoles.forEach(item => {
            if (item == role) {
                foundCounter++;
            }
        });

        if (foundCounter != 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Map date we got for total loans grid
     * @param result we got from server
     */
    prepareTotalLoansData(result) {
        let data: any = [];
        if (result && result.rows) {
            result.rows.forEach(row => {
                if (row.id) {
                    let id = row.id;
                    let actualLimits = row.actualLimits ? row.actualLimits : "";
                    let creditor = row.creditor.name ? row.creditor.name : "";
                    let customer = row.customer.name ? row.customer.name : "";
                    if (row.customer.surname) {
                        customer = row.customer.name + " " + row.customer.surname
                    }

                    data.push({
                        "id": id,
                        "actualLimits": actualLimits,
                        "creditor": creditor,
                        "customer": customer
                    });
                }
            });
        }
        return data;
    }

    /**
     * Map total loan data
     * @param data we get from server
     */
    mapTotalLoanData(data) {
        let mappedData: any = {};
        if (data) {
            mappedData.id = data.id ? data.id : "";
            mappedData.actualLimits = data.actualLimits ? data.actualLimits : 0;
            mappedData.creditor = data.creditor ? data.creditor : {};
            mappedData.customer = data.customer ? data.customer : {};
            mappedData.creditLimit = data.creditLimit ? data.creditLimit : 0;
            mappedData.creationDate = data.creationDate ? data.creationDate : "";
            mappedData.deactivatedSince = data.deactivatedSince ? data.deactivatedSince : "";
            mappedData.internalComments = data.internalComments ? data.internalComments : "";
            mappedData.limitReduction = data.limitReduction ? data.limitReduction : 0;
            mappedData.migrated = data.migrated ? data.migrated : "";
            mappedData.mortgageVolumeSum = data.mortgageVolumeSum ? data.mortgageVolumeSum : 0;
            mappedData.noticePeriod = data.noticePeriod ? data.noticePeriod : "";
            mappedData.openLimits = data.openLimits ? data.openLimits : 0;
            mappedData.recalledLimits = data.recalledLimits ? data.recalledLimits : "";
            mappedData.remarksOnOpenLimits = data.remarksOnOpenLimits ? data.remarksOnOpenLimits : "-";
            mappedData.retro = data.retro ? data.retro : {};
            mappedData.retroAmount = data.retroAmount ? data.retroAmount : {};
            mappedData.regularLimitReduction = data.regularLimitReduction ? data.regularLimitReduction : {};
        }
        return mappedData;
    }

    /**
     * TODO maybe not needed
     * @param mortgages 
     */
    mapMortgagesData(mortgages) {
        let mappedMortgagesArray = [];
        if (mortgages.rows) {
            mortgages.rows.forEach(item => {
                mappedMortgagesArray.push(Object.assign(item));
            });
        }
        return mappedMortgagesArray;
    }



}





