import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { ISubscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';

import { BlockUIService } from 'ng-block-ui';

import { PageTitleService } from '../../../core/page-title/page-title.service';
import { fadeInAnimation } from '../../../core/route-animation/route.animation';
import { EntitySearchService } from '../../../service/core/entity-search.service';
import { CreditTotalLoansService } from '../total-loans.service';
import { ToastrService } from 'ngx-toastr';
import { MortgageGridHelperService } from '../../../service/core/mortgage-grid-helper.serice';
import * as moment from 'moment';
import { AuthService } from '../../../service/auth-service/auth.service';


@Component({
    selector: 'ms-total-loan-display',
    templateUrl: './total-loan-display.component.html',
    styleUrls: ['./total-loan-display.component.scss'],
    encapsulation: ViewEncapsulation.None,
    host: {
        '[@fadeInAnimation]': 'true'
    },
    animations: [fadeInAnimation]
})
export class TotalLoanDisplayComponent implements OnInit {

    //public routeSubscription: ISubscription;
    public loanId: string; //Total loan ID got as route param
    public totalLoanIds = [];

    public referanceDate: string;

    public allMortgages = [];
    public validMortgages = [];
    public overdueMortgages = [];

    public groupedOverdueMortgages = [];
    public groupedReservedMortgages = [];
    public showNotDeactivatedWarning: boolean = false;

    public totalLoanData: any = {
        id: "",
        actualLimits: 0,
        creditor: {},
        customer: {},
        creditLimit: 0,
        creationDate: "",
        deactivatedSince: "",
        internalComments: "",
        limitReduction: 0,
        migrated: "",
        mortgageVolumeSum: 0,
        noticePeriod: "",
        openLimits: 0,
        recalledLimits: "",
        remarksOnOpenLimits: "-",
        retro: {},
        retroAmount: {},
        regularLimitReduction: {},
    };

    /*---------- Doughnut Chart configuration ----------*/
    public doughnutChartType: string = 'doughnut';
    pieChartColors: any[] = [{
        backgroundColor: ['#661A13', '#EA7B71', '#E53A2A', '#664F4D', '#B22D21']
    }];
    PieChartOptions: any = {
        elements: {
            arc: {
                borderWidth: 0
            }
        },
        legend: {
            position: 'left'
        },
        responsive: true
    }
    public doughnutLegend: boolean = true;
    public showChart: boolean = false;
    public doughnatChartLabels: Array<any> = [];
    public doughnutChartData: number[] = [];

    // Mortgages table configuration
    public rowsForMortgages = [];
    public columnsForMortgages = [
        { name: 'Type', prop: 'type' },
        { name: 'Duration', prop: 'duration' },
        { name: 'Interest rate (%)', prop: 'interestRate' },
        { name: 'Credit volume (CHF)', prop: 'creditVolume' }
    ];
    public enableSummary = true;
    public summaryPosition = 'bottom';

    public isClient: boolean = false;

    constructor(private pageTitleService: PageTitleService,
        private translate: TranslateService,
        private router: Router,
        private location: Location,
        private route: ActivatedRoute,
        private entitySearchService: EntitySearchService,
        private totalLoansService: CreditTotalLoansService,
        private toastrService: ToastrService,
        private authService: AuthService,
        private mortgageGridHelperService: MortgageGridHelperService,
        private blockUI: BlockUIService) { }

    ngOnInit() {
        this.blockUI.start('main-block');
        this.pageTitleService.setTitle("gesamtkredit");

        //Check if the current user is client
        let user = this.authService.getLocalStorageUser();
        let userRoles = user.roles.map(item => item.name);
        this.isClient = this.totalLoansService.checkRole(userRoles, "CLIENT") || this.totalLoansService.checkRole(userRoles, "DEMOKUNDE");
        //TODO!!!! user preferances to show or hide new mortgage or edit total credit button 

        //Set referance day for endpoint param
        this.referanceDate = moment(new Date()).format("YYYY-MM-DD");

        //Gets the ID of the role from the URL
        this.loanId = this.route.snapshot.params.id;

        //Get total loan item for provided id
        this.totalLoansService.fetchLoanData(this.loanId)
            .subscribe((data: any) => {
                console.log("Did we loan got data", data);

                this.totalLoanData = this.totalLoansService.mapTotalLoanData(data);
                this.totalLoanIds.push(data.id);

                this.getValidMortgages();
                this.getGroupedOverdueMortgages();
                this.getGroupedReservedMortgages();
            });

    }

    /**
     * Get mortgages grouped by last runing end
     */
    getGroupedOverdueMortgages() {
        let createEntitySearch: any = {};
        createEntitySearch.groupingPredicate = "BY_LETZTE_LAUFZEIT_ENDE";
        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "hasGesamtKredit", this.totalLoanIds);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "!isGueltig", null);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "isUeberfaellig", "0");
        //Prepare params to be provided for endpoint
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);

        this.totalLoansService.fetchGroupedOverdueMortgages(preparedSearchParams)
            .subscribe((data: any) => {
                if (data.map) {
                    //Push grouping info in every item to be prepared for grid
                    this.groupedOverdueMortgages = this.mortgageGridHelperService.setGroupedByParam(data);
                    this.groupedOverdueMortgages = this.mortgageGridHelperService.mapMortgagesDataForGrid(this.groupedOverdueMortgages);
                    if (this.groupedOverdueMortgages.length)
                        this.showNotDeactivatedWarning = true;
                }
            },
                (error) => {
                    // Show update error message in right corner
                    console.log(error);
                    this.toastrService.error(this.translate.instant("FAILED TO GET DATA"));
                });
    }

    /**
    * Get mortgages grouped by last reserved time end
    */
    getGroupedReservedMortgages() {
        let createEntitySearch: any = {};
        createEntitySearch.groupingPredicate = "BY_LETZTE_LAUFZEIT_ENDE_RESERVIERT";
        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "hasGesamtKredit", this.totalLoanIds);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'isAktiv', null);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'isReserviert', null);
        entitySearch = this.entitySearchService.pushPathParam(entitySearch, 'referenceday', JSON.stringify(this.referanceDate));
        //Prepare params to be provided for endpoint
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);

        this.totalLoansService.fetchGroupedReservedMortgages(preparedSearchParams)
            .subscribe((data: any) => {
                console.log("Reserved mortgages GROUPED ORIGINAL", data);
                if (data.map) {
                    //Push grouping info in every item to be prepared for grid
                    this.groupedReservedMortgages = this.mortgageGridHelperService.setGroupedByParam(data);
                    this.groupedReservedMortgages = this.mortgageGridHelperService.mapMortgagesDataForGrid(this.groupedReservedMortgages);
                    console.log("Reserved mortgages GROUPED", this.groupedReservedMortgages);
                }
            },
                (error) => {
                    // Show update error message in right corner
                    console.log(error);
                    this.toastrService.error(this.translate.instant("FAILED TO GET DATA"));
                });
    }

    /**
     * Get valid mortgages that have total credit 
     */
    getValidMortgages() {
        let createEntitySearch: any = {};
        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'isGueltig', null);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'hasGesamtKredit', [this.loanId]);
        //prepare params to be provided for endpoint
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);

        this.totalLoansService.fetchValidMortgages(preparedSearchParams)
            .subscribe((data: any) => {
                this.validMortgages = this.totalLoansService.mapMortgagesData(data);
                if (this.validMortgages.length) {
                    this.validMortgages.forEach(item => {
                        this.allMortgages.push(item);
                        this.rowsForMortgages = this.mortgageGridHelperService.mapMortgagesDataForGrid(this.allMortgages);
                    });
                }
                //Map mortgage ids to get overdue mortgages
                let mortgageIds = data.rows.map(item => item.mortgageId);
                this.getOverdueMortgages(mortgageIds);
            },
                (error) => {
                    // Show update error message in right corner
                    console.log(error);
                    this.toastrService.error(this.translate.instant("FAILED TO GET DATA"));
                });
    }

    /**
    * Get overdue mortgages that have total credit and are not valid
    */
    getOverdueMortgages(mortgageIds) {
        let createEntitySearch: any = {};
        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'isUeberfaellig', '0');
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'hasGesamtKredit', [this.loanId]);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'idIsNot', mortgageIds);
        //prepare params to be provided for endpoint
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);

        this.totalLoansService.fetchOverdueMortgages(preparedSearchParams)
            .subscribe((data: any) => {
                this.overdueMortgages = this.totalLoansService.mapMortgagesData(data);
                if (this.overdueMortgages.length) {
                    this.overdueMortgages.forEach(item => {
                        this.allMortgages.push(item);
                        this.rowsForMortgages = this.mortgageGridHelperService.mapMortgagesDataForGrid(this.allMortgages);
                    });
                    this.prepareDoughnutChartData(this.allMortgages);
                    this.initChart();
                    this.blockUI.stop('main-block');
                } else {
                    this.prepareDoughnutChartData(this.allMortgages);
                    this.initChart();
                    this.blockUI.stop('main-block');
                }
            },
                (error) => {
                    // Show update error message in right corner
                    console.log(error);
                    this.toastrService.error(this.translate.instant("FAILED TO GET DATA"));
                });
    }

    /**
     * Get chart labels and values
     * TODO translate values for chart ???
     * @param data array of mortgages
     */
    prepareDoughnutChartData(data) {
        data.forEach(item => {
            this.doughnatChartLabels.push(item.productType.replace("LIBOR_HYPOTHEK", "Libor Mortgage").replace("FEST_HYPOTHEK", "Fix Rate Mortgage").replace("ZINSSWAP", "Interest Rate Swapped"));
            this.doughnutChartData.push(item.restVolume);
        })
    }

    /**Flag to render chart */
    initChart() {
        setTimeout(() => {
            this.showChart = true;
        }, 0);
    }

    /**
    * Go to edit provide id of the loean
    * We get it as route param
    */
    editTotalLoan() {
        this.router.navigate(['/credit/totalloans/total-loan/edit/', this.loanId]);
    }

    /**
    * Go back to previous route
    */
    goBack() {
        this.location.back();
    }

    /**
     * TODO
    * Go to the form new mortgage
    */
    navigateToNewMortgage() {
        this.router.navigate(['/credit/mortgages/mortgage/new/', this.loanId]);
    }

    /**
     * Go to mortgage display page
     * @param event on grid row click get id of mortgage
     */
    goToMortgage(event) {
        if (event.type == 'click') {
            this.router.navigate(['/credit/mortgage/', event.row.mortgageId]);
            console.log(event);
        }
    }

    /**Go to customer page */
    goToCustomer() {
        this.router.navigate(['/contacts/', this.totalLoanData.customer.id]);
    }

    /** Go to lender page */
    goToLender() {
        this.router.navigate(['/contacts/lender/lender-edit/', this.totalLoanData.customer.id]);
    }

    //Hide summary for column
    hideSummary() {
        return "";
    }

    //Crediv olume column summary
    summaryRestVolume(cells: number[]) {
        let sumOfCredit: number = 0;
        let formatedSum: string = "";
        cells.forEach(item => sumOfCredit += Number(item));
        formatedSum = "CHF " + sumOfCredit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "'");
        return formatedSum;
    }



}
