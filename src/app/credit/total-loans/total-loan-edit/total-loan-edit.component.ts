import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PageTitleService } from '../../../core/page-title/page-title.service';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { EntitySearchService } from '../../../service/core/entity-search.service';
import { CreditTotalLoansService } from '../total-loans.service';
import { ToastrService } from 'ngx-toastr';
import { MortgageGridHelperService } from '../../../service/core/mortgage-grid-helper.serice';
import { BlockUIService } from 'ng-block-ui';
import * as moment from 'moment';
import { Object } from 'core-js';

@Component({
    selector: 'ms-total-loan-edit',
    templateUrl: './total-loan-edit.component.html',
    styleUrls: ['./total-loan-edit.component.scss']
})
export class TotalLoanEditComponent implements OnInit {

    public loanId: string;
    public clientId: string;
    public loanIds = [];
    public totalLoanIds = [];

    public editMode: boolean = false;
    public newMode: boolean = false;
    public newCustomer: any;

    public referanceDate: string;

    public groupedOverdueMortgages = [];
    public groupedReservedMortgages = [];

    public lenders = [];

    //Main form control group
    public userForm: FormGroup;

    public totalLoanDataObject: any = {};
    public totalLoanData: any = {
        id: "",
        actualLimits: 0,
        creditor: {},
        customer: {},
        creditLimit: 0,
        creationDate: "",
        deactivatedSince: "",
        internalComments: "",
        limitReduction: 0,
        migrated: "",
        mortgageVolumeSum: 0,
        noticePeriod: "",
        openLimits: 0,
        recalledLimits: "",
        remarksOnOpenLimits: "",
        retro: {},
        retroAmount: {},
        regularLimitReduction: {},
    };
    //public totalLoanData: TotalLoanModel;//Total loan main model

    retroRadioLabels: string[] = ['zahlung_retro_kein', 'zahlung_betrag', 'zahlung_prozent'];
    public showRadioSection: boolean = false;
    public showSumInput: boolean = false;
    public showPercentInput: boolean = false;;

    showNoticeInput = false;

    constructor(private formBuilder: FormBuilder,
        private pageTitleService: PageTitleService,
        private translate: TranslateService,
        private router: Router,
        private route: ActivatedRoute,
        private entitySearchService: EntitySearchService,
        private totalLoansService: CreditTotalLoansService,
        private location: Location,
        private toastrService: ToastrService,
        private mortgageGridHelperService: MortgageGridHelperService,
        private blockUI: BlockUIService) {
    }

    // Getter for form controls TODO
    get getFromControl() {
        return this.userForm.controls;
    }

    ngOnInit() {
        this.pageTitleService.setTitle("gesamtkredit");
        //Set referance day for endpoint param
        this.referanceDate = moment(new Date()).format("YYYY-MM-DD");
       
        this.buildForm();
        this.setUserCategoryValidators();

        //Gets the ID of the role from the URL
        let routeIdParam = this.route.snapshot.params.id;
        let routePath = this.route.snapshot.routeConfig.path;
        if (routePath.indexOf("new") == -1) {
            this.editMode = true;
            this.loanId = routeIdParam;
            this.getLenders();
        } else {
            this.newMode = true;
            this.clientId = routeIdParam;
            this.totalLoansService.fetchContact(this.clientId)
                .subscribe(contact => {
                    this.newCustomer = contact;
                    this.getLenders();
                },
                    (error) => {
                        // Show update error message in right corner
                        console.log(error);
                        this.toastrService.error("Failed to get contact!");
                    });
        }
    }

    /**
    * Build form fields
    */
    buildForm() {
        this.userForm = this.formBuilder.group({
            selectLender: [null, Validators.required],
            creditLimit: [null, [Validators.required, Validators.min(0)]],
            remarksOnOpenLimits: [null],
            regularLimitReductionAmount: [null],
            regularLimitReductionInterval: [null],
            regularLimitReductionStart: [null],
            deactivatedSince: [null],
            noticePeriodChecked: [null],
            noticePeriod: [null],
            retroPaymentType: [null],
            retroLumpAmount: [null],
            retroPercentAmount: [null],
            retroAccountingPosition: [null],
            retroBilSubmited: [null],
            internalComments: [null]
        });
    }

    /**
    * Set validation on retroPercentAmount field depending if the field is visible or not throuhg radio buttons
    */
    setUserCategoryValidators() {
        const retroPercentAmountControl = this.userForm.get('retroPercentAmount');
        const retroLumpAmountControl = this.userForm.get('retroLumpAmount');
        this.userForm.get('retroPaymentType').valueChanges
            .subscribe(paymentType => {
                if (paymentType === 'zahlung_prozent') {
                    retroPercentAmountControl.setValidators([Validators.required, Validators.min(0), Validators.max(100)]);
                    retroLumpAmountControl.setValidators(null);
                } else if (paymentType === 'zahlung_betrag') {
                    retroLumpAmountControl.setValidators([Validators.required, Validators.min(0)]);
                    retroPercentAmountControl.setValidators(null);
                } else if (paymentType === 'zahlung_retro_kein') {
                    retroPercentAmountControl.setValidators(null);
                    retroLumpAmountControl.setValidators(null);
                }
                retroPercentAmountControl.updateValueAndValidity();
                retroLumpAmountControl.updateValueAndValidity();
            });
    }

    /**
    * populateForm will receive current object and will fill form fields
    */
    populateForm(data: any) {

        this.totalLoanDataObject = data; //TODO this is the orignial object that needs to be sent on save or update
        console.log('data:', this.totalLoanDataObject);

        if (data.creditor)
            this.getFromControl.selectLender.setValue(data.creditor.id);
        if (data.creditLimit)
            this.getFromControl.creditLimit.setValue(data.creditLimit);
        if (data.remarksOnOpenLimits)
            this.getFromControl.remarksOnOpenLimits.setValue(data.remarksOnOpenLimits);
        if (data.regularLimitReduction) {
            this.getFromControl.regularLimitReductionAmount.setValue(data.regularLimitReduction.amount);
            this.getFromControl.regularLimitReductionInterval.setValue(data.regularLimitReduction.intervalInMonths);
            if (data.regularLimitReduction.start) {
                let regularLimitReductionStartTemp = moment(data.regularLimitReduction.start).toDate();
                this.getFromControl.regularLimitReductionStart.setValue(regularLimitReductionStartTemp);//need to get proper date
            }
        }
        if (data.deactivatedSince) {
            let deactivatedSinceTemp = moment(data.deactivatedSince).toDate();
            this.getFromControl.deactivatedSince.setValue(moment(deactivatedSinceTemp));//need to get proper date
        }
        //Depending on paymnetType display inputs
        if (Object.keys(data.retro).length > 0) {
            this.getFromControl.retroBilSubmited.setValue(data.retro.bilSubmited);
            if (data.retro.paymentType == 'KEIN') {
                this.getFromControl.retroPaymentType.setValue("zahlung_retro_kein");
            } else if (data.retro.paymentType == 'BETRAG') {
                this.getFromControl.retroPaymentType.setValue("zahlung_betrag");
                this.getFromControl.retroLumpAmount.setValue(data.retro.lumpAmount);
                let accountingPositionTemp = moment(data.retro.accountingPosition).toDate();
                this.getFromControl.retroAccountingPosition.setValue(accountingPositionTemp);//need to get proper date
                this.showRadioSection = true;
                this.showSumInput = true;
            } else if (data.retro.paymentType == 'PROZENT') {
                this.getFromControl.retroPaymentType.setValue("zahlung_prozent");
                this.getFromControl.retroPercentAmount.setValue(data.retro.percentAmount);
                let accountingPositionTemp = moment(data.retro.accountingPosition).toDate();
                this.getFromControl.retroAccountingPosition.setValue(accountingPositionTemp);//need to get proper date
                this.showRadioSection = true;
                this.showPercentInput = true;
            }
        } else {
            this.getFromControl.retroPaymentType.setValue("zahlung_retro_kein");
            this.getFromControl.retroBilSubmited.setValue(false);
        }
        //If notice period is empty set checkbox to checked, otherwise show noticeperiod input
        if (!data.noticePeriod) {
            this.getFromControl.noticePeriodChecked.setValue(true);
        } else {
            this.getFromControl.noticePeriodChecked.setValue(false);
            this.showNoticeInput = true;
            this.getFromControl.noticePeriod.setValue(data.noticePeriod);
        }
        if (data.internalComments)
            this.getFromControl.internalComments.setValue(data.internalComments);
    }

    /**
    * Get all lenders for dropdown
    */
    getLenders() {
        let createEntitySearch: any = {};
        createEntitySearch.predicateSource = {
            isKreditgeber: null
        };
        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        //Prepare params to be provided for endpoint
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);
        this.totalLoansService.fetchLenders(preparedSearchParams)
            .subscribe((data: any) => {
                this.lenders = data.rows;
                this.lenders.sort((a, b) => a.name.localeCompare(b.name));
                this.prepareData();
            },
                (error) => {
                    // Show update error message in right corner
                    console.log(error);
                    this.toastrService.error(this.translate.instant("kreditgeber_loading_failed"));
                });
    }

    /**
     * Fech data if we are in edit form
     */
    prepareData() {
        if (this.editMode) {
            this.totalLoansService.fetchLoanData(this.loanId)
                .subscribe((data: any) => {
                    this.totalLoanData = this.totalLoansService.mapTotalLoanData(data);
                    this.populateForm(data);
                    this.totalLoanIds.push(data.id);
                    this.getGroupedOverdueMortgages();
                    this.getGroupedReservedMortgages();
                },
                    (error) => {
                        // Show update error message in right corner
                        console.log(error);
                        this.toastrService.error(this.translate.instant("loading_gesamtkredit"));
                    });
        } else {
            this.totalLoanData.customer = this.newCustomer;
            this.populateForm(this.totalLoanData);
        }
    }

    /**
    * Get mortgages grouped by last runing end
    */
    getGroupedOverdueMortgages() {
        let createEntitySearch: any = {};
        createEntitySearch.groupingPredicate = "BY_LETZTE_LAUFZEIT_ENDE";
        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "hasGesamtKredit", this.totalLoanIds);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "!isGueltig", null);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "isUeberfaellig", "0");
        //Prepare params to be provided for endpoint
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);

        this.totalLoansService.fetchGroupedOverdueMortgages(preparedSearchParams)
            .subscribe((data: any) => {
                if (data.map) {
                    //Push grouping info in every item to be prepared for grid
                    this.groupedOverdueMortgages = this.mortgageGridHelperService.setGroupedByParam(data);
                    this.groupedOverdueMortgages = this.mortgageGridHelperService.mapMortgagesDataForGrid(this.groupedOverdueMortgages);
                }
            },
                (error) => {
                    // Show update error message in right corner
                    console.log(error);
                    this.toastrService.error("FAILED TO GET DATA");
                });
    }

    /**
    * Get mortgages grouped by last reserved time end
    * check date !!!
    */
    getGroupedReservedMortgages() {
        let createEntitySearch: any = {};
        createEntitySearch.groupingPredicate = "BY_LETZTE_LAUFZEIT_ENDE_RESERVIERT";
        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "hasGesamtKredit", this.totalLoanIds);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'isAktiv', null);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'isReserviert', null);
        entitySearch = this.entitySearchService.pushPathParam(entitySearch, 'referenceday', JSON.stringify(this.referanceDate));
        //Prepare params to be provided for endpoint
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);
        this.totalLoansService.fetchGroupedReservedMortgages(preparedSearchParams)
            .subscribe((data: any) => {
                console.log("Reserved mortgages GROUPED ORIGINAL", data);
                if (data.map) {
                    //Push grouping info in every item to be prepared for grid
                    this.groupedReservedMortgages = this.mortgageGridHelperService.setGroupedByParam(data);
                    this.groupedReservedMortgages = this.mortgageGridHelperService.mapMortgagesDataForGrid(this.groupedReservedMortgages);
                    console.log("Reserved mortgages GROUPED", this.groupedReservedMortgages);
                }
            },
                (error) => {
                    // Show update error message in right corner
                    console.log(error);
                    this.toastrService.error("FAILED TO GET DATA");
                });
    }

    /** Go back to previous route */
    goBack() {
        this.location.back();
    }

    /**
    * Show and hide section and change name of input
    * depending wich radio button was clicked
    */
    changeRadioButton(e: any) {
        if (e.value === "zahlung_betrag") {
            this.showRadioSection = true;
            this.showSumInput = true;
            this.showPercentInput = false;
        } else if (e.value === "zahlung_prozent") {
            this.showRadioSection = true;
            this.showSumInput = false;
            this.showPercentInput = true;
        } else {
            this.showRadioSection = false;
        }
    }

    /**
    * Show and hide section if checkbox is clicked
    */
    showNoticePeriod() {
        this.showNoticeInput = !this.showNoticeInput;
    }

    /**
    * Go to customer page, route depends if the user is creditor or not
    * */
    goToCustomer() {
        if (this.totalLoanData.customer.creditor) {
            this.router.navigate(['/contacts/lender/lender-edit/', this.totalLoanData.customer.id]);
        } else {
            this.router.navigate(['/contacts/', this.totalLoanData.customer.id]);
        }
    }


    /**
    * Submit data from form to object that will be sent on save or update
    */
    submitForm() {

        let preparedObject = this.totalLoanDataObject;

        if (this.newMode) {
            delete preparedObject.creationDate;
            delete preparedObject.actualLimits;
            delete preparedObject.id;
            delete preparedObject.limitReduction;
            delete preparedObject.migrated;
            delete preparedObject.openLimits;
            delete preparedObject.mortgageVolumeSum;
            delete preparedObject.migrated;
            delete preparedObject.deactivatedSince;
            preparedObject.regularLimitReduction = {};
            preparedObject.retro.paymentType = "KEIN";
            preparedObject.retro.payment = "EINMALIG";
        }


        let filteredCreditor = this.lenders.filter(item => item.id == this.userForm.value.selectLender)[0];
        preparedObject.creditor = filteredCreditor;
        preparedObject.creditLimit = this.userForm.value.creditLimit ? this.userForm.value.creditLimit : 0;
        preparedObject.remarksOnOpenLimits = this.userForm.value.remarksOnOpenLimits;
        //Set regularLimitReductionPeriod object properties
        if (this.userForm.value.regularLimitReductionAmount)
            preparedObject.regularLimitReduction.amount = this.userForm.value.regularLimitReductionAmount;
        if (this.userForm.value.regularLimitReductionStart) {
            preparedObject.regularLimitReduction.start = moment(this.userForm.value.regularLimitReductionStart).format("YYYY-MM-DD");
        } else {
            preparedObject.regularLimitReduction.start = null;
        }
        if (this.userForm.value.regularLimitReductionInterval)
            preparedObject.regularLimitReduction.intervalInMonths = this.userForm.value.regularLimitReductionInterval;

        if (this.userForm.value.deactivatedSince)
            preparedObject.deactivatedSince = moment(this.userForm.value.deactivatedSince).format("YYYY-MM-DD");
        //Set retro object properties
        if (this.userForm.value.retroPaymentType == "zahlung_retro_kein") {
            preparedObject.retro.paymentType = "KEIN";
            //preparedObject.retroAmount = null; //TODO exist on old client but that's the only place this property is used
            //preparedObject.retroProzent = null; //TODO exist on old client but that's the only place this property is used
        } else if (this.userForm.value.retroPaymentType == "zahlung_betrag") {
            preparedObject.retro.paymentType = "BETRAG";
            //preparedObject.retroAmount = null; //TODO exist on old client but that's the only place this property is used
            preparedObject.retro.lumpAmount = this.userForm.value.retroLumpAmount;
            if (this.userForm.value.retroAccountingPosition) {
                preparedObject.retro.accountingPosition = moment(this.userForm.value.retroAccountingPosition).format("YYYY-MM-DD");
            } else {
                preparedObject.retro.accountingPosition = null;
            }
            preparedObject.retro.bilSubmited = this.userForm.value.retroBilSubmited;
        } else if (this.userForm.value.retroPaymentType == "zahlung_prozent") {
            preparedObject.retro.paymentType = "PROZENT";
            preparedObject.retro.percentAmount = this.userForm.value.retroPercentAmount;
            if (this.userForm.value.retroAccountingPosition) {
                preparedObject.retro.accountingPosition = moment(this.userForm.value.retroAccountingPosition).format("YYYY-MM-DD");
            } else {
                preparedObject.retro.accountingPosition = null;
            }
            preparedObject.retro.bilSubmited = this.userForm.value.retroBilSubmited;
        }
        preparedObject.noticePeriod = this.userForm.value.noticePeriod;
        preparedObject.internalComments = this.userForm.value.internalComments;

        if (this.editMode) {
            console.log("PREPARED OBJECT FOR UPDATE", preparedObject);
            this.updateTotalLoan(preparedObject);
        } else if (this.newMode) {
            console.log("PREPARED NEW OBJECT", preparedObject);
            this.createNewTotalLoan(preparedObject);
        }


    }

    /**
    * Create new total loan
    * @param data prepared json 
    */
    createNewTotalLoan(data) {
        this.totalLoansService.createTotalLoan(data)
            .subscribe((result: any) => {
                console.log("On save result", result);
                this.toastrService.success(this.translate.instant("gesamtkredit_created"));
                this.goToDisplay(result.id);
            },
                (error) => {
                    console.log("There was an error while updating total loan!", error);
                    this.toastrService.error(this.translate.instant("gesamtkredit_creation_failed"));
                });
    }

    /**
    * Update total loan
    * @param data prepared json to be updated
    */
    updateTotalLoan(data) {
        this.totalLoansService.updateTotalLoanData(this.loanId, data)
            .subscribe((result: any) => {
                this.toastrService.success(this.translate.instant("gesamtkredit_saved"));
                this.goToDisplay(this.loanId);
            },
                (error) => {
                    console.log("There was an error while updating total loan!", error);
                    this.toastrService.error(this.translate.instant("gesamtkredit_save_failed"));
                });
    }


    /** Go to display */
    goToDisplay(id) {
        this.router.navigate(['/credit/totalloans/total-loan-display/', id]);
    }




}
