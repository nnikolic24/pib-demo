import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { Router } from "@angular/router";
import { CreditTotalLoansService } from './total-loans.service';
import { TranslateService } from '@ngx-translate/core';
import { EntitySearchService } from '../../service/core/entity-search.service';
import { BlockUIService } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../service/auth-service/auth.service';

@Component({
    selector: 'ms-total-loans',
    templateUrl: './total-loans.component.html',
    styleUrls: ['./total-loans.component.scss']
})
export class TotalLoansComponent implements OnInit {

    public totalLoansArray = [];
    public userId: string;
    public searchQuery: string = "";
    public totalNumberOfElemetns: number = 0;

    constructor(private pageTitleService: PageTitleService,
        private translate: TranslateService,
        private router: Router,
        private authService: AuthService,
        private entitySearchService: EntitySearchService,
        private toastrService: ToastrService,
        private totalLoansService: CreditTotalLoansService,
        private blockUI: BlockUIService) { }

    ngOnInit() {
        this.blockUI.start('main-block');
        this.pageTitleService.setTitle("kunde_gesamtkredite");

        //Get current user and if he is CLIENT or DEMOKUNDE get his id for searhQuery
        let user = this.authService.getLocalStorageUser();
        let userRoles = user.roles.map(item => item.name);
        let isClient = this.totalLoansService.checkRole(userRoles, "CLIENT") || this.totalLoansService.checkRole(userRoles, "DEMOKUNDE");
        //TODO!!!! user access premisin to ad customer id query search
        // if (isClient) {
        //     this.userId = user.id;
        // }

        this.prepareEntitySearch();
    }

    /**
     * On init get grid data from totalloans endpoint
     * If "Search" input has value provide it for search action
     * Result is grid data
     */
    prepareEntitySearch(getPage?) {
        let createEntitySearch: any = {};
        createEntitySearch.paging = { sortColumns: 'creditor.name' };
        createEntitySearch.predicateSource = {
            hasKunde: [this.userId],
            hasKreditgeberName: '%' + this.searchQuery + '%',
            isAktiv: null
        };

        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        //prepare params to be provided for endpoint
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);

        //if we get footer page number set params
        if (getPage) {
            preparedSearchParams.paged.page = getPage.page;
        }

        this.getData(preparedSearchParams);
    }

    getData(preparedSearchParams) {
        this.totalLoansService.fetchTotalLoansOnInit(preparedSearchParams)
            .subscribe((data: any) => {
                console.log("data for grid", data);
                //Prepare needed data from recived json
                this.totalLoansArray = this.totalLoansService.prepareTotalLoansData(data);
                //We get number of total items so we can use it for footer paging
                this.totalNumberOfElemetns = data.total;
                this.blockUI.stop('main-block');
            },
                (error) => {
                    console.log(error);
                    // Show update error message in right corner
                    this.toastrService.error(this.translate.instant("FAILED TO GET DATA"));
                });
    }

    /**
     * Filter data by provided "searchQuery" value
     * For search result set grid result to first page in grid
     */
    filterBy() {
        this.blockUI.start('main-block');
        let firstPage = { page: 1 }
        this.prepareEntitySearch(firstPage);
    }

    /**
     * Function will execute on any row event
     * @param event Received event
     */
    onActivate(event) {
        // If user clicks on row it should redirect him to form with user data 
        if (event.type == 'click') {
            this.router.navigate(['/credit/totalloans/total-loan-display/', event.row.id]);
        }
    }

}
