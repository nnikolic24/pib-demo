//Total loan model
export class TotalLoanModel {

    id: string = "";
    actualLimits: number = 0;
    creditor: any = {};
    customer: any = {};
    creditLimits: number = 0;
    creationDate: string = "";
    deactivatedSince: string = "";
    internalComments: string = "";
    limitReduction: number = 0;
    migrated: string = "";
    mortgageVolumeSum: number = 0;
    noticePeriod: string = "";
    openLimits: number = 0;
    recalledLimits: string = "";
    remarksOnOpenLimits: string = "-";
    retro: any = {};
    retroAmount: any = {};
    regularLimitReductionPeriod: any = {};

}

