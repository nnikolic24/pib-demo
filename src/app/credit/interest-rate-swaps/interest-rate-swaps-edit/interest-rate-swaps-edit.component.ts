import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../../../core/page-title/page-title.service';
import { ActivatedRoute, Router } from '@angular/router';
import { InterestRateSwapsService } from '../interest-rate-swaps.service';
import { BlockUIService } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { EntitySearchService } from '../../../service/core/entity-search.service';
import { CapitalizeFirstPipe } from '../../../shared/pipes/capitalizefirst.pipe';
import { CurrencyPipe } from '@angular/common';
import { ContactNamePipe } from '../../../shared/pipes/contactName.pipe';

@Component({
	selector: 'ms-interest-rate-swaps-edit',
	templateUrl: './interest-rate-swaps-edit.component.html',
	styleUrls: ['./interest-rate-swaps-edit.component.scss'],
	providers: [CapitalizeFirstPipe, CurrencyPipe, ContactNamePipe]
})
export class InterestRateSwapsEditComponent implements OnInit {

	public interestObject: any;
	public interestRateId = '';
	public allCreditors = [];
	public allFinanceObjects = [];
	public allInterestSeries = [];
	public regularReduction = [];
	public mortgages = [];
	public allKundeMortgages = [];
	public reductionEndExclusive = false;
	public reductionStartExclusive = false;

	public isRegularReductionEditable = {};
	public isMortgageEditable = {};



	public interestGroup: FormGroup;
	public regularReductionGroup: FormGroup;
	public uniqueRetroGroup: FormGroup;
	public reccuringRetroGroup: FormGroup;
	public mortgageGroup: FormGroup;

	constructor(
		private pageTitleService: PageTitleService,
		private route: ActivatedRoute,
		private formBuilder: FormBuilder,
		private searchService: EntitySearchService,
		private router: Router,
		private swapService: InterestRateSwapsService,
		private blockUI: BlockUIService,
		private toastr: ToastrService,
		private translate: TranslateService,
		private capFirst: CapitalizeFirstPipe,
		private currencyPipe: CurrencyPipe,
		private contactNamePipe: ContactNamePipe
	) { }

	ngOnInit() {
		this.pageTitleService.setTitle("Interest rate series");
		this.interestRateId = this.route.snapshot.paramMap.get('id');
		this.getInterestRateById(this.interestRateId);
		this.getInterestRateMortgages(this.interestRateId);
		this.getAllMortgagesByKundeId(this.interestRateId);
		this.getInterestRateRegularReduction(this.interestRateId);
		this.initForm();
		this.getAllCreditors();
		this.getAllFinanceObjects();
		this.getAllInterestSeries();
	}

	getAllCreditors() {
		this.swapService.getAllCreditors()
			.subscribe((result: any) => {
				this.allCreditors = result.rows.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
				console.log(this.allCreditors);
			}, error => {
				console.log(error);
			})
	}
	getAllFinanceObjects() {
		this.swapService.getAllFinanceObjects()
			.subscribe((result: any) => {
				this.allFinanceObjects = result.rows;
				this.allFinanceObjects = this.allFinanceObjects.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
			}, error => {
				console.log(error);
			})
	}

	getInterestRateRegularReduction(Id) {
		this.swapService.getInterestRateRegularReduction(Id)
			.subscribe((result: any) => {
				this.regularReduction = result;
				console.log('reduction', this.regularReduction);
			}, error => {
				console.log('error ', error);
			});
	}

	getAllMortgagesByKundeId(Id) {
		let searchData = {
			entity: 'mortgage',
			paging: {},
			predicateSource: {
				isGueltig: null,
				hasSwapbaresVolumen: null,
				hasKunde: [Id]
			}
		};

		//Create entity search with provided params
		let entitySearch = this.searchService.createEntitySearch(searchData);
		//prepare params to be provided for endpoint
		let objectParams = this.searchService.prepareParams(entitySearch);

		this.swapService.getAllMortgagesByKundeId(objectParams)
			.subscribe((result: any) => {
				console.log('all kunde mortgages', result);
				this.allKundeMortgages = result.rows;
				for (let mortgage of this.allKundeMortgages) {
					mortgage.id = mortgage.mortgageId;
					mortgage.displayName = `${mortgage.name}  (${this.formatDateRange(mortgage.todayValidTerm, false)}  mit einem Restvolumen von ${this.currencyPipe.transform(mortgage.restVolume, '', '', '0.0-0', 'de-CH')} bei der ${this.contactNamePipe.transform(mortgage.creditor,[])})`;
					if (mortgage.interestResidualVolume !== 0 && mortgage.swappedVolume) {
						mortgage.displayName += ` bereits geswapptes Volumen: ${this.currencyPipe.transform(mortgage.swappedVolume, '', '', '0.0-0', 'de-CH')}`;
					}
				}
				console.log('all formated names', this.allKundeMortgages);

			}, error => {
				console.log(error);
			});
	}

	getInterestRateMortgages(Id) {
		this.swapService.getInterestRateMortgages(Id)
			.subscribe((result: any) => {
				console.log(result);
				this.mortgages = result;
				console.log(this.mortgages);
			}, error => {
				console.log(error);
				this.toastr.error('', this.translate.instant("mortgage_loading_resource"), {
					timeOut: 3000
				});
			});
	}

	getAllInterestSeries() {
		this.swapService.getAllInterestSeries()
			.subscribe((result: any) => {
				this.allInterestSeries = result.rows;
			}, error => {
				console.log(error);
			})
	}

	getInterestRateById(Id) {
		this.blockUI.start('main-block');
		this.swapService.getInterestRateById(Id)
			.subscribe((result: any) => {
				this.interestObject = result;

				this.swapService.initRetroFieldsIfNotPresent(this.interestObject);
				this.swapService.loadRetroAmount(this.interestObject, this.interestObject.reccuringRetro);
				this.swapService.loadRetroAmount(this.interestObject, this.interestObject.uniqueRetro);

				this.patchValues(this.interestObject);

				console.log('by id', result);
				this.blockUI.stop('main-block');
				this.getAllMortgagesByKundeId(this.interestObject.totalCredit.customer.id);
			}, error => {
				console.log('by id error', error);
				this.blockUI.stop('main-block');
				this.toastr.error('', this.translate.instant("zinsswap_loading_resource"), {
					timeOut: 3000
				});
			});
	}


	goBack() {
		window.history.back();
	}


	initForm() {
		this.interestGroup = this.formBuilder.group({
			'creditor': [null, Validators.required],
			'financeObject': [null],
			'interestSeries': [null, Validators.required],
			'volume': [null, Validators.compose([Validators.required, Validators.min(0)])],
			'interest': [null, Validators.compose([Validators.required, Validators.min(-100), Validators.max(100)])],
			'start': [null, Validators.required],
			'ende': [null, Validators.required],
			'duration': [null],
			'intervalInMonths': [null, Validators.min(1)],
			'deactivatedSince': [null],
			'remarks': [''],
			'internalComments': [''],
			'regularAmortisationsRemarks': ['']
		});
		this.reccuringRetroGroup = this.formBuilder.group({
			'paymentType': ['KEIN'],
			'lumpAmount': [null, Validators.compose([Validators.required, Validators.min(0)])],
			'percentAmount': [null, Validators.compose([Validators.required, Validators.min(-100), Validators.max(100)])],
			'accountingPosition': [null],
			'billIntervalInMonths': [null],
			'billsPostedTo': [null]
		});
		this.uniqueRetroGroup = this.formBuilder.group({
			'paymentType': ['KEIN'],
			'percentAmount': [null, Validators.compose([Validators.required, Validators.min(-100), Validators.max(100)])],
			'lumpAmount': [null, Validators.compose([Validators.required, Validators.min(0)])],
			'bilSubmited': [false],
			'accountingPosition': [null]
		});

		this.regularReductionGroup = this.formBuilder.group({
			'amount': [null],
			'end': [null],
			'endExclusive': [false],
			'intervalMonths': [null],
			'start': [null],
			'startExclusive': [false]
		});

		this.mortgageGroup = this.formBuilder.group({
			'mortgage': [null, Validators.required],
			'amount': [0],
			'swappedFrom': [null]
		});
	}

	patchValues(interestObject) {
		this.reccuringRetroGroup.patchValue({
			paymentType: interestObject.reccuringRetro.paymentType,
			percentAmount: interestObject.reccuringRetro.percentAmount,
			lumpAmount: interestObject.reccuringRetro.lumpAmount,
			accountingPosition: interestObject.reccuringRetro.accountingPosition,
			billIntervalInMonths: interestObject.reccuringRetro.billIntervalInMonths,
			billsPostedTo: interestObject.reccuringRetro.billsPostedTo,
		});

		this.uniqueRetroGroup.patchValue({
			paymentType: interestObject.uniqueRetro.paymentType,
			percentAmount: interestObject.uniqueRetro.percentAmount,
			lumpAmount: interestObject.uniqueRetro.lumpAmount,
			bilSubmited: interestObject.uniqueRetro.bilSubmited,
			accountingPosition: interestObject.uniqueRetro.accountingPosition
		});

		this.interestGroup.patchValue({
			financeObject: interestObject.financeObject,
			creditor: interestObject.totalCredit.creditor,
			interestSeries: interestObject.interestSeries,
			volume: interestObject.volumina[0].volume,
			interest: interestObject.zinsLaufzeiten[0].interest,
			start: interestObject.zinsLaufzeiten[0].start,
			ende: interestObject.zinsLaufzeiten[0].ende,
			duration: interestObject.zinsFaelligkeit.start,
			intervalInMonths: interestObject.zinsFaelligkeit.intervalInMonths,
			deactivatedSince: interestObject.deactivatedSince,
			remarks: interestObject.remarks,
			internalComments: interestObject.internalComments,
			regularAmortisationsRemarks: interestObject.regularAmortisationsRemarks
		});
	}

	addRegularReduction() {
		let tempReductionOb = this.regularReductionGroup.value;
		tempReductionOb.end = this.saveDateFormat(tempReductionOb.end);
		tempReductionOb.start = this.saveDateFormat(tempReductionOb.start);
		tempReductionOb.startExclusive = this.reductionStartExclusive;
		tempReductionOb.endExclusive = this.reductionEndExclusive;
		this.swapService.saveNewRegularReduction(this.interestRateId, this.regularReductionGroup.value)
			.subscribe((result: any) => {
				this.getInterestRateRegularReduction(this.interestRateId);
				this.regularReductionGroup.reset();
				this.isRegularReductionEditable = {};
			}, error => {
				console.log('new reduction error', error)
			});
	}

	updateReductionRow(row) {
		console.log('row to update', row);
		let tempReductionOb = row;
		tempReductionOb.end = this.saveDateFormat(tempReductionOb.end);
		tempReductionOb.start = this.saveDateFormat(tempReductionOb.start);
		
		this.swapService.updateReqularReduction(this.interestRateId, tempReductionOb)
			.subscribe(() => {
				this.isRegularReductionEditable = {};
				this.getInterestRateRegularReduction(this.interestRateId);
			}, error => {
				console.log('error reduction delete', error);
			})
	}

	deleteReduction(rowIndex) {
		this.swapService.deleteRegularReduction(this.interestRateId, this.regularReduction[rowIndex].id)
			.subscribe(() => {
				this.isRegularReductionEditable = {};
				this.getInterestRateRegularReduction(this.interestRateId);
			}, error => {
				console.log('error reduction delete', error);
			})
	}

	addMortgage() {
		let tempMortgage = this.mortgageGroup.value;
		console.log(tempMortgage);
		let mortgageOb = {
			mortgage: { id : tempMortgage.mortgage },
			amount: tempMortgage.amount,
			swappedFrom: this.saveDateFormat(tempMortgage.swappedFrom),
		};
		console.log(mortgageOb);

		this.swapService.newMortgage(this.interestRateId, mortgageOb)
			.subscribe((result: any) => {
				this.getAllMortgagesByKundeId(this.interestRateId);
				this.getInterestRateMortgages(this.interestRateId);
			}, error => {
				console.log(error);
			});
	}

	/**
	 * 
	 * @param group name of the array of objects
	 * @param rowIndex index of row in array
	 */
	createBackupData(group, rowIndex) {
		const tempRow = JSON.parse(JSON.stringify(this[group][rowIndex]));
		this[group][rowIndex].backup = tempRow;
	}

	/**
	 * 
	 * @param group name of the array of objects
	 * @param rowIndex index of row in array
	 */
	removeBackupData(group, rowIndex) {
		const tempRow = JSON.parse(JSON.stringify(this[group][rowIndex].backup));
		this[group][rowIndex] = tempRow;
		this[group] = [...this[group]];
	}

	saveData() {
		this.blockUI.start('main-block');

		let tempUniqueRetro = this.uniqueRetroGroup.value;
		for (let key in tempUniqueRetro) {
			if (key === 'accountingPosition') {
				this.interestObject.uniqueRetro[key] = this.saveDateFormat(tempUniqueRetro[key]);
			} else {
				this.interestObject.uniqueRetro[key] = tempUniqueRetro[key];
			}
		}

		let tempReccuringRetro = this.reccuringRetroGroup.value;
		for (let key in tempReccuringRetro) {
			if (key === 'accountingPosition' || key === 'billsPostedTo') {
				this.interestObject.reccuringRetro[key] = this.saveDateFormat(tempReccuringRetro[key]);
			} else {
				this.interestObject.reccuringRetro[key] = tempReccuringRetro[key];
			}
		}

		let tempInterest = this.interestGroup.value;

		this.interestObject.totalCredit.creditor = tempInterest.creditor;
		this.interestObject.financeObject = tempInterest.financeObject;
		this.interestObject.interestSeries = tempInterest.interestSeries;
		this.interestObject.zinsLaufzeiten[0].ende = this.saveDateFormat(tempInterest.ende);
		this.interestObject.zinsLaufzeiten[0].start = this.saveDateFormat(tempInterest.start);
		this.interestObject.zinsLaufzeiten[0].interest = tempInterest.interest;
		this.interestObject.volumina[0].volume = tempInterest.volume;
		this.interestObject.zinsFaelligkeit.start = this.saveDateFormat(tempInterest.duration);
		this.interestObject.zinsFaelligkeit.intervalInMonths = tempInterest.intervalInMonths;
		this.interestObject.deactivatedSince = this.saveDateFormat(tempInterest.deactivatedSince);
		this.interestObject.internalComments = tempInterest.internalComments;
		this.interestObject.remarks = tempInterest.remarks;
		this.interestObject.regularAmortisationsRemarks = tempInterest.regularAmortisationsRemarks;


		this.swapService.updateInterestRow(this.interestRateId, this.interestObject)
			.subscribe((result: any) => {
				this.router.navigate(['/credit/interest-rate-swaps/interest-rate-swaps-display/', this.interestRateId]);
				this.blockUI.stop('main-block');

			}, error => {
				console.log('error update', error);
			});

	}

	mortgageRowClicked(event) {
		if (event.type == 'click' && event.cellIndex == 0) {
			this.router.navigate(['/credit/mortgage/', event.row.mortgage.id]);
		}
	}

	saveDateFormat(date) {
		if (date) {
			return moment(date).format('YYYY-MM-DD')
		}
		return date;
	}

	formatDate(date) {
		if (date) {
			return moment(date).format("DD.MM.YYYY");
		} else {
			return '';
		}
	}

	formatDateRange(value, showRange) {
		let output = '';
		if (!value || (!value.start && !value.end)) {
			return output;
		}
		if (value.start) {
			let startTag = this.capFirst.transform(this.translate.instant('filter_start'), []);
			output = `${startTag} ${this.createRangeLabel(value.startExclusive, showRange)} ${moment(value.start).format('DD.MM.YYYY')} `;
		}
		if (value.end) {
			let startTag = value.start ? this.translate.instant('filter_to') : this.capFirst.transform(this.translate.instant('filter_to'), []);
			output += `${startTag} ${this.createRangeLabel(value.startExclusive, showRange)} ${moment(value.end).format('DD.MM.YYYY')}`;
		} else if (value.ende) {
			let startTag = value.start ? this.translate.instant('filter_to') : this.capFirst.transform(this.translate.instant('filter_to'), []);
			output += `${startTag} ${this.createRangeLabel(value.startExclusive, showRange)} ${moment(value.ende).format('DD.MM.YYYY')}`;
		}
		return output;
	}
	
	createRangeLabel(value, show) {
		if (show) {
			return value ? this.translate.instant('filter_excl') : this.translate.instant('filter_incl');
		}
		return '';
	}
}