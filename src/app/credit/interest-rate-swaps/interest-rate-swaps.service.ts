import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import * as moment from 'moment';

@Injectable({
	providedIn: 'root'
})
export class InterestRateSwapsService {

	constructor(private http: HttpClient) { }

	getInterestRateSwapsData(params) {
		let paged = encodeURIComponent(JSON.stringify(params.paged));
		let predicate = encodeURIComponent(JSON.stringify(params.predicate));
		return this.http.get(environment.apiURL + environment.apiSufix + `/interestRateSwaps/search?paged=${paged}&predicate=${predicate}`);
	}

	getInterestRateById(Id) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/interestRateSwaps/${Id}`);
	}

	getInterestRateMortgages(Id) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/interestRateSwaps/${Id}/mortgage`);
	}

	getInterestRateRegularReduction(Id) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/${Id}/regularamortization`);
	}

	getInterestRateReductions(Id) {
		return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/${Id}/amortization`);
	}

	getAllMortgagesByKundeId(params) {
		let predicate = encodeURIComponent(JSON.stringify(params.predicate));
		return this.http.get(environment.apiURL + environment.apiSufix + `/mortgage/search?predicate=${predicate}`);
	}

	IsCurrentUserClient() {
		let currentUser = JSON.parse(localStorage.getItem('userProfile'));
		let filteredRoles = currentUser.roles.filter(item => item.name === 'ADMIN' || item.name === 'EMPLOYEE');
		let client = filteredRoles.length > 0 ? false : true;
		return client;
	}

	getAllCreditors() {
		let predicate = encodeURIComponent(JSON.stringify({ "predicate": [{ "name": "isKreditgeber" }] }));
		return this.http.get(environment.apiURL + environment.apiSufix + `/contacts/search?predicate=${predicate}`);
	}

	getAllFinanceObjects() {
		let predicate = encodeURIComponent(JSON.stringify({ "predicate": [{ "name": "isAktiv" }] }));
		return this.http.get(environment.apiURL + environment.apiSufix + `/financingobjects/search?predicate=${predicate}`);
	}

	getAllInterestSeries() {
		return this.http.get(environment.apiURL + environment.apiSufix + `/interestSeries`);
	}

	saveNewRegularReduction(Id, regularObject) {
		return this.http.post(environment.apiURL + environment.apiSufix + `/mortgage/${Id}/regularamortization`, regularObject);
	}

	updateReqularReduction(Id, regularObject) {
		return this.http.put(environment.apiURL + environment.apiSufix + `/mortgage/${Id}/regularamortization/${regularObject.id}`, regularObject);
	}

	deleteRegularReduction(Id, reductionId) {
		return this.http.delete(environment.apiURL + environment.apiSufix + `/mortgage/${Id}/regularamortization/${reductionId}`);
	}

	newMortgage(Id, regularObject) {
		return this.http.post(environment.apiURL + environment.apiSufix + `/interestRateSwaps/${Id}/mortgage`, regularObject);
	}

	updateMortgage() {

	}
	deleteMortgage() {
	
	}

	updateInterestRow(Id, interestObject) {
		return this.http.put(environment.apiURL + environment.apiSufix + `/interestRateSwaps/${Id}`, interestObject);
	}

	loadRetroAmount(interestObject, retro) {
		let validVolume;
		if (!interestObject && !interestObject.volumina) {
			return;
		}
		validVolume = this.loadRetroAmountForMortgage(interestObject);

		if (validVolume && retro && retro.paymentType === 'PROZENT' && retro.percentAmount) {
			retro.feeAmount = {
				amount: validVolume * (retro.percentAmount / 100)
			};
		}
	}

	initRetroFieldsIfNotPresent(interestObject) {
		if (interestObject) {
			if (!interestObject.uniqueRetro) {
				interestObject.uniqueRetro = {
					paymentType: 'KEIN',
					accountingPosition: null,
					payment: 'EINMALIG'
				};
			}

			if (!interestObject.reccuringRetro) {
				interestObject.reccuringRetro = {
					paymentType: 'KEIN',
					accountingPosition: null,
					payment: 'WIEDERKEHREND'
				};
			}
		}
	}

	loadRetroAmountForMortgage(interestObject) {
		let Moment = require('moment');
		let volumina = interestObject.volumina.sort((a, b) => new Moment(a.date).format('YYYYMMDD') - new Moment(b.date).format('YYYYMMDD'));
		let validVolume = 0;
		for (let i = 0; i < volumina.length; i++) {
			if (moment(volumina[i].date).isSameOrBefore()) {
				validVolume = volumina[i].volume;
			}
			break;
		}
		return validVolume;
	}

}
