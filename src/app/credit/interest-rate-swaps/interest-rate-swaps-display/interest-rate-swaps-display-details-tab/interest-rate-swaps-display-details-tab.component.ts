import { Component, OnInit, Input } from '@angular/core';
import { PageTitleService } from '../../../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../../../core/route-animation/route.animation";
import { TranslateService } from '@ngx-translate/core';
import { Router } from "@angular/router";

import * as moment from 'moment';
import { InterestRateSwapsService } from '../../interest-rate-swaps.service';

@Component({
	selector: 'ms-interest-rate-swaps-display-details-tab',
	templateUrl: './interest-rate-swaps-display-details-tab.component.html',
	styleUrls: ['./interest-rate-swaps-display-details-tab.component.scss']
})
export class InterestRateSwapsDisplayDetailsTabComponent implements OnInit {

	constructor(
		private router: Router,
		private swapService: InterestRateSwapsService
	) { }

	@Input('interestObject') interestObject: any;
	@Input('mortgages') mortgages: any;
	@Input('regularReduction') regularReduction: any;

	public isClient = true;

	ngOnInit() {
		this.isClient = this.swapService.IsCurrentUserClient();
	}


	goBack() {
		window.history.back();
	}

	goInterestRateSwapsEdit() {
		this.router.navigate(['/credit/interest-rate-swaps/interest-rate-swaps-edit', this.interestObject.id]);
	}

	formatDate(date) {
		return moment(date).format("DD.MM.YYYY")
	}
	formatDateRange(date1, date2) {
		return `${moment(date1).format('DD.MM.YYYY')} - ${date2 ? moment(date2).format("DD.MM.YYYY") : " "}`
	}

	mortgageRowClicked(event) {
		if (event.type == 'click') {
			this.router.navigate(['/credit/mortgage/', event.row.mortgage.id]);
		}
	}


}
