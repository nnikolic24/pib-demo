import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUIService } from 'ng-block-ui';
import { InterestRateSwapsService } from '../interest-rate-swaps.service';

import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'ms-interest-rate-swaps-display',
	templateUrl: './interest-rate-swaps-display.component.html',
	styleUrls: ['./interest-rate-swaps-display.component.scss']
})
export class InterestRateSwapsDisplayComponent implements OnInit {

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private blockUI: BlockUIService,
		private swapService: InterestRateSwapsService,
		private toastr: ToastrService,
		private translate: TranslateService
	) { }

	public mortgageId = '';

	public interestObject: any;

	public mortgages = [];
	public regularReduction = [];
	public reductions = [];
	public isClient = true;

	ngOnInit() {
		this.mortgageId = this.route.snapshot.paramMap.get('id');
		this.getInterestRateById(this.mortgageId);
		this.getInterestRateMortgages(this.mortgageId);
		this.getInterestRateRegularReduction(this.mortgageId);
		this.getInterestRateReductions(this.mortgageId);

		this.isClient = this.swapService.IsCurrentUserClient();
	}


	getInterestRateById(Id) {
		this.blockUI.start('main-block');
		this.swapService.getInterestRateById(Id)
			.subscribe((result: any) => {
				this.interestObject = result;

				this.swapService.initRetroFieldsIfNotPresent(this.interestObject);
				this.swapService.loadRetroAmount(this.interestObject, this.interestObject.reccuringRetro);
				this.swapService.loadRetroAmount(this.interestObject, this.interestObject.uniqueRetro);

				console.log('by id', result);
				this.blockUI.stop('main-block');

			}, error => {
				console.log('by id error', error);
				this.blockUI.stop('main-block');
				this.toastr.error('', this.translate.instant("zinsswap_loading_resource"), {
					timeOut: 3000
				});
			});
	}

	getInterestRateReductions(Id) {
		this.swapService.getInterestRateReductions(Id)
			.subscribe((result: any) => {
				console.log('getInterestRateReductions', result);
				this.reductions = result;
			}, error => {
				console.log('error', error);
				this.toastr.error('', this.translate.instant("regular_reduction_loading_resource"), {
					timeOut: 3000
				});
			});
	}

	getInterestRateRegularReduction(Id) {
		this.swapService.getInterestRateRegularReduction(Id)
			.subscribe((result: any) => {
				console.log('reduction', result);
				this.regularReduction = result;
			}, error => {
				console.log('error ', error);
			});
	}

	getInterestRateMortgages(Id) {
		this.swapService.getInterestRateMortgages(Id)
			.subscribe((result: any) => {
				console.log(result);
				this.mortgages = result.map(item => {
					let swappedFromDate = item.swappedFrom ? moment(item.swappedFrom).format('DD.MM.YYYY') : '';
					let mortgageDate = item.mortgageTodayCurrentInterestTerm ? moment(item.mortgageTodayCurrentInterestTerm).format('DD.MM.YYYY') : '';
					return {
						mortgage: {
							id: item.mortgage.id,
							displayTitle: item.displayTitle,
							todayRemainingVolume: item.todayRemainingVolume,
							mortgageTodayCurrentInterestTerm: mortgageDate,
							mortgageLenders: item.mortgageLenders
						},
						amount: item.amount,
						swappedFrom: swappedFromDate
					}
				});
				console.log(this.mortgages);
			}, error => {
				console.log(error);
				this.toastr.error('', this.translate.instant("mortgage_loading_resource"), {
					timeOut: 3000
				});
			});
	}

	formatDate(date) {
		return moment(date).format("DD.MM.YYYY")
	}
}
