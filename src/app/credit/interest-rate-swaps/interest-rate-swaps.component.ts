import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../core/route-animation/route.animation";
import { Router } from "@angular/router";
import { BlockUIService } from 'ng-block-ui';
import { EntitySearchService } from '../../service/core/entity-search.service';
import { InterestRateSwapsService } from './interest-rate-swaps.service';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'ms-interest-rate-swaps',
	templateUrl: './interest-rate-swaps.component.html',
	styleUrls: ['./interest-rate-swaps.component.scss']
})
export class InterestRateSwapsComponent implements OnInit {


	constructor(
		private pageTitleService: PageTitleService,
		private router: Router,
		private blockUI: BlockUIService,
		private searchService: EntitySearchService,
		private swapService: InterestRateSwapsService,
		private toastr: ToastrService,
		private translate: TranslateService) {
	}

	public searchQuery = '';
	public rows = [];
	public totalCount: number = 0;

	ngOnInit() {
		this.pageTitleService.setTitle("User");



		this.prepareObjectSearch();
	}

	filterBy() {
		this.prepareObjectSearch(1);
	}

	prepareObjectSearch(page?) {
		this.blockUI.start('main-block');

		let searchData = {
			entity: 'interestRateSwaps',
			paging: {},
			predicateSource: {
				//hasKunde: [this.userId], //TODO Check if user has specific role
				hasKundeName: '%' + this.searchQuery + '%',
				isZinsswap: null,
				isAktiv: null
			}
		};

		//Create entity search with provided params
		let entitySearch = this.searchService.createEntitySearch(searchData);
		//prepare params to be provided for endpoint
		let objectParams = this.searchService.prepareParams(entitySearch);

		//if we get footer page number set params
		if (page) {
			objectParams.paged.page = page;
		}

		this.swapService.getInterestRateSwapsData(objectParams)
			.subscribe((data: any) => {
				this.rows = data.rows.map(item => {
					let start: any = item.periodOfValidity.start;
					let ende: any = item.periodOfValidity.ende;
					let row = {
						Customer: item.customer,
						Lender: item.creditor,
						Duration: `${moment(start).format("DD.MM.YYYY")} - ${ende ? moment(ende).format("DD.MM.YYYY") : " "}`,
						Volume: item.validVolumeToday.volume,
						MortgageId: item.mortgageId
					}
					return row;
				});
				console.log(this.rows);
				this.totalCount = data.total;
				this.blockUI.stop('main-block');
			}, error => {
				this.blockUI.stop('main-block');
				this.toastr.error('', this.translate.instant("error_loading_zinsswaps"), {
					timeOut: 3000
				});
			});

	}

	changePage(event) {
		console.log(event);
		this.prepareObjectSearch(event.page);
	}


	//event which will be used later if it's needed to call endpoint when search is triggered
	sorted(event) {

	}


	/**
	 * Function will execute on any row event
	 * @param event Received event
	 */
	onActivate(event) {
		// If user clicks on row it should redirect him to form with user data 
		if (event.type == 'click') {
			this.router.navigate(['/credit/interest-rate-swaps/interest-rate-swaps-display/', event.row.MortgageId]);
		}
	}



}