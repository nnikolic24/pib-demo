import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../core/route-animation/route.animation";
import { TranslateService } from '@ngx-translate/core';
import { MortgagesService } from './mortgages.service';
import { CoreService } from '../../service/core/core.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { EntitySearchService } from '../../service/core/entity-search.service';
import { ToastrService } from 'ngx-toastr';
import { BlockUIService } from 'ng-block-ui';

import { MortgageGridHelperService } from './../../service/core/mortgage-grid-helper.serice';

import * as lodash from 'lodash';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ms-mortgages',
  templateUrl: './mortgages.component.html',
  styleUrls: ['./mortgages.component.scss']
})
export class MortgagesComponent implements OnInit {


  constructor(private pageTitleService: PageTitleService,
    private translate: TranslateService,
    private mortgageService: MortgagesService,
    private coreSvc: CoreService,
    private fb: FormBuilder,
    private searchService: EntitySearchService,
    private mortGridHelper: MortgageGridHelperService,
    private router: Router,
    private toastr: ToastrService,
    private blockUI: BlockUIService) { }



  renderedData = [];

  groupedByCustomer: any = {
    data: {},
    productTypeQuery: [],
    clientAdvisorQuery: undefined,
    lenderQuery: undefined
  };

  groupedByLender: any = {
    clientAdvisorQuery: undefined,
    lenderQuery: undefined
  };

  queryObject: any = this.groupedByCustomer;

  mortgageSearch: any = {};
  totalLoanSearch: any = {};

  mortgageSearchParams: any;
  totalLoanSearchParams: any;

  lenders = [];
  adviser = [];

  productType = [
    { id: 'VARIABLE_HYPOTHEK', value: 'VARIABLE_HYPOTHEK' },
    { id: 'LIBOR_HYPOTHEK', value: 'LIBOR_HYPOTHEK' },
    { id: 'FEST_HYPOTHEK', value: 'FEST_HYPOTHEK' },
    { id: 'ZINSSWAP', value: 'ZINSSWAP' }
  ];

  enableSummary = true;
  summaryPosition = 'bottom';
  tab: string;

  ngOnInit() {

    this.tab = 'Customers';
    console.log('OnInit tab ', this.tab);

    this.getAllLenders();
    this.getUserByRole();
    
    this.createMortSearch();
    this.createTLSearch();

    // set page title 
    this.pageTitleService.setTitle("Credit lists");


  }

  getAllLenders() {

    let createEntitySearch: any = {};
    createEntitySearch.entity = "contacts";

    let entitySearch = this.searchService.createEntitySearch(createEntitySearch);
    entitySearch = this.searchService.pushPredicate(entitySearch, "!isDeleted", null);
    entitySearch = this.searchService.pushPredicate(entitySearch, "isKreditgeber", null);

    let preparedSearchParams = this.searchService.prepareParams(entitySearch);

    this.mortgageService.fetchLenders(preparedSearchParams)
      .subscribe((data: any) => {
        this.lenders = data.rows;
      }, error => {
        console.log(error);
      });
  }

  getUserByRole() {
    this.mortgageService.retrieveUserByRole('EMPLOYEE')
      .subscribe((data: any) => {
        this.adviser = data;
      }, error => {
        console.log(error);
      });
  }

  createMortSearch() {

    let mortgageSearch: any = {}
    mortgageSearch.entity = 'mortgage';
    mortgageSearch.groupingPredicate = 'BY_LAUFZEIT_ENDE';

    this.mortgageSearch = this.searchService.createEntitySearch(mortgageSearch);
  }

  createTLSearch() {
    let totalLoanSearch: any = {};
    totalLoanSearch.entity = 'totalloans';

    this.totalLoanSearch = this.searchService.createEntitySearch(totalLoanSearch);
  }

  renderData(response) {
    this.groupedByCustomer.data = {
      dataAvailable: lodash.keys(response.map).length > 0,
      loans: response.map,
      groupingKeys: lodash.keys(response.map)
    };
    console.log(this.groupedByCustomer.data);
  }

  tabChanged(event) {
    this.tab = event.tab.textLabel;
    console.log("Tab selected ", this.tab);

    if (this.tab === 'Customers') {
      this.groupedByCustomer.data = {};
      this.queryObject = this.groupedByCustomer;
      this.searchService.setGroupingPredicate(this.mortgageSearch, 'BY_LAUFZEIT_ENDE');
    } else {
      this.queryObject = this.groupedByLender;
      this.searchService.setGroupingPredicate(this.totalLoanSearch, 'BY_KUNDE');
    }

    this.queryObject.clientAdvisorQuery = undefined;
    this.queryObject.lenderQuery = undefined;
  }

  preparePredicatesForMortgageSearch() {

    let productTypes = [];

    this.searchService.resetPredicates(this.mortgageSearch);

    if (this.tab === 'Customers') {
      if (this.queryObject.startTermQuery || this.queryObject.startTermToQuery) {
        this.searchService.pushPredicate(this.mortgageSearch, 'startZinsLaufzeitIn', this.mortgageService.range(this.queryObject.startTermQuery, this.queryObject.startTermToQuery).closed);
      }
      if (this.queryObject.endTermQuery || this.queryObject.endTermToQuery) {
        this.searchService.pushPredicate(this.mortgageSearch, 'endeZinsLaufzeitIn', this.mortgageService.range(this.queryObject.endTermQuery, this.queryObject.endTermToQuery).closed);
      }
      if (this.queryObject.startFrameQuery || this.queryObject.startFrameToQuery) {
        this.searchService.pushPredicate(this.mortgageSearch, 'startRahmenLaufzeitIn', this.mortgageService.range(this.queryObject.startFrameQuery, this.queryObject.startFrameToQuery).closed);
      }
      if (this.queryObject.endFrameQuery || this.queryObject.endFrameToQuery) {
        this.searchService.pushPredicate(this.mortgageSearch, 'endeRahmenLaufzeitIn', this.mortgageService.range(this.queryObject.endFrameQuery, this.queryObject.endFrameToQuery).closed);
      }

      if (this.queryObject.productTypeQuery) {
        this.queryObject.productTypeQuery.forEach(product => {
          productTypes.push(product.id);
        });
        this.searchService.pushPredicate(this.mortgageSearch, 'hasProduktTyp', productTypes);
      }
    }

    this.searchService.pushPredicate(this.mortgageSearch, 'isAktiv', null);

    if (this.tab === 'Lender') {
      if (this.queryObject.deadlineFromQuery || this.queryObject.deadlineToQuery) {
        this.searchService.pushPredicate(this.mortgageSearch, 'startErsteZinsLaufzeitIn', this.mortgageService.range(this.queryObject.deadlineFromQuery, this.queryObject.deadlineToQuery).closed);
      }
      this.searchService.pushPredicate(this.mortgageSearch, 'isGueltig', null);
    }

    if (this.queryObject.clientAdvisorQuery && this.queryObject.clientAdvisorQuery.id) {
      this.searchService.pushPredicate(this.mortgageSearch, 'hasKundenberater', [this.queryObject.clientAdvisorQuery.id]);
    }

    if (this.queryObject.lenderQuery && this.queryObject.lenderQuery.id) {
      this.searchService.pushPredicate(this.mortgageSearch, 'hasKreditgeber', [this.queryObject.lenderQuery.id]);
    }

    return this.mortgageSearchParams = this.searchService.prepareParams(this.mortgageSearch);
  }

  preparePredicatesForTotalloanSearch() {

    this.searchService.resetPredicates(this.totalLoanSearch);

    this.searchService.pushPredicate(this.totalLoanSearch, 'isGueltig', null);
    this.searchService.pushPredicate(this.totalLoanSearch, 'hasRetro', null);

    if (this.queryObject.clientAdvisorQuery && this.queryObject.clientAdvisorQuery.id) {
      this.searchService.pushPredicate(this.totalLoanSearch, 'hasKundenberater', [this.queryObject.clientAdvisorQuery.id]);
    }

    if (this.queryObject.lenderQuery && this.queryObject.lenderQuery.id) {
      this.searchService.pushPredicate(this.totalLoanSearch, 'hasKreditgeber', [this.queryObject.lenderQuery.id]);
    }
    return this.totalLoanSearchParams = this.searchService.prepareParams(this.totalLoanSearch);
  }

  search() {
    this.blockUI.start('main-block');
    this.preparePredicatesForMortgageSearch();
    this.preparePredicatesForTotalloanSearch();

    this.mortgageService.retrieveGroupedMortgages(this.mortgageSearchParams)
      .subscribe((data: any) => {
        this.renderedData = this.mortGridHelper.setGroupedByParam(data);
        this.renderedData = this.mortGridHelper.mapMortgagesDataForGrid(this.renderedData);
        this.renderedData = this.mortgageService.prepareDataForGrid(this.renderedData);
        this.blockUI.stop('main-block');
      }, error => {
        this.blockUI.stop('main-block');
        this.toastr.error("There was an error!");
        console.log(error);
      });
  };

  resetForm() {
    this.groupedByCustomer.clientAdvisorQuery = undefined;
    this.groupedByCustomer.lenderQuery = undefined;
    this.groupedByCustomer.productTypeQuery = [];
    this.groupedByCustomer.startTermQuery = undefined;
    this.groupedByCustomer.startTermToQuery = undefined;
    this.groupedByCustomer.endTermQuery = undefined;
    this.groupedByCustomer.endTermToQuery = undefined;
    this.groupedByCustomer.startFrameQuery = undefined;
    this.groupedByCustomer.startFrameToQuery = undefined;
    this.groupedByCustomer.endFrameQuery = undefined;
    this.groupedByCustomer.endFrameToQuery = undefined;

    this.renderedData = [];
  };

  redirect(e) {
    if (e.type == 'click') {
      if (e.cellIndex == 0) {
        this.router.navigate(['/contacts/', e.row.customerID]);
      } else if (e.cellIndex == 1) {
        if (e.row.productType == "ZINSSWAP") {
          this.router.navigate(['/credit/interest-rate-swaps/interest-rate-swaps-display/', e.row.mortgageId]);
        } else {
          this.router.navigate(['/credit/mortgage/', e.row.mortgageId]);
        }
      }
    }
  }

  //Credi volume column summary
  summaryRestVolume(cells: number[]) {
    let sumOfCredit: number = 0;
    let formatedSum: string = "";
    cells.forEach(item => sumOfCredit += Number(item));
    formatedSum = Math.round(sumOfCredit).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "'");
    return formatedSum;
  }

  setLenderExcelDownloadUrl() {

    let predicates, predicatesTotalLoan;

    this.preparePredicatesForMortgageSearch();
    this.preparePredicatesForTotalloanSearch();

    predicates = this.searchService.generateSearchPayload(this.mortgageSearch);
    predicatesTotalLoan = this.searchService.generateSearchPayload(this.totalLoanSearch);

    let predicate = this.searchService.prepareParams(predicates);
    let totalLoanpredicate = this.searchService.prepareParams(predicatesTotalLoan);

    window.open(this.mortgageService.getExcel(predicate, totalLoanpredicate), "_blank");
    
  };

}
