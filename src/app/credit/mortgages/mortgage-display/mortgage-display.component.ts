import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MortgagesService } from '../mortgages.service';
import { AuthService } from '../../../service/auth-service/auth.service';

@Component({
	selector: 'mortgage-display',
	templateUrl: './mortgage-display.component.html',
	styleUrls: ['./mortgage-display.component.scss']
})
export class MortgageDisplayComponent implements OnInit {

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private mortgageService: MortgagesService,
		private authService: AuthService

	) { }

	public mortgage: any;
	public mortgageId;
	public amortizations = []; 
	public regularAmortizations = []; 
	public mortgageVolume = []; 
	public mortgageInterestMaturities= []; 
	public isClient = true;

	ngOnInit() {
		this.mortgageId = this.route.snapshot.paramMap.get('id');
		//Check if the current user is client
		let user = this.authService.getLocalStorageUser();
		let userRoles = user.roles.map(item => item.name);
		this.isClient = this.mortgageService.checkRole(userRoles, "CLIENT") || this.mortgageService.checkRole(userRoles, "DEMOKUNDE");
		this.getMortgageById();
		this.getMortgageVolume();
		this.getMortgageInterestMaturities();
		this.getAllRegularAmortizations();
	}

	getMortgageById() {
		this.mortgageService.getMortgageById(this.mortgageId)
			.subscribe((result: any) => {
				console.log('mortgage by id', result);
				this.mortgage = result;
			}, error => {
				console.log('error get mortgage', error);
			});
	}

	getAllAmortizations() {
		this.mortgageService.getAmortizationsByMortgageId(this.mortgageId)
			.subscribe((result: any) => {
				console.log('all amortizations', result);
				this.amortizations = result;
			}, error => {
				console.log('error all amortizations', error);
			})
	}

	getAllRegularAmortizations() {
		this.mortgageService.getRegularAmortizationsByMortgageId(this.mortgageId)
			.subscribe((result: any) => {
				console.log('all regularAmortizations', result);
				this.regularAmortizations = result;
			}, error => {
				console.log('error all regularAmortizations', error);
			})
	}
	getMortgageVolume() {
		this.mortgageService.getMortgageVolume(this.mortgageId)
			.subscribe((result: any) => {
				this.mortgageVolume = result;
				console.log('this.mortgageVolume ', this.mortgageVolume);
			}, error => {
				console.log('error from volume',error)
			})
	}
	getMortgageInterestMaturities() {
		this.mortgageService.getMortgageInterestMaturities(this.mortgageId)
			.subscribe((result: any) => {
				this.mortgageInterestMaturities = result;
				console.log('this.mortgageInterestMaturities ', this.mortgageInterestMaturities);
			}, error => {
				console.log('error from mortgageInterestMaturities',error)
			})
	}


	onTabChanged(event) {
		if (event.index === 2) {
			this.getAllAmortizations();
		}
	}

}
