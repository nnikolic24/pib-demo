import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MortgagesService } from '../../mortgages.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'mortgage-amortization',
	templateUrl: './mortgage-amortization.component.html',
	styleUrls: ['./mortgage-amortization.component.scss']
})
export class MortgageAmortizationComponent implements OnInit {

	isEditable = {};
	errorMsg = {
		date: 'Bitte geben Sie f�r die Amortisation ein g�ltiges Datum ein',
		amount: 'Bitte geben Sie f�r die Amortisation einen g�ltigen Betrag ein'
	}
	constructor(
		private mortgageService: MortgagesService,
		private formBuilder: FormBuilder,
		private toastr: ToastrService
	) { }


	@Input('amortizations') amortizations: [];
	@Input('isClient') isClient: boolean;
	@Input('mortgageId') mortgageId: string;
	@Output('amortizationReload') amortizationReload: EventEmitter<any> = new EventEmitter();

	public amortizationGroup: FormGroup;
	

	ngOnInit() {
		this.initForm();
	}

	initForm() {
		this.amortizationGroup = this.formBuilder.group({
			'amount': [null, [Validators.required, Validators.min(0)]],
			'date': [null, Validators.required],
			'remarks': [null]
		});
	}
	addAmortization() {
		if (!this.amortizationGroup.valid) {
			let msg = '';
			if (!this.amortizationGroup.controls['date'].valid) {
				msg = this.errorMsg.date;
			} else {
				msg = this.errorMsg.amount;
			}
			this.toastr.error('', msg, {
				timeOut: 3000
			});
		} else {
			let tempAmortization = this.amortizationGroup.value;
			tempAmortization.date = this.formatDateBeforeSave(tempAmortization.date);
			this.mortgageService.addAmortizationToMortgage(this.mortgageId, tempAmortization)
				.subscribe((result: any) => {
					this.amortizationGroup.reset();
					this.amortizationReload.emit();
					this.isEditable = {};
				}, error => {
					console.log('error save amortization', error);
				});
		}
	}

	deleteAmortization(row) {
		this.mortgageService.deleteAmortizationFromMortgage(this.mortgageId, row.id)
			.subscribe(() => {
				this.isEditable = {};
				this.amortizationReload.emit();
			}, error => {
				console.log('error delete amortization', error);
			});
	}

	updateAmortization(row) {
		let tempAmortization = row;
		tempAmortization.date = this.formatDateBeforeSave(tempAmortization.date);
		if (!tempAmortization.date || tempAmortization.amount < 0) {
			let msg = '';
			if (!tempAmortization.date) {
				msg = this.errorMsg.date;
			} else if (tempAmortization.amount < 0) {
				msg = this.errorMsg.amount;
			}
			this.toastr.error('', msg, {
				timeOut: 3000
			});
		} else {
			this.mortgageService.updateAmortizationFromMortgage(this.mortgageId, tempAmortization)
				.subscribe(() => {
					this.isEditable = {};
					this.amortizationReload.emit();
				}, error => {
					console.log('error update amortization', error);
				});
		}


	}


	/**
	 * 
	 * @param group name of the array of objects
	 * @param rowIndex index of row in array
	 */
	createBackup(group, rowIndex) {
		const tempRow = JSON.parse(JSON.stringify(this[group][rowIndex]));
		this[group][rowIndex].backup = tempRow;
	}

	/**
	 * 
	 * @param group name of the array of objects
	 * @param rowIndex index of row in array
	 */
	deleteBackup(group, rowIndex) {
		const tempRow = JSON.parse(JSON.stringify(this[group][rowIndex].backup));
		this[group][rowIndex] = tempRow;
		this[group] = [...this[group]];
	}

	formatDateBeforeSave(date) {
		if (date) {
			return moment(date).format('YYYY-MM-DD');
		}

		return date;
	}
}
