import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ContactNamePipe } from '../../../../shared/pipes/contactName.pipe';
import { CurrencyPipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { CapitalizeFirstPipe } from '../../../../shared/pipes/capitalizefirst.pipe';
import * as moment from 'moment';

@Component({
	selector: 'mortgage-credit-details',
	templateUrl: './mortgage-credit-details.component.html',
	styleUrls: ['./mortgage-credit-details.component.scss'],
	providers: [CurrencyPipe, ContactNamePipe, CapitalizeFirstPipe]
})
export class MortgageCreditDetailsComponent {
	
	@Input('mortgage') mortgage: any;
	@Input('isClient') isClient: any;
	@Input('regularAmortizations') regularAmortizations: any;
	@Input('mortgageVolume') mortgageVolume: any;
	@Input('mortgageInterestMaturities') mortgageInterestMaturities: any;
	public allProductTypes = ['VARIABLE_HYPOTHEK', 'LIBOR_HYPOTHEK', 'FEST_HYPOTHEK'];

	constructor(
		private currencyPipe: CurrencyPipe,
		private contactNamePipe: ContactNamePipe,
		private translate: TranslateService,
		private capFirst: CapitalizeFirstPipe
	) { }

	getTotalCredit(credit) {
		if (credit) {
			return `${this.currencyPipe.transform(credit.creditLimit, '', '', '0.0-0', 'de-CH')} ${this.translate.instant('filter_at')} ${this.contactNamePipe.transform(credit.creditor, [])}`;
		}
		return '';
	}

	formatDateRange(value) {
		let end = value.end ? value.end : value.ende ? value.ende : null;
		if (!value || (!value.start && !end )) {
			return '-';
		}
		if (value.start && !end) {
			return `${this.capFirst.transform(this.translate.instant('filter_from'), [])} ${moment(value.start).format('DD.MM.YYYY')}`;
		}
		if (end && !value.start) {
			return `${this.capFirst.transform(this.translate.instant('hypo_list_term_to'), [])} ${moment(end).format('DD.MM.YYYY')}`;
		} 

		return `${this.capFirst.transform(this.translate.instant('filter_from'), [])} ${moment(value.start).format('DD.MM.YYYY')} 
				${this.translate.instant('hypo_list_term_to')} ${moment(end).format('DD.MM.YYYY')}`		
	}

}


