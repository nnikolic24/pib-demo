import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mortgage-overview',
  templateUrl: './mortgage-overview.component.html',
  styleUrls: ['./mortgage-overview.component.scss']
})
export class MortgageOverviewComponent implements OnInit {

  public showChart: boolean = false;

  /**TODO line chart options */
  public lineChartLabels: Array<any> = ['05.2019', '06.2019', '07.2019', '08.2019'];
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Volume amaunt (CHF)'
        }
      }]
    }
  };
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';

  public lineChartSteppedData: Array<any> = [{
    data: [65, 59, 80, 81, 56, 55, 40],
    label: 'Series A',
    borderWidth: 1,
    fill: false,
    steppedLine: true
  }, {
    data: [28, 48, 40, 19, 86, 27, 90],
    label: 'Series B',
    borderWidth: 1,
    fill: false,
    steppedLine: true
  }, {
    data: [18, 48, 77, 9, 100, 27, 40],
    label: 'Series C',
    borderWidth: 1,
    fill: false,
    steppedLine: true
  }];

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.showChart = true;
    }, 0);
  }

}
