import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageTitleService } from './../../../core/page-title/page-title.service';
import { TranslateService } from '@ngx-translate/core';
import { Location, CurrencyPipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { MortgagesService } from '../mortgages.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EntitySearchService } from '../../../service/core/entity-search.service';
import { FromEventTarget } from 'rxjs/internal/observable/fromEvent';

@Component({
    selector: 'ms-new-mortgage',
    templateUrl: './new-mortgage.component.html',
    styleUrls: ['./new-mortgage.component.scss'],
	providers: [CurrencyPipe]
})
export class NewMortgageComponent implements OnInit {

    public interestCalculationRadioLabels: string[] = ['zins_usanz_ch', 'zins_usanz_international'];
    public interestTypeRadioLabels: string[] = ['hypo_details_dev', 'hypo_details_total'];
    public retroRadioLabels: string[] = ['zahlung_retro_kein', 'zahlung_betrag', 'zahlung_prozent'];

    public showRadioSectionUnique: boolean;
    public uniqueSumShow: boolean = false;
    public uniquePercentShow: boolean = false;

    public showRadioSectionRecurring: boolean;
    public recurringSumShow: boolean = false;
    public recurringPercentShow: boolean = false;

    public newMode: boolean = false;
    public editMode: boolean = false;

    public totalLoanId: string;
    public mortgageId: string;

    public allTotalLoans = [];
    public allFinancingObjects = [];

    public customer: any;
    public creditor: any;


    public productType = [
        { name: 'Fix Rate Mortage', value: "FEST_HYPOTHEK" },
        { name: 'Libot Mortage', value: "LIBOR_HYPOTHEK" },
        { name: 'Variable Mortage', value: "VARIABLE_HYPOTHEK" }
    ];

    public termination = [
        { name: 'Inventory', value: "BESTANDS_AUFNAHME" },
        { name: 'Neuhypothek', value: "NEU_HYPOTHEK" },
        { name: 'Conversion', value: "UMWANDLUNG" },
        { name: 'Renewal', value: "VERLAENGERUNG" }
    ];

    public mortgageForm: FormGroup;
    public volumeForm: FormGroup;
    public interestRateForm: FormGroup; //TODO: zinsLaufzeiten
    public interestRateStartExclusive = false;
    public interestRateEndExclusive = true;
    public frameTermForm: FormGroup;
    public frameTermStartExclusive = false;
    public frameTermEndExclusive = true;
    public interestDueForm: FormGroup; //TODO: zinsFaelligkeit
    public regularAmortizationForm: FormGroup; //TODO: regulaereAmortisationLaufzeiten
    public regularAmortizationStartExclusive = false;
    public regularAmortizationEndExclusive = true;
    public uniqueRetroForm: FormGroup;
    public reccuringRetro: FormGroup;

    constructor(private fb: FormBuilder,
        private pageTitleService: PageTitleService,
        private translate: TranslateService,
        private toastrService: ToastrService,
        private route: ActivatedRoute,
        private router: Router,
        private entitySearchService: EntitySearchService,
        private mortgagesService: MortgagesService,
        private currencyPipe: CurrencyPipe,
        private location: Location) {
    }

    ngOnInit() {
        this.pageTitleService.setTitle("hypothek_new");
        this.createForms();

        //Gets the ID of the role from the URL
        let routeIdParam = this.route.snapshot.params.id;
        let routePath = this.route.snapshot.routeConfig.path;
        if (routePath.indexOf("new") == -1) {
            this.editMode = true;
            this.mortgageId = routeIdParam;
            this.getTotalLoan();
            console.log("this.mortgageId", this.mortgageId);
        } else {
            this.newMode = true;
            this.totalLoanId = routeIdParam;
            this.getTotalLoan();
            console.log("this.totalLoanId", this.totalLoanId);
        }
    }

    createForms() {
        this.mortgageForm = this.fb.group({
            financingObjectId: [null],
            totalLoanId: [null],
            productType: [null],
            termination: ["NEU_HYPOTHEK"],
            accountNumber: [null],
            noticePeriod: [null],
            interestRateType: [null],
            interestChargeType: [null],
            marge: [null], //TODO: margin
            deactivatedSince: [null],
            regularAmortisationsRemarks: [null],
            remarks: [null],
            internalComments: [null]
        });
        this.volumeForm = this.fb.group({
            date: [new Date()],
            volume: [null],
            loanAmountPaid: [null],
            interestOnTheLoan: [null]
        });
        this.interestRateForm = this.fb.group({
            start: [new Date()],
            startExclusive: [false],
            ende: [null],
            endExclusive: [false],
            interest: [null],
            remark: [null]
        });
        this.frameTermForm = this.fb.group({
            start: [null],
            startExclusive: [null],
            ende: [null],
            endExclusive: [null]
        });
        this.interestDueForm = this.fb.group({
            start: [null],
            intervalInMonths: [null]
        });
        this.regularAmortizationForm = this.fb.group({
            start: [null],
            startExclusive: [null],
            ende: [null],
            endExclusive: [null],
            amount: [null],
            intervalInMonths: [null]
        });
        this.uniqueRetroForm = this.fb.group({
            lumpAmount: [null],
            percentAmount: [null],
            accountingPosition: [null],
            bilSubmited: [null]
        });
        this.reccuringRetro = this.fb.group({
            lumpAmount: [null],
            percentAmount: [null],
            accountingPosition: [null],
            billIntervalInMonths: [null],
            billsPostedTo: [null]
        });
    }

    getTotalLoan() {
        //Get total loan item for provided id
        this.mortgagesService.fetchLoanData(this.totalLoanId)
            .subscribe((data: any) => {
                this.customer = data.customer;
                this.creditor = data.creditor;

                this.initializeTotalCredit(this.customer.id);
                this.financeObjectSearch(this.customer.id);
            });
    }

    /**Get all total credits for curent customer */
    initializeTotalCredit(id) {
        let createEntitySearch: any = {};
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'hasKunde', [id]);
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);
        this.mortgagesService.fetchTotalLoans(preparedSearchParams)
            .subscribe((data: any) => {
                this.allTotalLoans = this.totalLoansFilter(data.rows);
            },
                (error) => {
                    console.log(error);
                    this.toastrService.error(this.translate.instant("loading_gesamtkredite"));
                });
    }

    /**
     *  Create array of objects with id and total loan display name for dropdown
     * @param rows total credits
     */
    totalLoansFilter(rows) {
        let filteredTotalLoans = [];
        rows.forEach(element => {
            let totalLoanForAll: any = {};
            totalLoanForAll.id = element.id;
            totalLoanForAll.totalLoanDisplayName = this.currencyPipe.transform(element.creditLimit, 'CHF', 'CHF', '0.0-0', 'de-CH') + " at " + element.creditor.name;
            filteredTotalLoans.push(totalLoanForAll);
        });
        return filteredTotalLoans;
    }


    /**Get all financing objects for curent customer */
    financeObjectSearch(id) {
        let createEntitySearch: any = {};
        createEntitySearch.predicateSource = {
            hasKunde: [id],
            isAktiv: null
        };
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);
        this.mortgagesService.fetchFinanceObject(preparedSearchParams)
            .subscribe((data: any) => {
                this.allFinancingObjects = data.rows;
            },
                (error) => {
                    console.log(error);
                    this.toastrService.error(this.translate.instant("loading_finanzierungsobjekte"));
                });
    }

    /** Go back to previous route */
    goBack() {
        this.location.back();
    }

    /** Unique retro dhow and hide section and display input depending wich radio button was clicked */
    changeRadioButtonUnique(e: any) {
        if (e.value === "zahlung_betrag") {
            this.showRadioSectionUnique = true;
            this.uniqueSumShow = true;
            this.uniquePercentShow = false;
        } else if (e.value === "zahlung_prozent") {
            this.showRadioSectionUnique = true;
            this.uniqueSumShow = false;
            this.uniquePercentShow = true;
        } else {
            this.showRadioSectionUnique = false;
        }
    }

    /** Recurring retro dhow and hide section and display input depending wich radio button was clicked */
    changeRadioButtonRecurring(e: any) {
        if (e.value === "zahlung_betrag") {
            this.showRadioSectionRecurring = true;
            this.recurringSumShow = true;
            this.recurringPercentShow = false;
        } else if (e.value === "zahlung_prozent") {
            this.showRadioSectionRecurring = true;
            this.recurringSumShow = false;
            this.recurringPercentShow = true;
        } else {
            this.showRadioSectionRecurring = false;
        }
    }

    /**Go to customer page */
    goToCustomer() {
        this.router.navigate(['/contacts/', this.customer.id]);
    }

    /** Go to lender page */
    goToLender() {
        this.router.navigate(['/contacts/lender/lender-edit/', this.creditor.id]);
    }


    onSubmit() {

        this.interestRateForm.patchValue({
            startExclusive: this.interestRateStartExclusive,
            endExclusive: this.interestRateEndExclusive
        });
        this.frameTermForm.patchValue({
            startExclusive: this.frameTermStartExclusive,
            endExclusive: this.frameTermEndExclusive
        });
        this.regularAmortizationForm.patchValue({
            startExclusive: this.regularAmortizationStartExclusive,
            endExclusive: this.regularAmortizationEndExclusive
        });

        console.log("this.mortgageForm", this.mortgageForm.value);
        console.log("this.volumeForm", this.volumeForm.value);
        console.log("this.interestRateForm", this.interestRateForm.value);
        console.log("this.frameTermForm", this.frameTermForm.value);
        console.log("this.interestDueForm", this.interestDueForm.value);
        console.log("this.regularAmortizationForm", this.regularAmortizationForm.value);
        console.log("this.uniqueRetroForm", this.uniqueRetroForm.value);
        console.log("this.reccuringRetro", this.reccuringRetro.value);

    }

    


}
