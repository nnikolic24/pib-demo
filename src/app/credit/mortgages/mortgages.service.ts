import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CreditService } from '../../service/credit/credit.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class MortgagesService extends CreditService {

  constructor(http: HttpClient) {
    super(http);
  }

  /**
   * Check if provided role is in array of rows
   * @param userRoles roles curent user has
   * @param role to check
   */
  checkRole(userRoles: Array<string>, role: string) {
    let foundCounter: number = 0;
    userRoles.forEach(item => {
      if (item == role) {
        foundCounter++;
      }
    });

    if (foundCounter != 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Prepares dates so you can send it easily to end point
   * @param lowerBound start date
   * @param upperBound end date
   */
  closed(lowerBound, upperBound) {
    let toReturn: any = {};

    if (lowerBound) {
      toReturn.lowerBoundType = 'CLOSED';
      toReturn.lowerBound = lowerBound;
    }

    if (upperBound) {
      toReturn.upperBoundType = 'CLOSED';
      toReturn.upperBound = upperBound;
    }
    return toReturn;
  }

  /**
   * Formats the dates for end point use
   * @param lowerBound start date
   * @param upperBound end date
   */
  range(lowerBound, upperBound) {
    let closed;

    if (lowerBound && upperBound) {
      closed = this.closed(moment(lowerBound).format('YYYY-MM-DD'), moment(upperBound).format('YYYY-MM-DD'));
    } else if (lowerBound && !upperBound) {
      closed = this.closed(moment(lowerBound).format('YYYY-MM-DD'), null);
    } else if (!lowerBound && upperBound) {
      closed = this.closed(null, moment(upperBound).format('YYYY-MM-DD'));
    } else {
      closed = null
    }

    return {
      closed
    }
  }

  prepareDataForGrid(data) {
    let mappedMortgages: any = [];
    if (data && data.length) {

      data.forEach(item => {

        let mortgageId = item.mortgageId;
        let groupedBy = item.groupedBy;
        let fObj: any = {};
        fObj.name = item.mortgageName ? item.mortgageName : "";
        fObj.swapped = {};
        fObj.swapped.isSwapped = item.swapped.isSwapped;
        fObj.swapped.swapPercent = item.swapped.swapPercent;
        let productType = item.productType ? item.productType : "";
        let lender = item.lender;
        let clientAdvisor = item.clientAdvisor;
        let restVolume = item.restVolume;
        let interestTerm: any = {};
        interestTerm.date = item.todayValidTermDuration;
        interestTerm.overdue = item.isOverdue;
        let binding = item.frameTermDuration;
        let interestPY = item.interestPY;
        let customer = item.customer;
        let customerID = item.customerID;

        mappedMortgages.push({
          "mortgageId": mortgageId,
          "groupedBy": groupedBy,
          "productType": productType,
          "interestTerm": interestTerm,
          "restVolume": restVolume,
          "lender": lender,
          "binding": binding,
          "clientAdvisor": clientAdvisor,
          "interestPY": interestPY,
          "customer": customer,
          "customerID": customerID,
          "property": fObj
        });
      });
    }
    return mappedMortgages;
  }

}
