import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreService } from '../../service/core/core.service';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    constructor(private http: HttpClient, private coreSvc: CoreService) { }

    getUserMatrix(userid) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/accessrules/matrix/users/${userid}`);
    }

    getUserPref(userid) {
        return this.http.get(environment.apiURL + environment.apiSufix + `/userpreferences/${userid}`);
    }
    
    updateUserPref(userid, data) {
        return this.http.put(environment.apiURL + environment.apiSufix + `/userpreferences/${userid}`, data);
    }

    sendSMS(userid, data) {
        return this.http.put(environment.apiURL + environment.apiSufix + `/ws/rest/users/${userid}/smsauthactivate`, data);
    }

    // TODO: Implement
    deactivateSMS() {

    }
}