import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageTitleService } from './../../../core/page-title/page-title.service';
import { Router } from '@angular/router';
import { AuthService } from './../../../service/auth-service/auth.service';
import { ProfileService } from './../profile.service';

@Component({
  selector: 'ms-tfa',
  templateUrl: './tfa.component.html',
  styleUrls: ['./tfa.component.scss']
})
export class TfaComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private pageTitleService: PageTitleService,
    private router: Router,
    private authService: AuthService,
    private profileService: ProfileService) { }

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  deactivateFormGroup: FormGroup;
  pattern: string = "^[0-9*#+]+$";
  userData;
  smsActivate = [];
  show: boolean;

  ngOnInit() {
    this.pageTitleService.setTitle("Two factor authentication");
    this.userData = this.authService.getLocalStorageUser();

    this.firstFormGroup = this.formBuilder.group({
      phoneNumber: ['', [Validators.required, Validators.pattern(this.pattern)]]
    });
    this.secondFormGroup = this.formBuilder.group({
      token: ['', Validators.required]
    });
    this.deactivateFormGroup = this.formBuilder.group({
      token: ['', Validators.required]
    });

    this.fillForm();

    this.profileService.getUserPref(this.userData.id)
      .subscribe((data: any) => {
        this.smsActivate = data.preferences.filter(data => data.key === "smsauth.enabled");
        if (this.smsActivate[0].value == "false") {
          this.show = false;
        } else {
          this.show = true;
        }
      });
  }

  goToProfile() {
    this.router.navigate(["/dashboard/profile"]);
  }

  fillForm() {
    this.firstFormGroup.controls.phoneNumber.setValue(this.userData.mobileNumber);
  }

  sendCode() {
    console.log(this.firstFormGroup.value);
    this.profileService.sendSMS(this.userData.id, this.firstFormGroup.value)
      .subscribe(result => {
        console.log("Success: ", result);
      }, error => {
        console.log(error);
      });
  }

  // TODO: Figure out how it should work and implement it
  deactivateSMS() {
    console.log("Test for now");
  }

}
