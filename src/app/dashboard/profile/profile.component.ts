import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { AuthService } from '../../service/auth-service/auth.service';
import { ProfileService } from './profile.service';
import { CoreService } from './../../service/core/core.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userData: any;
  userPreferences: any;
  userInterestRate = [];
  userAmortization = [];
  updatePreferences = {
    preferences: []
  }
  public form: FormGroup;

  period = [
    { name: 'date_jahr', value: 'Jahr' },
    { name: 'date_monat', value: 'Monat' }
  ];

  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private fb: FormBuilder,
    private coreService: CoreService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.form = this.fb.group({
      interestRate: [null, Validators.compose([Validators.required])],
      amortization: [null, Validators.compose([Validators.required])]
    });
    this.userData = this.authService.getLocalStorageUser();
    console.log(this.userData);
    this.profileService.getUserPref(this.userData.id)
      .subscribe((data: any) => {
        this.userPreferences = data;
        this.userInterestRate = data.preferences.filter(data => data.key === "hypo.zinsAnzeigeZeitraum");
        this.userAmortization = data.preferences.filter(data => data.key === "hypo.amortisationAnzeigeZeitraum");
        this.fillForm(this.userInterestRate[0], this.userAmortization[0]);
      });
  }

  fillForm(userIR, userA) {
    this.form.controls.interestRate.setValue(userIR.value);
    this.form.controls.amortization.setValue(userA.value);
  }

  updateUserSettings() {
    this.updatePreferences.preferences.push({
      key: "hypo.zinsAnzeigeZeitraum",
      value: this.form.value.interestRate
    });

    this.updatePreferences.preferences.push({
      key: "hypo.amortisationAnzeigeZeitraum",
      value: this.form.value.amortization
    });

    this.profileService.updateUserPref(this.userData.id, this.updatePreferences)
      .subscribe(item => {
        this.toastr.success('Successfully updated!');
      }, error => {
        console.log(error);
        this.toastr.error('There was an error!');
      });
  }

  goToSMS() {
    this.router.navigate(["/dashboard/profile/smsAuthWizard"]);
  }

}
