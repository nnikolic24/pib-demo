import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard-v1/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { TfaComponent } from './profile/tfa/tfa.component'

export const DashboardRoutes: Routes = [
   {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
   },
   {
      path: '',
      children: [
         {
            path: 'dashboard',
            component: DashboardComponent
         },
         {
            path: 'profile',
            component: ProfileComponent
         },
         {
            path: 'profile/smsAuthWizard',
            component: TfaComponent
         }
      ]
   }
];

@NgModule({
   imports: [RouterModule.forChild(DashboardRoutes)],
   exports: [RouterModule]
 })
 export class DashboardRoutingModule { }