import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from "../../core/route-animation/route.animation";
import { Router } from '@angular/router';
import { CoreService } from '../../service/core/core.service';
import { TranslateService } from '@ngx-translate/core';

import { MatDialog, MatDialogRef } from '@angular/material';

import { RegisterUserDialogComponent } from 'app/shared/dialogs/registration-dialog';
import { ConsultantDialogComponent } from 'app/shared/dialogs/consultant-info-dialog';
import { SupportInfoDialogComponent } from 'app/shared/dialogs/support-info-dialog';

import { AuthService } from '../../service/auth-service/auth.service';

@Component({
   selector: 'ms-dashboard',
   templateUrl: './dashboard-component.html',
   styleUrls: ['./dashboard-component.scss'],
   encapsulation: ViewEncapsulation.None,
   host: {
      "[@fadeInAnimation]": 'true'
   },
   animations: [fadeInAnimation]
})

export class DashboardComponent implements OnInit {

   registerUserDialogRef: MatDialogRef<RegisterUserDialogComponent>;
   consultantDialogRef: MatDialogRef<ConsultantDialogComponent>;
   supportInfoDialogRef: MatDialogRef<SupportInfoDialogComponent>;

   // User Data from LocalStorage
   userData:any;

   constructor(private pageTitleService: PageTitleService,
      private authService: AuthService,
      private router: Router,
      private dialog: MatDialog) {
   }

   ngOnInit() {
      this.pageTitleService.setTitle("Home");
      
      // Get the data from LocalStorage (first name and last name of the logged in user)
      this.userData = this.authService.getLocalStorageUser();
   }

   goToProfile() {
      this.router.navigate(['/dashboard/profile']);
   }

   openRegisterDialog() {
      this.registerUserDialogRef = this.dialog.open(RegisterUserDialogComponent, { autoFocus: false });
   }

   openConsultantDialog() {
      this.consultantDialogRef = this.dialog.open(ConsultantDialogComponent, { autoFocus: false });
   }

   openSupportDialog() {
      this.supportInfoDialogRef = this.dialog.open(SupportInfoDialogComponent, { autoFocus: false });

   }

}