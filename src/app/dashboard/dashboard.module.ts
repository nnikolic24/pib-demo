import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { RouterModule } from '@angular/router';

import {
	MatInputModule,
	MatFormFieldModule,
	MatCardModule,
	MatButtonModule,
	MatIconModule,
	MatCheckboxModule,
	MatDividerModule,
	MatToolbarModule,
	MatTabsModule,
	MatStepperModule,
	MatProgressBarModule
} from '@angular/material';

import { NgSelectModule } from '@ng-select/ng-select';

import { DashboardComponent } from './dashboard-v1/dashboard.component';
import { ProfileComponent } from './profile/profile.component';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { TfaComponent } from './profile/tfa/tfa.component';
import { SharedModule } from './../shared/shared.module';

@NgModule({
	declarations: [
		DashboardComponent,
		ProfileComponent,
		TfaComponent
	],
	imports: [
		MatInputModule,
		MatFormFieldModule,
		FlexLayoutModule,
		MatCardModule,
		MatButtonModule,
		MatIconModule,
		MatToolbarModule,
		CommonModule,
		FlexLayoutModule,
		DashboardRoutingModule,
		MatTabsModule,
		MatDividerModule,
		NgSelectModule,
		FormsModule,
		ReactiveFormsModule,
		MatStepperModule,
		MatProgressBarModule,
		TranslateModule,
		SharedModule
	]
})

export class DashboardModule { }
