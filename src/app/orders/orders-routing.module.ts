import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './orders/orders.component';
import { CommissionsComponent } from './commissions/commissions.component';
import { OrderDisplayComponent } from './orders/order-display/order-display.component';
import { OrderEditComponent } from './orders/order-edit/order-edit.component';



const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'orders',
      component: OrdersComponent
    },
    {
      path: 'commissions',
      component: CommissionsComponent
    },
    {
      path: 'orders/order-display/:id',
      component: OrderDisplayComponent
    },
    {
      path: 'orders/order-edit/:id',
      component: OrderEditComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
