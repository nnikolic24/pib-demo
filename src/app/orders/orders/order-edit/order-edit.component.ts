import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { OrdersService } from '../orders.service';
import { BlockUIService } from 'ng-block-ui';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';

@Component({
  selector: 'ms-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})

export class OrderEditComponent implements OnInit {

  routeSubscription: Subscription;
  ordersForm: FormGroup;

  /**
   * Initially empty Array which will contains total loans and Details Type data,
   * which will be displayed on orders edit form (dropdowns)
   */
  totalLoans: any = [];
  type: any = [];
  
  showPercent: boolean = false;
  showSum: boolean = false;

  public assignmentData: any = {
    id: '',
    assignmentNumber: '',
    contactPerson: '',
    controller: '',
    crmTypId: '',
    creationDate: ''    
  }; 

  constructor(private route: ActivatedRoute,
              private ordersService: OrdersService,
              private translateService: TranslateService,
              private blockUI: BlockUIService,
              private pageTitleService: PageTitleService,
              private translate: TranslateService) { }

  ngOnInit() {
    this.blockUI.start('main-block');

    this.ordersForm = new FormGroup({
      type: new FormControl('', Validators.required),
      totalLoans: new FormControl('', Validators.required),
      feeType: new FormGroup({
        fee: new FormControl('None', Validators.required),
        feeTypeValue: new FormGroup({
          feePercent: new FormControl('', [Validators.min(1), Validators.max(100)]),
          feeSum: new FormControl('')
        })
      })
    });

    this.routeSubscription = this.route.params.subscribe(params => {
      // Fetch data of single assignment , based on ID 
      this.ordersService.fetchAssignment(params.id).subscribe((data) => {
        this.assignmentData = this.ordersService.mapAssignmentData(data);
        this.blockUI.stop('main-block');
      })
    })
  }

  // Get type value (Radio buttons - None, Sum, Percent)
  get feeTypeValue() { 
    return (this.ordersForm.get('feeType.fee')).value; 
  }

  // Go back to previous page
  cancel(){
    window.history.back();
  }

  // Submit Order form
  onSubmit() {
    console.log(this.ordersForm.value);
  }
}