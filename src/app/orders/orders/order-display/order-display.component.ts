import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from "@angular/router";
import { Subscription } from 'rxjs';
import { OrdersService } from '../orders.service';
import { BlockUIService } from 'ng-block-ui';
import { TranslateService } from '@ngx-translate/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';


@Component({
  selector: 'ms-order-display',
  templateUrl: './order-display.component.html',
  styleUrls: ['./order-display.component.scss']
})

export class OrderDisplayComponent implements OnInit {

  routeSubscription: Subscription;
  show: boolean = false;

  public assignmentData: any = {
    id: '',
    assignmentNumber: '',
    contactPerson: '',
    controller: '',
    crmTypId: '',
    creationDate: ''    
  };

  constructor(private route: ActivatedRoute,
              private ordersService: OrdersService,
              private blockUI: BlockUIService,
              private translate: TranslateService) { }

  columns = [
    { 
      name: 'Lender',
    },
    { 
      name: 'Credit volume (CHF)'
    },
    { 
      name: 'Fee (CHF)'
    }
  ];

  ngOnInit() {
    this.blockUI.start('main-block');

    // Translation
    this.callTranslateService();

    this.routeSubscription = this.route.params.subscribe(params => {
      // Fetch data of single assignment , based on ID 
      this.ordersService.fetchAssignment(params.id).subscribe((data) => {
        this.assignmentData = this.ordersService.mapAssignmentData(data);

        this.blockUI.stop('main-block');
      })
      this.assignmentData.id = params.id;
    })
  }

  // Go back
  goBack(){
    window.history.back();
  }

  // Open Bexio login page when user click on the Order Number from Order Display form
  openBexio() {
    const url = 'https://idp.bexio.com/login';
    window.open(url, "_blank");
  }

  // Generate position in CRM 
  generateCrmPosition() {
    this.ordersService.createAutragsPosition(this.assignmentData.id, this.assignmentData);
  }

  callTranslateService() {
    //On lang change translate page title
    this.translate.onLangChange.subscribe((event: any) => {
      this.columns = [
        { 
          name: this.translate.instant('auftrag_lender'),
        },
        { 
          name: this.translate.instant('dash_summary_loan_vol')
        },
        { 
          name: this.translate.instant('auftrag_price')
        }
      ];
    });
  }


  showExpand(){
    this.show = true;
  }

  showMin(){
    this.show = false;
  }
}