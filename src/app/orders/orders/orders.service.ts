import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { TranslateService } from "@ngx-translate/core";

import { environment } from '../../../environments/environment';
import { CreditService } from "app/service/credit/credit.service";

@Injectable({
   providedIn: 'root'
 })
export class OrdersService {

    mappedData: any;

    constructor(private http: HttpClient,
                private toastr: ToastrService,
                private translate: TranslateService,
                private creditService: CreditService) { }

    /**
     * Get assignments from assignments endpoint
     */
    fetchAssignemnts() {
        return this.http.get(environment.apiURL + environment.apiSufix + "/assignments");
    }

    /**
     * Get single assignment
     * @param id Assignment ID
     */
    fetchAssignment(id) {
        return this.http.get(environment.apiURL + environment.apiSufix + "/assignments/" + id);
    }

    /**
     * parseData will receive assignments data from server and use only data for columns that needed
     */
    prepareAssignmentsData(result) {
        // Empty array in which will be stored mapped data
        let data: any = [];

        // Check if result exists and map to column names
        if(result && result.rows) {
            
            result.rows.forEach(row => {

            if (row.id) {
                let id = row.id;
                let assignmentNumber = row.assignmentNumber ? row.assignmentNumber: "-";
                let name = row.contact.name ? row.contact.name: "-";
                let surname = row.contact.surname ? row.contact.surname: "-";
                let contatPerson = name + " " + surname;
                let controller = row.contact.clientAdvisor.firstName;

                let crmTypId = row.crmTypId ? row.crmTypId: "Not assigned";

                let creationDate = row.creationDate;

                data.push({
                    "id": id,
                    "assignmentNumber": assignmentNumber,
                    "contactPerson": contatPerson,
                    "controller": controller,
                    "crmTypId": crmTypId,
                    "creationDate": creationDate
                })
            }    

        });
    }
    // Return mapped data
    return data;
    }


    mapAssignmentData(data: any) {
        // Empty array in which will be stored mapped data
        let mappedData: any = {};
    
        // Check if result exists and map column data
        if (data) {
          mappedData.id = data.id ? data.id: "";
          mappedData.assignmentNumber = data.assignmentNumber || "";
          mappedData.creationDate = data.creationDate || "";
          if (data.contact) {
              mappedData.contact = data.contact.name + " " + data.contact.surname
              mappedData.firstName = data.contactPerson.firstName
          }
          mappedData.crmTypId = data.crmTypId || "-";
          mappedData.comment = data.comment || "-";
        }
    
        // Return mapped data
        return mappedData;
    }

    // Create Auftrags Position (Orders)
    createAutragsPosition(id: string, data: any) {
        return this.http.post(
            environment.apiURL + environment.apiSufix + "/assignments/" + id + "/itemposition", data
        ).subscribe((result) => {
            // Position created in CRM
            this.toastr.success(this.translate.instant("auftrag_generiere_position_success"));
        }, error => {
            // Error during position creation in CRM
            this.toastr.error(this.translate.instant("auftrag_generiere_position_failed"));
        });
    }

    // Get Total Loans data for dropdown
    getTotalLoans() {
        // this.creditService.fetchTotalLoansOnInit();
    }
}