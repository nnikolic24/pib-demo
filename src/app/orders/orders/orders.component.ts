import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { OrdersService } from './orders.service';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { TranslateService } from '@ngx-translate/core';
import { BlockUIService } from 'ng-block-ui';

@Component({
  selector: 'ms-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})

export class OrdersComponent implements OnInit {

  assignmentArray = [];
  assignmentTempArray = [];
  noAssignmentMessage: string = '';

  columns = [
    { 
      name: 'Number', 
      prop: 'assignmentNumber' 
    },
    { 
      name: 'Contact', 
      prop: 'contactPerson'
    },
    { 
      name: 'Controller', 
      prop: 'controller'
    },
    { 
      name: 'Type', 
      prop: 'crmTypId' 
    },
    { 
      name: 'Date created', 
      prop: 'creationDate' 
    }
  ];

  constructor(private router: Router,
              private translate: TranslateService,
              private pageTitleService: PageTitleService,
              private ordersService: OrdersService,
              private blockUI: BlockUIService) { }

  ngOnInit() {

    this.blockUI.start('main-block');
  
    // Translation
    this.callTranslateService();

    //Get all assignments from assignments endpoint
    this.fetchAssignments();
  }

  callTranslateService(){
    //Set title on initila load of page
    this.pageTitleService.setTitle(this.translate.instant("auftraege"));

    //On lang change translate page title
    this.translate.onLangChange.subscribe((event: any) => {
       this.pageTitleService.setTitle(this.translate.instant("auftraege"));

       this.columns = [
        { 
          name: this.translate.instant('auftrag_nummer'), 
          prop: 'assignmentNumber' 
        },
        { 
          name: this.translate.instant('kontakt'), 
          prop: 'contactPerson'
        },
        { 
          name: this.translate.instant('auftrag_verantwortlicher'), 
          prop: 'controller'
        },
        { 
          name: this.translate.instant('auftrag_typ'), 
          prop: 'crmTypId' 
        },
        { 
          name: this.translate.instant('creation_date'), 
          prop: 'creationDate' 
        }
      ];
    });
  }

  fetchAssignments() {
    // Get all roles from role endpoint
    this.ordersService.fetchAssignemnts()
      .subscribe((data) => {
          
            // cache our list
            this.assignmentTempArray = this.ordersService.prepareAssignmentsData(data);
      
            // push our inital complete list
            this.assignmentArray = this.ordersService.prepareAssignmentsData(data);

            this.blockUI.stop('main-block');

            // Check if there is no assingments inthe assignment array
            if(this.assignmentArray.length == 0) {
              this.noAssignmentMessage = this.translate.instant("auftraege_none_found");
            }
      },
      (error) => {
          console.log('Error', error);
      }
    );
  }

  onActivate(event) {
      // If user clicks on row it should redirect him to form with user data 
      if (event.type == 'click') {
        this.router.navigate(['/orders/orders/order-display/', event.row.id]);
    }
  }
 }