import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders/orders.component';
import { CommissionsComponent } from './commissions/commissions.component';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { NgSelectModule } from '@ng-select/ng-select';

import { MatInputModule,
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatIconModule,
  MatDividerModule,
  MatRadioModule,
  MatDatepickerModule,
  MatTabsModule,
} from '@angular/material';

import { OrderDisplayComponent } from './orders/order-display/order-display.component';
import { OrderEditComponent } from './orders/order-edit/order-edit.component';
import { CommisionsOpenComponent } from './commissions/commisions-open/commisions-open.component';
import { CommisionsBillableComponent } from './commissions/commisions-billable/commisions-billable.component';

@NgModule({
  declarations: [OrdersComponent, 
                 CommissionsComponent, 
                 OrderDisplayComponent, 
                 OrderEditComponent, 
                 CommisionsOpenComponent, 
                 CommisionsBillableComponent
                ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    OrdersRoutingModule,
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatRadioModule,
    NgxDatatableModule,
    MatDatepickerModule,
    FlexLayoutModule,
    MatTabsModule,
    TranslateModule,
    NgSelectModule
  ]
})
export class OrdersModule { }
