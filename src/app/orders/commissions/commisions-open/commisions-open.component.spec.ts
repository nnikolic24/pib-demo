import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommisionsOpenComponent } from './commisions-open.component';

describe('CommisionsOpenComponent', () => {
  let component: CommisionsOpenComponent;
  let fixture: ComponentFixture<CommisionsOpenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommisionsOpenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommisionsOpenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
