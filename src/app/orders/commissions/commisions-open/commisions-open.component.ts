import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ms-commisions-open',
  templateUrl: './commisions-open.component.html',
  styleUrls: ['./commisions-open.component.scss']
})
export class CommisionsOpenComponent implements OnInit {

  show: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  showExpand(){
    this.show = true;
  }

  showMin(){
    this.show = false;
  }
}
