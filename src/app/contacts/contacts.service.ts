import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreService } from "./../service/core/core.service";
import { environment } from './../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ContactsService {

    constructor(private http: HttpClient,
        private coreSvc: CoreService, ) { }


    fetchContactsPersons(preparedSearchParams) {
        let paged = encodeURIComponent(JSON.stringify(preparedSearchParams.paged));
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));

        return this.http.get(environment.apiURL + environment.apiSufix + `/contacts/search?paged=${paged}&predicate=${predicates}`);
    }

    //TODO not sure what is needed here as param???
    synchronizeContacts(rows) {
        return this.http.post(environment.apiURL + environment.apiSufix + "/contacts/synchronize", rows);
    }

    /**
   * Get Contacts from Contacts endpoint with id
   */
    fetchContact(id) {
        return this.http.get(environment.apiURL + environment.apiSufix + "/contacts/" + id);
    }


    /**
     * parseData will receive data from server
     * and use only data for columns that needed
     */
    parseData(result) {
        // Empty array in which will be stored mapped data
        let data: any = [];

        // Check if result exists and map to column names
        if (result && result.rows) {

            result.rows.forEach(row => {
                // declare contact name object 
                let contactName = {};

                // First name and last name
                contactName["name"] = row.name;
                contactName["surname"] = row.surname;
                contactName["fullName"] = '';

                if (row.surname) {
                    contactName["fullName"] = row.name + " " + row.surname;
                } else {
                    contactName["fullName"] = row.name;
                }

                contactName["id"] = row.id;

                // Street postcode and city
                contactName["adresse"] = row.street;
                if (row.street) {
                    contactName["adresseFull"] = row.street + ' , ' + row.postcode + ' ' + row.municipality;
                } else {
                    contactName["adresseFull"] = '-';
                }

                //Email
                contactName["email"] = row.email;
                if (row.email) {
                    contactName["email"] = row.email;
                } else {
                    contactName["email"] = '-';
                }

                contactName["location"] = row.site;

                //Checks if contact is Customer or Lander
                contactName["contactType"] = '';
                contactName["contactTypeCustomer"] = row.customer;
                contactName["contactTypeLender"] = row.creditor;
                if (row.customer === true && row.creditor === false) {
                    contactName["contactType"] = 'Customer';
                } else if (row.creditor === true && row.customer === false) {
                    contactName["contactType"] = 'Lender';
                } else {
                    contactName["contactType"] = '-';
                }


                data.push(contactName);
            });

        }

        this.sortByContact(data);
        // Return mapped data
        return data;
    }

    /**
* Sort total loans array by contacts name
*/
    sortByContact(rows) {
        return rows.sort((a, b) => a.name.localeCompare(b.name));
    }

    mapAssignmentData(data) {
        // Empty array in which will be stored mapped data
        let mappedData: any = {};

        // Check if result exists and map column data
        if (data) {
            mappedData.id = data.id ? data.id : "";
            mappedData.name = data.name;
            mappedData.fullAdresse = data.street + ' , ' + data.postcode + ' ' + data.municipality;
            mappedData.email = data.email;
            mappedData.privatePhoneNo = data.privatePhoneNo;
            mappedData.mobilePhoneNo = data.mobilePhoneNo;
            mappedData.businessPhoneNo = data.businessPhoneNo;
            mappedData.clientAdvisor = data.clientAdvisor.firstName;
        }

        // Return mapped data
        return mappedData;
    }

}
