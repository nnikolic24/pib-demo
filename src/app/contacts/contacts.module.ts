import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactsRoutingModule } from './contacts-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { CustomersComponent } from './customers/customers.component';
import { LenderComponent } from './lender/lender.component';
import { ContactsComponent } from './contacts/contacts.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LenderEditComponent } from './lender/lender-edit/lender-edit.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {CreditModule} from '../credit/credit.module';

import { MatInputModule,
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatIconModule,
  MatDividerModule,
  MatRadioModule,
  MatDatepickerModule,
  MatTabsModule
} from '@angular/material';
import { CustomerDetailsComponent } from './contacts/customer-details/customer-details.component';
import { SharedModule } from '../shared/shared.module';
import { EditCustomerComponent } from './customers/edit-customer/edit-customer.component';
import { EditContactComponent } from './contacts/edit-contact/edit-contact.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [CustomersComponent, LenderComponent, ContactsComponent, LenderEditComponent, CustomerDetailsComponent, EditCustomerComponent, EditContactComponent],
  imports: [
    CommonModule,
    SharedModule,
    ContactsRoutingModule,
    MatInputModule,
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatIconModule,
  MatDividerModule,
  MatRadioModule,
  NgxDatatableModule,
  MatDatepickerModule,
  FlexLayoutModule,
  MatTabsModule,
  TranslateModule,
  FormsModule,
  ReactiveFormsModule
  ]
})
export class ContactsModule { }
