import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";

import { PageTitleService } from '../../core/page-title/page-title.service';
import { ContactsService } from '../contacts.service';
import { EntitySearchService } from '../../service/core/entity-search.service';
import { TranslateService } from '@ngx-translate/core';
import { BlockUIService } from 'ng-block-ui';


@Component({
    selector: 'ms-contacts',
    templateUrl: './contacts.component.html',
    styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

    public preparedSearchParams: any;
    public totalNumberOfElemetns: number = 0;
    public searchQuery: string = "";

    public contactsArray = [];

    columns = [
        { name: this.translate.instant("kontakt_name"), prop: 'fullName' },
        { name: this.translate.instant("kontakt_adresse"), prop: 'adresseFull' },
        { name: this.translate.instant("kontakt_email"), prop: 'email' },
        { name: this.translate.instant("standort"), prop: 'location' },
        { name: this.translate.instant("kontakt_typ"), prop: 'contactType' },
    ];

    temp: any = [];

    constructor(
        private pageTitleService: PageTitleService,
        private router: Router,
        private translate: TranslateService,
        private toastrService: ToastrService,
        private entitySearchService: EntitySearchService,
        private contactsService: ContactsService,
        private blockUI: BlockUIService) {

    }

    ngOnInit() {
        this.blockUI.start('main-block');
        //Translate text generated through code
        this.callTransalteService();

        this.prepareEntitySearch();
    }

    /**
    * Used for translating text that's provided through code
    * Set initial translation for page title, and further translate on lang change
    */
    callTransalteService() {
        //Set title on initila load of page
        this.pageTitleService.setTitle(this.translate.instant("kontakte"));

        //On lang change transalte page title
        this.translate.onLangChange.subscribe((event: any) => {
            this.pageTitleService.setTitle(this.translate.instant("kontakte"));
            this.columns = [
                { name: this.translate.instant("kontakt_name"), prop: 'fullName' },
                { name: this.translate.instant("kontakt_adresse"), prop: 'adresseFull' },
                { name: this.translate.instant("kontakt_email"), prop: 'email' },
                { name: this.translate.instant("standort"), prop: 'location' },
                { name: this.translate.instant("kontakt_typ"), prop: 'contactType' }
            ];
        });
    }

    /**
    * Function will execute on any row event
    * @param event Received event
    */
    onActivate(event) {
        // If user clicks on row it should redirect him to form with user data 
        if (event.type == 'click') {
            this.router.navigate(['/contacts/', event.row.id]);
        }
    }

    /**
    * On init preapre search params for contacts entity
    * If "Search" input has value provide it for search action
    * Result is grid data
    * @param getPage grid footer provides page number
    */
    prepareEntitySearch(getPage?) {

        let createEntitySearch: any = {};
        createEntitySearch.entity = "contacts"; //TODO check will it be needed on new app like this
        createEntitySearch.paging = { sortColumns: "name" };
        createEntitySearch.predicateSource = {
            hasName: "%" + this.searchQuery + "%",
        };

        //Create entity search with provided params
        let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
        entitySearch = this.entitySearchService.pushPredicate(entitySearch, "!isDeleted", null); //We need to use pushPredicate function to push param with !
        //prepare params to be provided for endpoint
        this.preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);

        //if we get footer page number set params
        if (getPage) {
            this.preparedSearchParams.paged.page = getPage.page;
        }

        this.getData(this.preparedSearchParams);
    }

    /**
     * Call endpoint with prepared params
     * @param preparedSearchParams 
     */
    getData(preparedSearchParams) {
        this.contactsService.fetchContactsPersons(preparedSearchParams)
            .subscribe((data: any) => {
                //Prepare needed data from recived json
                this.contactsArray = this.contactsService.parseData(data);
                //We get number of total items so we can use it for footer paging
                this.totalNumberOfElemetns = data.total;
                this.blockUI.stop('main-block');
            });
    }

    /**
     * TODO not sure what is needed here as param?
     */
    synchronize() {
        this.blockUI.start('main-block');
        this.contactsService.synchronizeContacts(this.contactsArray)//<<<<<<<<
            .subscribe((data: any) => {
                this.getData(this.preparedSearchParams);
                this.toastrService.success(this.translate.instant("synchronize_kontakte"));
            },
                (error) => {
                    // Show update error message in right corner
                    this.toastrService.error(this.translate.instant("synchronize_kontakte_failed"));
                });
    }

    /**
    * Filter data by provided "searchQuery" value
    * For search result set grid result to first page in grid
    */
    filterBy() {
        this.blockUI.start('main-block');
        let firstPage = { page: 1 }
        this.prepareEntitySearch(firstPage);
    }

}
