import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactsComponent } from './contacts/contacts.component';
import { LenderComponent } from './lender/lender.component';
import { CustomersComponent } from './customers/customers.component';
import { LenderEditComponent } from './lender/lender-edit/lender-edit.component';
import { CustomerDetailsComponent } from './contacts/customer-details/customer-details.component';
import { EditContactComponent } from './contacts/edit-contact/edit-contact.component';
import { EditCustomerComponent } from './customers/edit-customer/edit-customer.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'contacts',
      component: ContactsComponent
    },
    {
      path: 'lender',
      component: LenderComponent,
    },
    {
      path: 'customers',
      component: CustomersComponent
    },
    {
      path: 'lender/lender-edit/:id',
      component: LenderEditComponent
    },
    {
      path: 'contacts/customer-details',
      component: CustomerDetailsComponent
    },
    {
      path: 'customers/edit-customer/:id',
      component: EditCustomerComponent
    },
    {
      path: ':id',
      component: EditContactComponent
    }


  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule { }
