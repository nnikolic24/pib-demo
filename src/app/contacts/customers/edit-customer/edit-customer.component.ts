import { Component, OnInit, ViewChild } from '@angular/core';

import { DurationComponent } from './../../../shared/duration/duration.component';
import { DueCreditsComponent } from './../../../shared/due-credits/due-credits.component';
import { LenderStructureComponent } from './../../../shared/lender-structure/lender-structure.component';
import { CreditGrowthComponent } from './../../../shared/credit-growth/credit-growth.component';
import { CustomerDetailsComponent } from '../../../shared/customer-details/customer-details.component';
import { CockpitComponent } from '../../../shared/cockpit/cockpit.component';
import {InterestsComponent} from '../../../shared/interests/interests.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ms-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss']
})
export class EditCustomerComponent implements OnInit {

  id;

  constructor(
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
  }

  @ViewChild("durationShared") durationShared: DurationComponent;
  @ViewChild("dueCreditsShared") dueCreditsShared: DueCreditsComponent;
  @ViewChild("lenderStructureShared") lenderStructureShared: LenderStructureComponent;
  @ViewChild("creditGrowthShared") creditGrowthShared: CreditGrowthComponent;
  @ViewChild("cockpitShared") cockpitShared: CockpitComponent;
  @ViewChild("interestsShared") interestsShared:CockpitComponent;
  

  tabChanged(event) {
    console.log("event.index", event.index);
    switch (event.index) {
      //   case 1:
      //   this.analysesInterestTerm.initChart();
      //   break;
      case 1:
        this.cockpitShared.initChart();
        break;
      
      case 2:
        this.interestsShared.initChart();
        break;

      case 3:
        this.durationShared.initChart();
        break;

      case 4:
        this.dueCreditsShared.initChart();
        break;

      case 5:
        this.lenderStructureShared.initChart();
        break;

      case 6:
        this.creditGrowthShared.initChart();
        break;
    }
  }


  goBack(){
  }

  newProperty() {
    this.router.navigate(['properties/new/', this.id]);
  }

}
