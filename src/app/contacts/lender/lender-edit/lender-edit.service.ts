import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreService } from "../../../service/core/core.service";
import { environment } from './../../../../environments/environment'

@Injectable({
    providedIn: 'root'
})
export class LenderEditService {

    constructor(private http: HttpClient,
        private coreSvc: CoreService, ) { }

    /**
   * Get Contacts from Contacts endpoint with id
   */
    getTotalLoans(id){
        let myVar = { "predicate": [{ "name": "hasKreditgeber", "parameter": ["[\"" + id + "\"]"] }, { "name": "isAktiv" }] };
        let predicated = encodeURIComponent(JSON.stringify(myVar));
        return this.http.get(environment.apiURL + environment.apiSufix + `/totalloans/search?predicate=${predicated}`);
    }

    fetchTotalLoansOnInit(preparedSearchParams) {
        let predicates = encodeURIComponent(JSON.stringify(preparedSearchParams.predicate));
        let referanceDay = preparedSearchParams.referenceday;
        return this.http.get(environment.apiURL + environment.apiSufix + `/totalloans/search?&predicate=${predicates}&referenceday=${referanceDay}`);
      }

    //Update Rental income row by id
    updateRentalIncome(id, data) {
        console.log("this is data in service:", data);
        return this.http.put(environment.apiURL + environment.apiSufix + '/rentalIncome/' + id,data);
    }

    //Delete  Rental income row by id
    deleteRentalIncome(id) {        
        return this.http.delete(environment.apiURL + environment.apiSufix + '/rentalIncome/' + id);
    }
    
    /**
* Get Rental Income from Contacts endpoint with id
*/
    getRentalIncome(id) {
        let myVar = { "predicate": [{ "name": "hasKunde", "parameter": ["[\"" + id + "\"]"] }] };
        let predicated = encodeURIComponent(JSON.stringify(myVar));
        return this.http.get(environment.apiURL + environment.apiSufix + `/rentalIncome/search?predicate=${predicated}`);
    }

}
