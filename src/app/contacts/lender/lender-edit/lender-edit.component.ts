import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PageTitleService } from '../../../core/page-title/page-title.service';
import { CustomValidators } from 'ng2-validation';
import { Subscription } from 'rxjs';
import { CoreService } from "./../../../service/core/core.service";
import { ContactsService } from '../../contacts.service';
import { ToastrService } from 'ngx-toastr';
import { LenderEditService } from './lender-edit.service';
import { forkJoin } from 'rxjs';
import { BlockUIService } from 'ng-block-ui';
import * as moment from 'moment';


import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { EntitySearchService } from 'app/service/core/entity-search.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
    selector: 'ms-lender-edit',
    templateUrl: './lender-edit.component.html',
    styleUrls: ['./lender-edit.component.scss']
})
export class LenderEditComponent implements OnInit {

    // Getter for form controls TODO
    get getFromControl() {
        return this.DaterForm.controls;
    }

    tempTotalLoans: any = [];
    rowsTotalLoans: any = [];
    lenderId: any;

    loans = {};
    adaptedLoans = [];

    rentalIncome = {};
    adaptedRentalIncome = [];

    public isNameEditable: boolean = false;
    public showChart: boolean = false;

    isEditable = {};

    public referanceDate: string;

    public showEditSectionMessage: boolean = false;
    public showEditSection: boolean = false;
    public warningMessage:  boolean = false;
    public rateSeries: any = [];
    public rateSeriesName: string = "";

    id;
    
    public contactId: any;

    DaterForm: FormGroup;


    columnsTotalLoans = [
        { name: this.translate.instant("kontakt_name"), prop: 'fullName' },
        { name: this.translate.instant("kontakt_adresse"), prop: 'adresseFull' },
        { name: this.translate.instant("kontakt_email"), prop: 'email' },
        { name: this.translate.instant("standort"), prop: 'location' },
    ];

    //Data Model for presenting data
    public lenderData: any = {
        id: '',
        fullName: '',
        fullAdresse: '',
        email: '',
        privatePhoneNo: '',
        mobilePhoneNo: '',
        businessPhoneNo: '',
        clientAdvisor: ''
    };

    public rentalIncomeDataObject: any = {};
    public rentalIncomeData: any = {
        rentalIncome: 0,
        activatedSince: ""

    };

    public rentalIncomeId = this.rentalIncome

    routeSubscription: Subscription;

    public mortgageVolumeSumDisplay = 0;

    constructor(
        private fb: FormBuilder,
        private pageTitleService: PageTitleService,
        private router: Router,
        private route: ActivatedRoute,
        private cntSvc: ContactsService,
        private translate: TranslateService,
        private toastrService: ToastrService,
        private lenderEditService: LenderEditService,
        private coreService: CoreService,
        private blockUI: BlockUIService,
        private entitySearchService: EntitySearchService,
    ) { }


    // Getter for form controls
    get f() {
        return this.DaterForm.controls;
    }

    ngOnInit() {
        this.id = this.route.snapshot.params.id;
        //Set referance day for endpoint param
        this.referanceDate = moment(new Date()).format("YYYY-MM-DD");

        this.contactId = this.route.snapshot.params.id;
        
        this.getTotalLoans();
        
        //Form creation, setting date and validating it
        this.DaterForm = this.fb.group({
            FromDate: [new Date(), Validators.required],
            rentalIncome: ['', Validators.required],
            activatedSince: [new Date(), Validators.required],
            id: [null],
        });

        //Translate text generated through code
        this.callTransalteService();

        this.routeSubscription = this.route.params.subscribe(routeParams => {
            // Fetch data of single assignment , based on ID 
            this.cntSvc.fetchContact(routeParams.id).subscribe((data) => {
                this.lenderData = this.cntSvc.mapAssignmentData(data);
                this.lenderId = this.lenderData.id;

                this.prepare();
            })
        });
    }


    myFilter = (d: Date): boolean => {
        const day = d.getDay();
        console.log("ovo je dan", day);
        // Prevent Saturday and Sunday from being selected.
        return day  == 0;
      }

        //Allows only number for input rental income field
        numberOnly(event): boolean {
            const charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        //Reference day

        referceDay(){
            let preparedObject: any = {};

            if (this.DaterForm.value.FromDate) {
                preparedObject.FromDate = moment(this.DaterForm.value.FromDate).format("YYYY-MM-DD");
            }

            console.log("ovo je referentni dan:", preparedObject);
            //this.getTotalLoans(preparedObject);


            let createEntitySearch: any = {};
    
            //Create entity search with provided params
            let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
            entitySearch = this.entitySearchService.pushPredicate(entitySearch, "hasKreditgeber", [this.contactId]);
            entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'isAktiv', null);
            entitySearch = this.entitySearchService.pushPathParam(entitySearch, 'referenceday', JSON.stringify(preparedObject.FromDate));
            //Prepare params to be provided for endpoint
            let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);
        
            console.log("ovo je prapared parms:", preparedSearchParams);
        
            this.lenderEditService.fetchTotalLoansOnInit(preparedSearchParams)
              .subscribe((data: any) => {
                this.mortgageVolumeSumDisplay = 0;
                this.loans = data.rows;
        
                console.log("ovo su podaci u parametrima:",this.loans);
                this.adaptedLoans = this.coreService.objectToArray(this.loans);
                this.adaptedLoans = this.adaptedLoans.map(item => {
        
                    let fullnameC;
                    if (!item.customer.surname) {
                        fullnameC = item.customer.name;
                    } else {
                        fullnameC = item.customer.name + "" + item.customer.surname;
                    }
        
                    this.mortgageVolumeSumDisplay = this.mortgageVolumeSumDisplay + item.creditLimit;
        
                    return {
                        "ID": item.id,
                        "Customer": fullnameC,
                        "Loan": item.creditLimit,
                    }
                });
                console.log("Total loans fetch", data);
        
              },
                (error) => {
                  // Show update error message in right corner
                  console.log(error);
                  this.toastrService.error(this.translate.instant("FAILED TO GET DATA"));
                });
        }


    submitForm(row) {
        console.log("ovo je row", row);
        let preparedObject: any = {};

        // let filteredCreditor = this.adaptedRentalIncome.filter(item => item.id == this.DaterForm.value.selectLender)[0];
        // preparedObject.creditor = filteredCreditor;
        preparedObject.rentalincome = row.rentalIncome;
        preparedObject.id = row.id;
        console.log("This is ID:", preparedObject.id);

        if (row.activatedSince) {
            preparedObject.activatedSince = moment(row.activatedSince).format("YYYY-MM-DD");
        }

        console.log("PREPARED OBJECT", preparedObject);
        this.updateRentalIncomeById(preparedObject);
        
    }

    prepare() {
        this.fetch()
            .subscribe(data => {                
                this.prepareRentalIncome(data[1]);
                console.log("Print Rental income in lender edit:", data[1]);
            });
    }

    fetch() {
        //Pulls all data from the endpoint that you need
        return forkJoin([
            this.lenderEditService.getTotalLoans(this.lenderId),
            this.lenderEditService.getRentalIncome(this.lenderId),
        ]);
    }


      /**
  * Get total loans by reference day
  */
  getTotalLoans() {
    let createEntitySearch: any = {};
    
    //Create entity search with provided params
    let entitySearch = this.entitySearchService.createEntitySearch(createEntitySearch);
    entitySearch = this.entitySearchService.pushPredicate(entitySearch, "hasKreditgeber", [this.contactId]);
    entitySearch = this.entitySearchService.pushPredicate(entitySearch, 'isAktiv', null);
    entitySearch = this.entitySearchService.pushPathParam(entitySearch, 'referenceday', JSON.stringify(this.referanceDate));
    //Prepare params to be provided for endpoint
    let preparedSearchParams = this.entitySearchService.prepareParams(entitySearch);

    console.log("ovo je prapared parms:", preparedSearchParams);

    this.lenderEditService.fetchTotalLoansOnInit(preparedSearchParams)
      .subscribe((data: any) => {
        this.mortgageVolumeSumDisplay = 0;
        this.loans = data.rows;

        console.log("ovo su podaci u parametrima:",this.loans);
        this.adaptedLoans = this.coreService.objectToArray(this.loans);
        this.adaptedLoans = this.adaptedLoans.map(item => {

            let fullnameC;
            if (!item.customer.surname) {
                fullnameC = item.customer.name;
            } else {
                fullnameC = item.customer.name + "" + item.customer.surname;
            }

            this.mortgageVolumeSumDisplay = this.mortgageVolumeSumDisplay + item.creditLimit;

            return {
                "ID": item.id,
                "Customer": fullnameC,
                "Loan": item.creditLimit,
            }
        });
        console.log("Total loans fetch", data);

      },
        (error) => {
          // Show update error message in right corner
          console.log(error);
          this.toastrService.error(this.translate.instant("FAILED TO GET DATA"));
        });
  }



    prepareRentalIncome(data) {
        this.rentalIncome = data.rows;

        this.adaptedRentalIncome = this.coreService.objectToArray(this.rentalIncome);
        this.adaptedRentalIncome = this.adaptedRentalIncome.map(item => {

            return {
                //TODO change name for rental income when it comes from backend
                "rentalIncome": item.betrag,
                "activatedSince": item.startDate,
                "id": item.id,
            }
        });
        console.log("Ispis adapted rental income", this.adaptedRentalIncome);
        //Show hide message in Rental income list
        if (this.adaptedRentalIncome.length == 0) {
            console.log("ovo je duzina rental incoma:", this.rentalIncome)
            this.showEditSectionMessage = true;
        } else {
            this.showEditSectionMessage = false;
        };

        //Show hide edit options in Rental income list
        if (this.adaptedRentalIncome.length > 0) {
            this.showEditSection = true;
        } else {
            this.showEditSection = false;
        };
    }


    //Update Rental income by id    
    updateRentalIncomeById(preparedObject) {

        this.blockUI.start('main-block');
        this.lenderEditService
            .updateRentalIncome(preparedObject.id, preparedObject)
            .subscribe(() => {
                this.prepare();
                this.blockUI.stop('main-block');
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translate.instant("Ne radi update!!!");
                this.toastrService.error(message);
            })
    }

    //Delete Rental income by id
    deleteRentalIncomeById(row) {
        this.blockUI.start('main-block');
        this.lenderEditService
            .deleteRentalIncome(row.id)
            .subscribe(() => {
                this.prepare();
                this.blockUI.stop('main-block');
            }, error => {
                this.blockUI.stop('main-block');
                let message = this.translate.instant('zins_reihe_eintraege_loeschen_failed');
                this.toastrService.error(message);
            })
    }


    /**
     * Used for translating text that's provided through code
     * Set initial translation for page title, and further translate on lang change
     */
    callTransalteService() {
        //Set title on initila load of page
        this.pageTitleService.setTitle(this.translate.instant("gesamtkredit"));

        //On lang change transalte page title
        this.translate.onLangChange.subscribe((event: any) => {
            this.pageTitleService.setTitle(this.translate.instant("gesamtkredit"));
        });
    }

    ngOnDestroy() {
        // Unsubscribe from routeSubscription, to prevent memory leak
        this.routeSubscription.unsubscribe();
    }

    goBack() {
        // window.history.back();
        window.history.back();
    }

    /**
     * Route to Setting/user administration/new user
     */
    goCreateUser() {
        this.router.navigate(['/settings/ac/users/new/', this.lenderData.id]);
    }

    /**
   * Route to Setting/user administration/new user
   */
    goNewInterestRateSwap() {
        this.router.navigate(['/credit/interest-rate-swaps/interest-rate-swaps-edit/', this.lenderData.id]);
    }

    /**
    * Route to Setting/user administration/new user
    */
    newProperty() {
        this.router.navigate(['properties/new/', this.lenderData.id]);
    }


    /**
 * Route to Setting/user administration/new user
 */
    goCreditTotalLoansEdit() {
        this.router.navigate(['credit/totalloans/total-loan-edit/', this.lenderData.id]);
    }


    /**
* Route to Setting/user administration/new user
*/
    goCreditRentalIncome() {
        this.router.navigate(['/credit/rental-income/', this.lenderData.id]);
        console.log(this.lenderData.id);
    }

}
